/*
Programeed : Takumi Adachi / C&G SYSTEMS 2017.8.30
*/

#include "Cube.h"
#include "Line.h"
#include "MMBox.h"

// コンストラクタ
CCube::CCube(VECTOR _pos0, VECTOR _pos1, XMFLOAT4 _color)
{
	pos0 = vm->New(3);
	pos1 = vm->New(3);

	for(int i = 0; i < 3; i++)
	{
		pos0[i] = _pos0[i];
		pos1[i] = _pos1[i];
	}

	color = _color;

	CreateModel();
}

CCube::CCube(MMBox* _box, XMFLOAT4 _color)
{
	pos0 = vm->New(3);
	pos1 = vm->New(3);

	for(int i = 0; i < 3; i++)
	{
		pos0[i] = _box->Min()[i];
		pos1[i] = _box->Max()[i];
	}

	color = _color;

	CreateModel();
}

// デストラクタ
CCube::~CCube()
{
	vm->Delete(pos0);
	vm->Delete(pos1);

	cubeModel.Cleanup();

	std::vector<CLine*>::iterator it = cubeEdges.begin();
	while(it != cubeEdges.end())
	{
		delete (*it);
		++it;
	}

	cubeEdges.clear();
}

// モデルの生成
short CCube::CreateModel()
{
	// Initialize for Cube Model
	D3DVertex vertices[24] =
	{
		// front
		{ XMFLOAT3(pos0[0], pos0[1], pos0[2]), color },
		{ XMFLOAT3(pos1[0], pos0[1], pos0[2]), color },
		{ XMFLOAT3(pos0[0], pos1[1], pos0[2]), color },
		{ XMFLOAT3(pos1[0], pos1[1], pos0[2]), color },

		// back
		{ XMFLOAT3(pos0[0], pos0[1], pos1[2]), color },
		{ XMFLOAT3(pos1[0], pos0[1], pos1[2]), color },
		{ XMFLOAT3(pos0[0], pos1[1], pos1[2]), color },
		{ XMFLOAT3(pos1[0], pos1[1], pos1[2]), color },

		// top
		{ XMFLOAT3(pos0[0], pos0[1], pos0[2]), color },
		{ XMFLOAT3(pos1[0], pos0[1], pos0[2]), color },
		{ XMFLOAT3(pos0[0], pos0[1], pos1[2]), color },
		{ XMFLOAT3(pos1[0], pos0[1], pos1[2]), color },

		// bottom
		{ XMFLOAT3(pos0[0], pos1[1], pos0[2]), color },
		{ XMFLOAT3(pos1[0], pos1[1], pos0[2]), color },
		{ XMFLOAT3(pos0[0], pos1[1], pos1[2]), color },
		{ XMFLOAT3(pos1[0], pos1[1], pos1[2]), color },

		// left
		{ XMFLOAT3(pos0[0], pos0[1], pos0[2]), color },
		{ XMFLOAT3(pos0[0], pos1[1], pos0[2]), color },
		{ XMFLOAT3(pos0[0], pos0[1], pos1[2]), color },
		{ XMFLOAT3(pos0[0], pos1[1], pos1[2]), color },

		// right
		{ XMFLOAT3(pos1[0], pos0[1], pos0[2]), color },
		{ XMFLOAT3(pos1[0], pos1[1], pos0[2]), color },
		{ XMFLOAT3(pos1[0], pos0[1], pos1[2]), color },
		{ XMFLOAT3(pos1[0], pos1[1], pos1[2]), color },
	};

	WORD indices[36] = {
		 0,  1,  2,  1,  2,  3,
         4,  5,  6,  5,  6,  7,
         8,  9, 10,  9, 10, 11,
        12, 13, 14, 13, 14, 15,
        16, 17, 18, 17, 18, 19,
        20, 21, 22, 21, 22, 23
	};

	cubeModel.Init(vertices, 24, indices, 36);

	// Initialize for Cube Edges
	VECTOR edgePos0 = vm->New(3);
	VECTOR edgePos1 = vm->New(3);

	for(int i = 0; i < 3; i++)
	{
		edgePos0[i] = pos0[i] - 0.025;
		edgePos1[i] = pos1[i] + 0.025;
	}

	SCALAR edgeVertices[24][3] = {
		{edgePos0[0], edgePos0[1], edgePos0[2]}, {edgePos1[0], edgePos0[1], edgePos0[2]},
		{edgePos1[0], edgePos0[1], edgePos0[2]}, {edgePos1[0], edgePos1[1], edgePos0[2]},
		{edgePos1[0], edgePos1[1], edgePos0[2]}, {edgePos0[0], edgePos1[1], edgePos0[2]},
		{edgePos0[0], edgePos1[1], edgePos0[2]}, {edgePos0[0], edgePos0[1], edgePos0[2]},

		{edgePos0[0], edgePos0[1], edgePos1[2]}, {edgePos1[0], edgePos0[1], edgePos1[2]},
		{edgePos1[0], edgePos0[1], edgePos1[2]}, {edgePos1[0], edgePos1[1], edgePos1[2]},
		{edgePos1[0], edgePos1[1], edgePos1[2]}, {edgePos0[0], edgePos1[1], edgePos1[2]},
		{edgePos0[0], edgePos1[1], edgePos1[2]}, {edgePos0[0], edgePos0[1], edgePos1[2]},

		{edgePos0[0], edgePos0[1], edgePos0[2]}, {edgePos0[0], edgePos0[1], edgePos1[2]},
		{edgePos1[0], edgePos0[1], edgePos0[2]}, {edgePos1[0], edgePos0[1], edgePos1[2]},
		{edgePos1[0], edgePos1[1], edgePos0[2]}, {edgePos1[0], edgePos1[1], edgePos1[2]},
		{edgePos0[0], edgePos1[1], edgePos0[2]}, {edgePos0[0], edgePos1[1], edgePos1[2]},
	};

	for(int i = 0; i < 12; i++)
	{
		cubeEdges.push_back(new CLine(edgeVertices[i * 2 + 0], edgeVertices[i * 2 + 1]));
	}

	vm->Delete(edgePos0);
	vm->Delete(edgePos1);

	return 0;
}

// 描画
void CCube::Draw()
{
	cubeModel.Draw(BLEND_MULTIPLY);

	std::vector<CLine*>::iterator it = cubeEdges.begin();
	while(it != cubeEdges.end())
	{
		(*it)->Draw();
		++it;
	}
}