#pragma once

//C++でウィンドウプロシージャやウィンドウ関連操作APIを(C++の)クラスにラップしてみる
//https://qiita.com/seekerkrt/items/2478c05c590c308146fd

#include <windows.h>
#include <windowsx.h>
#include <tchar.h>
namespace wcl {
	const int MAX_LOADSTRING = 100; //ウィンドウクラスネームとアプリケーションタイトルの最大文字数です

	class Window {
	public:
		Window();
		virtual ~Window();
	public:
		bool Create(HINSTANCE hInstance, TCHAR class_name[MAX_LOADSTRING], TCHAR app_name[MAX_LOADSTRING], int width, int height);
		bool IsCreated();   //ウィンドウが生成（生存）されているか確認
		HWND GetHWND() { return hWnd; }
		bool UpdateWindow();
		bool ShowWindow(int nCmdShow);
		void SetTitle(TCHAR title[MAX_LOADSTRING]);
		WPARAM GetMessageLoop(UINT wMsgFilterMin, UINT wMsgFilterMax);  //汎用かつ定番のメッセージループ
		WPARAM PeekMessageLoop(UINT wMsgFilterMin, UINT wMsgFilterMax, UINT wRemoveMSG);    //特定用途メッセージループ

	public: //カスタマイズするメソッド
		virtual void InnerPeekMessage() {};//PeekMessageLoopで呼ばれる
		virtual LRESULT LocalWndProc(HWND hWnd, UINT msg, WPARAM wp, LPARAM lp); //デフォルトではDefWndProcを呼ぶかWM_DESTORYを処理するぐらい

	private:
		void SetPointer(HWND hWnd); //windowオブジェクト(thisポインタ)とウィンドウハンドルを関連付けるためのトリックな関数

	protected:
		volatile HWND hWnd = nullptr;
		static LRESULT CALLBACK BaseWndProc(HWND hWnd, UINT msg, WPARAM wp, LPARAM lp); //ラップするためのトリックな関数

	};
};