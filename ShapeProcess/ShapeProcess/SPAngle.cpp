#include "SPAngle.h"

#define _USE_MATH_DEFINES
#include <math.h>

template <typename T>
void SPAngle<T>::Set(T _angle)
{
	angle = _angle;
	FormatAngle();
}

template <typename T> 
void SPAngle<T>::FormatAngle()
{
	if (-360 <= angle && angle <= 360)
	{
		//Do nothing.
	}
	else if (angle < -360)
	{
		angle += 360;
	}
	else//(angle > 360)
	{
		angle -=  360;
	}
}

template <typename T>
T SPAngle<T>::Get()
{
	return angle;
}

template <typename T>
T SPAngle<T>::GetRasian()
{
	return angle * M_PI / 180;
}
//
//template <typename T>
//T SPAngle<T>::Cos()
//{
//	return 
//}
//
//
//template <typename T>
//T SPAngle::Sin()
//{
//
//}