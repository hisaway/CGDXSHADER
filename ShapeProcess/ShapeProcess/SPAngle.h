#pragma once
#include <math.h>

template <typename T = float>
class SPAngle
{
private:
	T angle;

public:
	SPAngle(T _angle) { angle = _angle; };
	~SPAngle() {};

	T Get();
	void Set(T _angle);
	void FormatAngle();
	T GetRasian();
	/*T Cos();
	T Sin();*/
};

