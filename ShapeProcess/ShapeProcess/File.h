/*
	Programed : Takumi Adachi / C&G SYSTEMS 2017.8.30
*/
#pragma once

#include <vector>
#include <string>

class Entity;
class CPoint;
class CBSplineCurve;
class CBSplineSurface;

enum ERROR_TYPE
{
	ERR_UNREADABLE,
	ERR_INCORRECT_FORMAT
};

// ファイル
class File
{
protected:
	std::vector<std::string> formatEntityTypes;

	// 文字列分割
	void StringSplit(const std::string &str, char sep, std::vector<std::string>* splitStrs);

	// 先頭のデータを削除
	void PopFront(std::vector<std::string>* dataStrings, short num = 1);

	// エラー表示
	void DisplayErrorMessage(ERROR_TYPE errorType, char* path);

private:
	// エンティティーの作成
	virtual Entity* CreateEntity(short entityType, std::vector<std::string>& dataStrings);

	// 曲線生成
	CBSplineCurve* CreateCurve(std::vector<std::string>* dataStrings);

	// 曲面生成
	CBSplineSurface* CreateSurface(std::vector<std::string>* dataStrings);

public:
	File();
	~File();

	// エンティティ読み込み
	virtual Entity* Load(char* path);
};