/*
Programeed : Takumi Adachi / C&G SYSTEMS 2017.8.30
*/

#include "MMBox.h"

// コンストラクタ
MMBox::MMBox(VECTOR _minPos, VECTOR _maxPos)
{
	minPos = vm->New(3);
	maxPos = vm->New(3);

	for(int i = 0; i < 3; i++)
	{
		minPos[i] = _minPos[i];
		maxPos[i] = _maxPos[i];
	}
}

// デストラクタ
MMBox::~MMBox()
{
	vm->Delete(minPos);
	vm->Delete(maxPos);
}

short MMBox::CreModel(XMFLOAT4 _modelColor, XMFLOAT4 _polyColor)
{
	int nLine = 12*2;
	UINT nVertices = nLine;
	UINT nIndices = nLine;

	D3DVertex* vertices = new D3DVertex[nVertices];
	WORD* indices = new WORD[nIndices];

	int i = 0;
	
	///
	vertices[i++].Pos = XMFLOAT3(minPos[0], minPos[1], minPos[2]);
	vertices[i++].Pos = XMFLOAT3(minPos[0], maxPos[1], minPos[2]);

	vertices[i++].Pos = XMFLOAT3(minPos[0], maxPos[1], minPos[2]);
	vertices[i++].Pos = XMFLOAT3(minPos[0], maxPos[1], maxPos[2]);

	vertices[i++].Pos = XMFLOAT3(minPos[0], maxPos[1], maxPos[2]);
	vertices[i++].Pos = XMFLOAT3(minPos[0], minPos[1], maxPos[2]);

	vertices[i++].Pos = XMFLOAT3(minPos[0], minPos[1], maxPos[2]);
	vertices[i++].Pos = XMFLOAT3(minPos[0], minPos[1], minPos[2]);

	///
	vertices[i++].Pos = XMFLOAT3(maxPos[0], minPos[1], minPos[2]);
	vertices[i++].Pos = XMFLOAT3(maxPos[0], maxPos[1], minPos[2]);

	vertices[i++].Pos = XMFLOAT3(maxPos[0], maxPos[1], minPos[2]);
	vertices[i++].Pos = XMFLOAT3(maxPos[0], maxPos[1], maxPos[2]);

	vertices[i++].Pos = XMFLOAT3(maxPos[0], maxPos[1], maxPos[2]);
	vertices[i++].Pos = XMFLOAT3(maxPos[0], minPos[1], maxPos[2]);

	vertices[i++].Pos = XMFLOAT3(maxPos[0], minPos[1], maxPos[2]);
	vertices[i++].Pos = XMFLOAT3(maxPos[0], minPos[1], minPos[2]);
	
	///
	vertices[i++].Pos = XMFLOAT3(maxPos[0], minPos[1], minPos[2]);
	vertices[i++].Pos = XMFLOAT3(minPos[0], minPos[1], minPos[2]);
	
	vertices[i++].Pos = XMFLOAT3(maxPos[0], maxPos[1], minPos[2]);
	vertices[i++].Pos = XMFLOAT3(minPos[0], maxPos[1], minPos[2]);

	vertices[i++].Pos = XMFLOAT3(maxPos[0], minPos[1], maxPos[2]);
	vertices[i++].Pos = XMFLOAT3(minPos[0], minPos[1], maxPos[2]);

	vertices[i++].Pos = XMFLOAT3(maxPos[0], maxPos[1], maxPos[2]);
	vertices[i++].Pos = XMFLOAT3(minPos[0], maxPos[1], maxPos[2]);




	// Indices
	for (UINT i = 0; i < nIndices; i++)
	{
		indices[i] = i;
	}

	// Vertices Color
	for (UINT i = 0; i < nVertices; i++)
	{
		vertices[i].Color = _modelColor;
	}

	if (FAILED(boxModel.Init(vertices, nVertices, indices, nIndices, D3D_PRIMITIVE_TOPOLOGY_LINELIST)))
	{
		return 2;
	}

	delete vertices;
	delete indices;

	return 1;
}

void MMBox::Draw()
{
	CreModel();
	boxModel.Draw();
}