#include "BezierBase.h"
#include "MMBox.h"
#include <algorithm>

#define MODEL_POLYGONS 50

BezierBase::BezierBase(XMFLOAT4 _modelColor,XMFLOAT4 _polyColor)
{
	CreModel();
}


BezierBase::~BezierBase()
{

}

int BezierBase::SetCtrlPoints(MATRIX _ctrlPoints, int _nCtrlPoints)
{
	ctrlPoints = vm->New(3, _nCtrlPoints);

	for (int i = 0; i < _nCtrlPoints; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			ctrlPoints[j][i] = _ctrlPoints[j][i];
		}
	}

	return 0;
}


// P!:階乗関数
int BezierBase::factorial(int x) {

	int ans = 0;

	for (ans = 1; x>0; ans *= x--);

	return ans;
};

// nCi:コンビネーション関数
int BezierBase::combination(int n, int i) {

	int ans;

	ans = factorial(n) / factorial(n - i) / factorial(i);

	return ans;
};

// Bernstain基底関数
short BezierBase::Bernstain(SCALAR _t, VECTOR _bsfunc, int _dim) const
{
	if (_dim == 0)
	{
		_bsfunc[0] = pow(1 - _t, 3);
		_bsfunc[1] = 3 * pow(1 - _t, 2)*_t;
		_bsfunc[2] = 3 * (1 - _t) * _t*_t;
		_bsfunc[3] = _t*_t*_t;
	}
	else if (_dim == 1)
	{
		_bsfunc[0] = -3 * pow(1 - _t, 2);
		_bsfunc[1] = 3 - 12 * _t + 9 * _t*_t;
		_bsfunc[2] = 3 * _t*(2 - 3 * _t);
		_bsfunc[3] = 3 * _t*_t;
	}
	else if (_dim == 2)
	{
		_bsfunc[0] = 6 * (1 - _t);
		_bsfunc[1] = 6 * (-2 + 3 * _t);
		_bsfunc[2] = 6 * (1 - 3 * _t);
		_bsfunc[3] = 6 * _t;
	}
	else
	{
		return 1;
	}

//	_bsfunc = _bsfunc;

	return 0;
};

short BezierBase::GetMMBox(MMBox ** _mmbox, short _nCtrlPoints)
{
	SetTargetPos(_nCtrlPoints);

	*_mmbox = new MMBox(minPos, maxPos);

	return 0;
}

short BezierBase::SetTargetPos(short _nCtrlPoints)
{
	std::vector<SCALAR> xs, ys, zs;

	for (int i = 0; i < _nCtrlPoints; i++)
	{
		xs.push_back(ctrlPoints[0][i]);
		ys.push_back(ctrlPoints[1][i]);
		zs.push_back(ctrlPoints[2][i]);
	}

	std::sort(xs.begin(), xs.end());
	std::sort(ys.begin(), ys.end());
	std::sort(zs.begin(), zs.end());

	minPos[0] = xs.front();
	minPos[1] = ys.front();
	minPos[2] = zs.front();

	maxPos[0] = xs.back();
	maxPos[1] = ys.back();
	maxPos[2] = zs.back();

	return 0;
}

short BezierBase::Split(
	const SCALAR _t, 
	const MATRIX& _ctrlPoints, 
	MATRIX& _ctrlPointsA, 
	MATRIX& _ctrlPointsB)
{
	SCALAR t = _t;

	MATRIX tmp = vm->New(3, 3);
	_ctrlPointsA = vm->New(3, 4);
	_ctrlPointsB = vm->New(3, 4);

	for (int i = 0; i < 3; i++)
	{
		// 第1世代
		for (int j = 0; j < 3; j++)
		{
			tmp[0][j] = _ctrlPoints[i][j] * (1 - t) + _ctrlPoints[i][j + 1] * t;
		}
		// 第2世代
		for (int j = 0; j < 2; j++)
		{
			tmp[1][j] = tmp[0][j] * (1 - t) + tmp[0][j + 1] * t;
		}
		// 第3世代
		tmp[2][0] = tmp[1][0] * (1 - t) + tmp[1][1] * t;

		// 制御点の出力
		_ctrlPointsA[i][0] = _ctrlPoints[i][0];
		_ctrlPointsA[i][1] = tmp[0][0];
		_ctrlPointsA[i][2] = tmp[1][0];
		_ctrlPointsA[i][3] = tmp[2][0];
		_ctrlPointsB[i][0] = tmp[2][0];
		_ctrlPointsB[i][1] = tmp[1][1];
		_ctrlPointsB[i][2] = tmp[0][2];
		_ctrlPointsB[i][3] = _ctrlPoints[i][3];
	}

	vm->Delete(tmp);

	return 0;
}

void BezierBase::Draw()
{
	bsModel.Draw();
}

short BezierBase::CreModel()
{
	UINT nVertices = MODEL_POLYGONS*4;
	UINT nIndices = MODEL_POLYGONS*4;

	D3DVertex* vertices = new D3DVertex[nVertices];
	WORD* indices = new WORD[nIndices];

	SCALAR t;
	VECTOR pos = vm->New(4);
	
	for (int i = 0; i < MODEL_POLYGONS; i++)
	{
		t = i / (SCALAR)(MODEL_POLYGONS - 1);

		if (Bernstain(t, pos) != 0)
		{
			return 1;
		}
		vertices[i].Pos = XMFLOAT3(t, pos[0], 0);
		vertices[i + 1 * MODEL_POLYGONS].Pos = XMFLOAT3(t, pos[1], 0);
		vertices[i + 2 * MODEL_POLYGONS].Pos = XMFLOAT3(t, pos[2], 0);
		vertices[i + 3 * MODEL_POLYGONS].Pos = XMFLOAT3(t, pos[3], 0);
	}

	// Indices
	for (UINT i = 0; i < nIndices; i++)
	{
		indices[i] = i;
	}

	// Vertices Color
	for (UINT i = 0; i < nVertices; i++)
	{
		vertices[i].Color = modelColor;
	}

	if (FAILED(bsModel.Init(vertices, nVertices, indices, nIndices, D3D_PRIMITIVE_TOPOLOGY_LINESTRIP)))
	{
		return 2;
	}
	
	vm->Delete(pos);

	return 0;
}

short BezierBase::PassPoints2CtrlPoints(
	MATRIX _passPoints,
	VECTOR _t,
	short _nPassPoints,
	MATRIX _ctrlPoints,
	short _nCtrlPoints)
{
	// 制御点の座標を解とする連立一次方程式を解く
	// 第5回形状処理勉強会：レジメ第5回_04月
	MATRIX coeff0 = vm->New(_nCtrlPoints, _nCtrlPoints);
	VECTOR coeff1 = vm->New(_nCtrlPoints);
	VECTOR solu = vm->New(_nCtrlPoints);
	VECTOR bFunc = vm->New(_nCtrlPoints);

	if (_nCtrlPoints - 4.0 != 0)
		return 1;

	for (int i = 0; i < 3; i++)
	{
		// 係数ベクトル a
		for (int j = 0; j < _nCtrlPoints; j++)
		{
			Bernstain(_t[j], bFunc);
			for (int k = 0; k < _nCtrlPoints; k++)
			{
				coeff0[j][k] = bFunc[k];
			}
		}


		// 係数ベクトル b
		for (int j = 0; j < _nPassPoints; j++)
		{
			coeff1[j] = _passPoints[i][j];
		}

		// 解を求める
		vm->SimultLinearEquations(coeff0, coeff1, solu, _nCtrlPoints);

		for (int j = 0; j < _nCtrlPoints; j++)
		{
			_ctrlPoints[i][j] = solu[j];
		}
	}

	vm->Delete(coeff0);
	vm->Delete(coeff1);
	vm->Delete(solu);
	vm->Delete(bFunc);

	return 0;
}
