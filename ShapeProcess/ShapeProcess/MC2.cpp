#include <algorithm>
#include <vector>

#include "MC2.h"
#include "LINE.h"
#include "POINT.h"
#include "BezierCurve.h"
#include "BezierSurface.h"
#include "MMBox.h"
#include "CollisionDetector.h"
#include "SPFile.h"
#include "LineStrip.h"

void _mc2::Clear()
{
	/*if()
	resource->clear();
	resource->shrink_to_fit();*/
}

void _mc2::GetResource(vector<Entity*>& _resource, const int menuID)
{
	Clear();
	
	resource = &_resource;

	switch (menuID)
	{
	case MC2_00000:
		DisplayBernstain();
		break;

	case MC2_00001:
		DisplayCurve();
		break;

	case MC2_000011:
		CalcNearpointToBC();
		break;

	case MC2_00002:
		MkBezierCurveFromThroughPoint();
		break;

	case ID_Surf:
		DisplaySurface();
		break;

	case ID_IsoCv:
		UVCurve();
		break;

	case MC2_000041:
		CalcNearpointToBS();
		break;

	case MC2_00006_1:
		BCOffset();
		break;

	case MC2_000081:
		BC_Kapper();
		break;

	case MC2_000091:
		SplitBC();
		break;
	case ID_SplitBS:
		SplitBS();
		break;

	case ID_TrmBS:
		TrmBS();
		break;

	case ID_CD:
		InterferenceDetector();
		break;

	case ID_IP:
		CalcIntersectionPointByMMbox();
		break;

	case ID_BS2BS:
		CalcCrossLine2BS();
		break;

	case ID_ScanPoints:
		DisplayPoints();
		break;

	default:
		break;
	}
}

void _mc2::DisplayPoints() {
	file.LoadASC("Resource/square2.asc", *resource);
}

void _mc2::DisplayCurve()
{
	//BezierCurve* bc = (BezierCurve*)file.Load("Resource/curve01.txt");
	resource->push_back(file.Load("Resource/curve01.txt"));
}

void _mc2::DisplaySurface()
{
	//SCALAR cp[16][3] = {
	//	{0, 0, 0}, {1, 0, 1}, {2, 0, 1}, {3, 0, 0},
	//	{0, 1, 1}, {1, 1, 2}, {2, 1, 2}, {3, 1, 2},
	//	{0, 2, 1}, {1, 2, 1}, {2, 2, 2}, {3, 2, 2},
	//	{0, 3, 1}, {1, 3, 2}, {2, 3, 1}, {3, 3, 2}
	//};

	////Initialize
	//MATRIX ctrlPoints = vm->New(3, 4 * 4);

	//for (int i = 0; i < 4 * 4; i++)
	//{
	//	for (int j = 0; j < 3; j++)
	//	{
	//		ctrlPoints[j][i] = cp[i][j];
	//	}
	//}
	//XMFLOAT4 modelColor = { 0.5f, 0.3f, 0.2f, 0.8f };
	//BezierSurface* bs = new BezierSurface(ctrlPoints, modelColor, modelColor);
	//BezierSurface* bs = (BezierSurface*)file.Load("Resource/surface01.txt");

	//vm->Delete(ctrlPoints);
	resource->push_back(file.Load("Resource/surface01.txt"));
}

//BezierCurve* _mc2::MCP_2015_06_0()
//{
//	SCALAR mcp10_BC[4][3] = {
//		{ 1.0, 2.0, 0.0 },
//		{ 2.0, 3.0, 1.0 },
//		{ 3.0, 1.0, 2.0 },
//		{ 5.0, 1.0, 2.0 }
//	};
//
//	//Initialize
//	MATRIX ctrlPoints = vm->New(3, 4);
//
//	for (int i = 0; i < 4; i++)
//	{
//		for (int j = 0; j < 3; j++)
//		{
//			ctrlPoints[j][i] = mcp10_BC[i][j];
//		}
//	}
//
//	BezierCurve* bc = new BezierCurve(ctrlPoints, 4);
//
//	resource->push_back(bc);
//	return bc;
//	//return cv2;
//}

void _mc2::BCOffset()
{
	SCALAR mcp10_BC[4][3] = {
		{ 1.0, 2.0, 0.0 },
		{ 2.0, 3.0, 1.0 },
		{ 3.0, 1.0, 2.0 },
		{ 5.0, 1.0, 2.0 }
	};

	//Initialize
	MATRIX ctrlPoints = vm->New(3, 4);

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			ctrlPoints[j][i] = mcp10_BC[i][j];
		}
	}

	XMFLOAT4 modelColor = { 0.0f, 0.0f, 0.8f, 0.8f };
	XMFLOAT4 polyColor = { 0.0f, 0.0f, 0.0f, 0.8f };
	BezierCurve* bc = new BezierCurve(ctrlPoints, 4, modelColor, polyColor);

	resource->push_back(bc);
	double t[4] = { 0.0, 0.3, 0.7, 1.0 };

	BezierCurve* ofcv = new BezierCurve(ctrlPoints, 4, modelColor, polyColor);
	ofcv->Offset(0.5, t, 4);

	resource->push_back(ofcv);
	//return ofcv;
	//return cv2;
}

BezierCurve* _mc2::MkBezierCurveFromThroughPoint()
{
	SCALAR ps[4][3] = {
		{1, 2, 0},
		{2, 3, 0},
		{3, 1, 0},
		{5, 1, 0}
	};

	SCALAR t[4] = { 0.0, 0.2, 0.6, 1.0 };

	MATRIX psmat = vm->New(3, 4);
	VECTOR tvec = vm->New(4);

	for (int i = 0; i < 4; i++)
	{
		tvec[i] = t[i];
		for (int j = 0; j < 3; j++)
		{
			psmat[j][i] = ps[i][j];
		}
	}

	XMFLOAT4 modelColor = { 0.2f, 0.6f, 0.2f, 0.8f };
	XMFLOAT4 polyColor = { 0.0f, 0.0f, 0.0f, 0.8f };

	BezierCurve *bc = new BezierCurve(psmat, t, 4, modelColor, polyColor);
	vector<Entity*> v;
	resource->push_back(bc);
	return bc;
}

BezierBase* _mc2::DisplayBernstain()
{
	BezierBase* bb = new BezierBase();
	resource->push_back(bb);
	return bb;
}

void _mc2::BSOffset()
{
	SCALAR cp[16][3] = {
		{ 0, 0, 0 },{ 1, 0, 1 },{ 2, 0, 1 },{ 3, 0, 0 } ,
		{ 0, 1, 1 },{ 1, 1, 2 },{ 2, 1, 2 },{ 3, 1, 2 } ,
		{ 0, 2, 1 },{ 1, 2, 1 },{ 2, 2, 2 },{ 3, 2, 2 } ,
		{ 0, 3, 1 },{ 1, 3, 2 },{ 2, 3, 1 },{ 3, 3, 2 }
	};

	//Initialize
	MATRIX ctrlPoints = vm->New(3, 4 * 4);

	for (int i = 0; i < 4 * 4; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			ctrlPoints[j][i] = cp[i][j];
		}
	}
	XMFLOAT4 modelColor = { 0.5f, 0.3f, 0.2f, 0.8f };
	BezierSurface* bs = new BezierSurface(ctrlPoints, modelColor, modelColor);

	resource->push_back(bs);
}

void _mc2::CalcNearpointToBC()
{
	/*BezierCurve* bc = DisplayCurve();

	double anypoint[3] = { 4, 3, 0 };
	VECTOR np = vm->New(3);
	double t, dist;
	bc->NearestPoint(anypoint, np, &t, &dist);
	CLine* line = new CLine(anypoint, np);

	resource->push_back(line);*/
}

void _mc2::CalcNearpointToBS()
{
	/*BezierSurface* bs = DisplaySurface();

	double anypoint[3] = { 1.0, 1.0, 3.0 };
	double uv[2] = { 0.4, 0.3 };
	VECTOR np = vm->New(3);

	double t, dist;

	bs->NearestPoint(anypoint, uv, np, &dist);

	CLine* line = new CLine(anypoint, np);
	resource->push_back(line);*/

	BezierSurface* bs = 
		(BezierSurface*)file.Load("Resource/surface01.txt");

	double anypoint[3] = { 1.0, 1.0, 3.0 };
	double uv[2] = { 0.4, 0.3 };
	VECTOR np = vm->New(3);

	double dist;

	bs->NearestPoint(anypoint, uv, np, &dist);

	CLine* line = new CLine(anypoint, np);

	resource->push_back(bs);
	resource->push_back(line);
}

void _mc2::UVCurve()
{
	/*double uv[4][2] =
	{
		{0.2, 0.8}, {0.2, 0.2}, {0.8, 0.2}, {0.8, 0.8}
	};

	BezierSurface* bs = DisplaySurface();
	resource->push_back(bs);*/

	BezierSurface* bs =
		(BezierSurface*)file.Load("Resource/surface01.txt");

	BezierCurve* bc;
	bs->IsoCurve(0.5, true, &bc);
	resource->push_back(bc);
}

// オリジナル曲線を10分割した部分曲線とそのミニマクスボックス
void _mc2::SplitBC()
{
	SCALAR cp[4][3] = {
		{ 5.0, 10.0, -10.0 },
		{ 10.0, 15.0, 10.0 },
		{ 15.0, 25.0, 20.0 },
		{ 35.0, 25.0, 30.0 }
	};

	//Initialize
	MATRIX ctrlPoints = vm->New(3, 4);

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			ctrlPoints[j][i] = cp[i][j];
		}
	}

	BezierCurve* bc = new BezierCurve(ctrlPoints, 4);
	BezierCurve *a;
	MMBox* mmbox;
	SCALAR t0, t1;
	int j = 0;
	int nDiv = 10;
	for (int i = 0; i < nDiv - 1; i++)
	{
		t0 = (SCALAR)i / nDiv;
		t1 = (SCALAR)(i + 1) / nDiv;

		if (bc->Triming(t0, t1, &a) == 0) {
			resource->push_back(a);
			a->GetMMBox(&mmbox);
			resource->push_back(mmbox);
		}
	}
	vm->Delete(ctrlPoints);
}

// オリジナル曲面を10分割した部分曲線とそのミニマクスボックス
void _mc2::SplitBS()
{
	SCALAR cpS[16][3] = {
		{ 0, 0, 0 },{ 0, 10, 10 },{ 0, 20, 10 },{ 0, 30, 10 } ,
		{ 10, 0, 10 },{ 10, 10, 20 },{ 10, 20, 10 },{ 10, 30, 20 } ,
		{ 20, 0, 10 },{ 20, 10, 20 },{ 20, 20, 20 },{ 20, 30, 10 } ,
		{ 30, 0, 0 },{ 30, 10, 20 },{ 30, 20, 20 },{ 30, 30, 20 }
	};

	//Initialize
	MATRIX ctrlPointsS = vm->New(3, 4 * 4);

	for (int i = 0; i < 4 * 4; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			ctrlPointsS[j][i] = cpS[i][j];
		}
	}
	XMFLOAT4 modelColor = { 0.5f, 0.3f, 0.2f, 0.8f };
	BezierSurface* bs = new BezierSurface(ctrlPointsS, modelColor, modelColor);

	BezierSurface *a;
	MMBox* mmbox;
	SCALAR v0, v1, u0, u1;
	int j = 0;
	int nDiv = 10;
	for (int i = 0; i < nDiv - 1; i++)
	{
		u0 = (SCALAR)i / nDiv;
		u1 = (SCALAR)(i + 1) / nDiv;
		for (int j = 0; j < nDiv - 1; j++)
		{
			v0 = (SCALAR)j / nDiv;
			v1 = (SCALAR)(j + 1) / nDiv;

			if (bs->Trim(u0, u1, v0, v1, &a) == 0) {
				resource->push_back(a);
				a->GetMMBox(&mmbox);
				resource->push_back(mmbox);
			}
		}
	}
	vm->Delete(ctrlPointsS);
}

void _mc2::TrmBS()
{
	SCALAR cpS[16][3] = {
		{ 0, 0, 0 },{ 0, 10, 10 },{ 0, 20, 10 },{ 0, 30, 10 } ,
		{ 10, 0, 10 },{ 10, 10, 20 },{ 10, 20, 10 },{ 10, 30, 20 } ,
		{ 20, 0, 10 },{ 20, 10, 20 },{ 20, 20, 20 },{ 20, 30, 10 } ,
		{ 30, 0, 0 },{ 30, 10, 20 },{ 30, 20, 20 },{ 30, 30, 20 }
	};

	//Initialize
	MATRIX ctrlPointsS = vm->New(3, 4 * 4);

	for (int i = 0; i < 4 * 4; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			ctrlPointsS[j][i] = cpS[i][j];
		}
	}
	XMFLOAT4 modelColor = { 0.0f, 0.2f, 0.7f, 0.3f };
	BezierSurface* bs = new BezierSurface(ctrlPointsS, modelColor, modelColor);
	BezierSurface* trm;
	bs->Trim(0.3, 0.6, 0.3, 0.6, &trm);

	resource->push_back(trm);
	resource->push_back(bs);
}

void _mc2::InterferenceDetector()
{
	SCALAR v0, v1, u0, u1;
	int j = 0;
	int nDiv = 10;
	int id = 0;
	BezierCurve *trmBC;
	BezierSurface *trmBS;
	MMBox* mmbox;
	CollisionDetector cd;

	//Initialize BC
	SCALAR cp[4][3] = {
		{ 5.0, 10.0, -10.0 },
		{ 10.0, 15.0, 10.0 },
		{ 15.0, 25.0, 20.0 },
		{ 35.0, 25.0, 30.0 }
	};

	MATRIX ctrlPoints = vm->New(3, 4);

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			ctrlPoints[j][i] = cp[i][j];
		}
	}

	BezierCurve* bc = new BezierCurve(ctrlPoints, 4);
	resource->push_back(bc);

	// Calc MMBox
	for (int i = 0; i < nDiv - 1; i++)
	{
		u0 = (SCALAR)i / nDiv;
		u1 = (SCALAR)(i + 1) / nDiv;

		if (bc->Triming(u0, u1, &trmBC) == 0) {
			trmBC->GetMMBox(&mmbox);
			cd.Add(id++, mmbox);
			resource->push_back(mmbox);
		}
	}

	//Initialize BS
	SCALAR cpS[16][3] = {
		{ 0, 0, 0 },	{  0, 10, 10 },{  0, 20, 10 },{  0, 30, 10 } ,
		{ 10, 0, 10 },	{ 10, 10, 20 },{ 10, 20, 10 },{ 10, 30, 20 } ,
		{ 20, 0, 10 },	{ 20, 10, 20 },{ 20, 20, 20 },{ 20, 30, 10 } ,
		{ 30, 0, 0 },	{ 30, 10, 20 },{ 30, 20, 20 },{ 30, 30, 20 }
	};

	MATRIX ctrlPointsBS = vm->New(3, 4 * 4);

	for (int i = 0; i < 4 * 4; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			ctrlPointsBS[j][i] = cpS[i][j];
		}
	}
	XMFLOAT4 modelColor = { 0.5f, 0.3f, 0.2f, 0.8f };
	BezierSurface* bs = new BezierSurface(ctrlPointsBS, modelColor);
	resource->push_back(bs);

	for (int i = 0; i < nDiv - 1; i++)
	{
		u0 = (SCALAR)i / nDiv;
		u1 = (SCALAR)(i + 1) / nDiv;
		for (int j = 0; j < nDiv - 1; j++)
		{
			v0 = (SCALAR)j / nDiv;
			v1 = (SCALAR)(j + 1) / nDiv;

			if (bs->Trim(u0, u1, v0, v1, &trmBS) == 0) {
				trmBS->GetMMBox(&mmbox);
				cd.Add(id++, mmbox);
				resource->push_back(mmbox);
			}
		}
	}

	// MMBoxの衝突(干渉判定)
	std::vector<IntersectMMBoxPair> pairs;
	cd.Check(&pairs);

	std::vector<IntersectMMBoxPair>::iterator it = pairs.begin();
	vector<short> idStuck(pairs.size());

	while (it != pairs.end())
	{
		if (std::find(idStuck.begin(), idStuck.end(), it->Id1()) != idStuck.end())
		{
			resource->push_back(cd.GetMMBox(it->Id1()));
			idStuck.push_back(it->Id1());
		}

		if (std::find(idStuck.begin(), idStuck.end(), it->Id2()) != idStuck.end())
		{
			resource->push_back(cd.GetMMBox(it->Id2()));
			idStuck.push_back(it->Id2());
		}
		++it;
	}
	vm->Delete(ctrlPoints);
	vm->Delete(ctrlPointsBS);
}

void _mc2::BC_Kapper()
{
	SCALAR cp[4][3] = {
		{ 1.0, 2.0, 0.0 },
		{ 2.0, 3.0, 1.0 },
		{ 3.0, 1.0, 2.0 },
		{ 5.0, 1.0, 2.0 }
	};

	//Initialize
	MATRIX ctrlPoints = vm->New(3, 4);

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			ctrlPoints[j][i] = cp[i][j];
		}
	}

	BezierCurve* bc = new BezierCurve(ctrlPoints, 4);
	resource->push_back(bc);

	int nDiv = 100;
	SCALAR t;

	VECTOR P = vm->New(3);
	VECTOR Pt = vm->New(3);
	VECTOR Ptt = vm->New(3);
	VECTOR Ans = vm->New(3);
	SCALAR nrmPt, nrmPtt, nrmAns;

	SCALAR kapper;
	VECTOR kpDir = vm->New(3);

	for (int i = 0; i < nDiv + 1; i++)
	{
		t = (double)i / nDiv;
		bc->Position(t, P);
		bc->Tangent(t, Pt);
		bc->Secdiff(t, Ptt);
		vm->Cross(Pt, Ptt, Ans);
		nrmPtt = vm->Length(Ptt, 3);
		nrmPt = vm->Length(Pt, 3);
		nrmAns = vm->Length(Ans, 3);

		// 曲率
		kapper = nrmAns / pow(nrmPt, 3);

		// 曲率方向
		vm->Cross(Ans, Pt, kpDir);
		vm->Normalize(kpDir, kpDir, 3);

		// 曲率方向ベクトル
		vm->Scale(kpDir, kpDir, 3, 1.0 / kapper);
		vm->Add(kpDir, P, kpDir, 3);
		resource->push_back(new CLine(P, kpDir));
	}

	vm->Delete(ctrlPoints);
	vm->Delete(P);
	vm->Delete(Pt);
	vm->Delete(Ptt);
	vm->Delete(Ans);
	vm->Delete(kpDir);
}

void _mc2::BS_Kapper()
{

}

void _mc2::CalcIntersectionPointByMMbox()
{
	XMFLOAT4 modelColor = { 0.1f, 0.6f, 0.8f, 0.8f };

	//Initialize BC
	SCALAR cpBC[4][3] = {
		{ 5.0, 10.0, -10.0 },
		{ 10.0, 15.0, 10.0 },
		{ 15.0, 25.0, 20.0 },
		{ 35.0, 25.0, 30.0 }
	};

	MATRIX ctrlPointsBC = vm->New(3, 4);

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			ctrlPointsBC[j][i] = cpBC[i][j];
		}
	}

	//Initialize BS
	SCALAR cpBS[16][3] = {
		{ 0, 0, 0 },{ 0, 10, 10 },{ 0, 20, 10 },{ 0, 30, 10 } ,
		{ 10, 0, 10 },{ 10, 10, 20 },{ 10, 20, 10 },{ 10, 30, 20 } ,
		{ 20, 0, 10 },{ 20, 10, 20 },{ 20, 20, 20 },{ 20, 30, 10 } ,
		{ 30, 0, 0 },{ 30, 10, 20 },{ 30, 20, 20 },{ 30, 30, 20 }
	};

	MATRIX ctrlPointsBS = vm->New(3, 4 * 4);

	for (int i = 0; i < 4 * 4; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			ctrlPointsBS[j][i] = cpBS[i][j];
		}
	}

	// Create Object
	BezierSurface* bs = new BezierSurface(ctrlPointsBS, modelColor);
	BezierCurve* bc = new BezierCurve(ctrlPointsBC);

	VECTOR intersectPoint = vm->New(3);

	bs->IntersectionCurveByMMBox(*bc, &intersectPoint);

	CPoint* ip = new CPoint(intersectPoint, XMFLOAT4(0.8f, 0.5f, 0.0f, 0.8f));

	resource->push_back(bs);
	resource->push_back(bc);
	resource->push_back(ip);
}

void _mc2::CalcCrossLine2BS()
{
	BezierSurface* bsA = 
		(BezierSurface*)file.Load("Resource/surfaceA_201412.txt");

	BezierSurface* bsD = 
		(BezierSurface*)file.Load("Resource/surfaceD_201412.txt");

	bsA->SetDrawState(true, false, false);
	//bsD->SetDrawState(false, true, false);

	resource->push_back(bsA);
	resource->push_back(bsD);

	BezierCurve* isoCv;
	UINT divNum = 10;
	for (size_t n = 0; n < divNum; ++n)
	{
		double t = (SCALAR)n / divNum;

		// Uパラメータを固定(Vアイソ曲線)
		bsA->IsoCurve(t, true, &isoCv);
		resource->push_back(isoCv);
	}

	vector<BezierCurve*> intersectCv, intersectUVCvA, intersectUVCvB;
	bsA->IntersectionCvForSurface(10, *bsD, intersectCv, intersectUVCvA, intersectUVCvB);
	vector<BezierCurve*>::iterator it = intersectCv.begin();
	while (it != intersectCv.end())
	{
		resource->push_back(*it);
	}

	//CLineStrip *intersectLine, *il_uvLineA, *il_uvLineB;

	//bsA->IntersectionPoints2Surface(10, *bsD, &intersectLine, &il_uvLineA, &il_uvLineB);

	//resource->push_back(intersectLine);
	///*resource->push_back(il_uvLineA);
	//resource->push_back(il_uvLineB);*/
}

BezierCurve* _mc2::MCP_()
{
	SCALAR mcp10_BC[4][3] = {
		{ 5.0, 10.0, -10.0 },
		{ 10.0, 15.0, 10.0 },
		{ 15.0, 25.0, 20.0 },
		{ 35.0, 25.0, 30.0 }
	};

	//Initialize
	MATRIX ctrlPoints = vm->New(3, 4);

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			ctrlPoints[j][i] = mcp10_BC[i][j];
		}
	}

	BezierCurve* bc = new BezierCurve(ctrlPoints, 4);

	vm->Delete(ctrlPoints);
	resource->push_back(bc);
	return bc;
}