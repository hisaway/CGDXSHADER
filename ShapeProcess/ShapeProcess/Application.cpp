
/*
Programeed : Takumi Adachi / C&G SYSTEMS 2017.8.30
*/

#include "Application.h"
#include "Entity.h"
#include "Axes.h"
#include "Point.h"
#include "Line.h"
#include "Cube.h"
#include "BSplineCurve.h"
#include "BSplineSurface.h"
#include "MMBox.h"
#include "BezierCurve.h"
#include "MC2.h"
#include "SPFile.h"
#include <algorithm>
#include <math.h>

using namespace DirectX;


//--------------------------------------------------------------------------------------
// Constructor and Destructor
//--------------------------------------------------------------------------------------
Application::Application(HINSTANCE _hInstance, HINSTANCE _hPrevInstance, LPWSTR _lpCmdLine, int _nCmdShow) :
	hInstance(_hInstance), hPrevInstance(_hPrevInstance), lpCmdLine(_lpCmdLine), nCmdShow(_nCmdShow),
	driverType(D3D_DRIVER_TYPE_NULL), featureLevel(D3D_FEATURE_LEVEL_11_0),
	pd3dDevice(nullptr), pd3dDevice1(nullptr), pDeviceContext(nullptr), pDeviceContext1(nullptr),
	pSwapChain(nullptr), pSwapChain1(nullptr), pRenderTargetView(nullptr),
	pDepthStencil(nullptr), pDepthStencilView(nullptr),
	pVertexShader(nullptr), pPixelShader(nullptr), pPixelShaderSolid(nullptr), pVertexLayout(nullptr),
	pRasterizerSolid(nullptr), pRasterizerWireframe(nullptr),
	pBlendAlpha(nullptr), pBlendAdd(nullptr), pBlendSubtract(nullptr), pBlendMultiply(nullptr)
{
}

Application::~Application()
{
}


//--------------------------------------------------------------------------------------
// Register class and create window
//--------------------------------------------------------------------------------------
HRESULT Application::InitWindow(HINSTANCE hInstance, int nCmdShow)
{
    // Register class
    WNDCLASSEXW wcex;
    wcex.cbSize = sizeof(WNDCLASSEX);
    wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc = BaseWndProc;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, (LPCTSTR)IDI_APPLICATION);
    wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
    wcex.lpszMenuName = MAKEINTRESOURCEW(IDR_MENU1);

    wcex.lpszClassName = L"ShapeProcessWindowClass";
	wcex.hIconSm = LoadIcon(wcex.hInstance, (LPCTSTR)IDI_APPLICATION);

    if( !RegisterClassExW(&wcex) )
        return E_FAIL;

    // Create window
    RECT rc = {0, 0, SCREEN_WIDTH, SCREEN_HEIGHT};
    AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW, FALSE);
	HWND ret = CreateWindowW(
		L"ShapeProcessWindowClass", L"Shape Process",
        WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX,
        CW_USEDEFAULT, CW_USEDEFAULT, 
		rc.right - rc.left, rc.bottom - rc.top, 
		nullptr, 
		nullptr, 
		hInstance,
        this);
/*
	hWnd = CreateWindow(
		L"ShapeProcessWindowClass", 
		L"Shape Process",
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0,
		nullptr, nullptr, hInstance, nullptr, this);*/

	//ウィンドウ生成に失敗？
	if (ret == nullptr) {
		return false;
	}
	//成功したのでtrueを返して抜ける
	return true;
}


//--------------------------------------------------------------------------------------
// Called every time the application receives a message
//--------------------------------------------------------------------------------------
LRESULT Application::LocalWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HMENU hmenu;
	static MENUITEMINFO menuInfo;
	
	switch (message)
	{
	case WM_CREATE:
	{
		hmenu = GetMenu(hWnd);
		menuInfo.cbSize = sizeof(MENUITEMINFO);
		menuInfo.fState = MFS_CHECKED;
		break;
	}
	case WM_COMMAND:
	{

		menuInfo.fMask = MIIM_STATE;
		int wmId = LOWORD(wParam);
		switch (wmId)
		{
		case OpenFile:
			FileHandler();
			return DefWindowProc(hWnd, message, wParam, lParam);

		default:
			MC2_EventHandler(wmId);
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		
	}
	break;

	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc;
		hdc = BeginPaint(hWnd, &ps);
		EndPaint(hWnd, &ps);
	}
	break;

	case WM_DESTROY:
		::PostQuitMessage(0);
		break;
		/*case WM_CLOSE:
			int id;
			id = ::MessageBox(hWnd, TEXT("終了しますか？"),
				TEXT("終了確認"), MB_OKCANCEL | MB_ICONQUESTION);

			if (id == IDOK) DestroyWindow(this->hWnd);
			break;*/

	default:
		return ::DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

void Application::FileHandler() {
	file.OpenReadFile();
	file.Load(resource);

	const int wmId = OpenFile;
	static HMENU hmenu;
	MENUITEMINFO menuInfo;
	string menuname = to_string(wmId);

	menuInfo.cbSize = sizeof(MENUITEMINFO);
	menuInfo.fMask = MIIM_STATE;
	hmenu = GetMenu(hWnd);
	GetMenuItemInfo(hmenu, wmId, FALSE, &menuInfo);
	menuInfo.fMask = MIIM_STATE;

	map<string, vector<Entity*> >::iterator itr = entities.find(menuname);
	
	if (itr != entities.end()) {
		entities.erase(itr);
	}
	else {
		entities[menuname] = resource;
	}

	resource.clear();
	resource.shrink_to_fit();
}

//Called Menu is Clecked in WndProc
void Application::MC2_EventHandler(const int menuID)
{
	static HMENU hmenu;
	MENUITEMINFO menuInfo;
	string menuname = to_string(menuID);
	
	menuInfo.cbSize = sizeof(MENUITEMINFO);
	menuInfo.fMask = MIIM_STATE;
	hmenu = GetMenu(hWnd);
	GetMenuItemInfo(hmenu, menuID, FALSE, &menuInfo);
	menuInfo.fMask = MIIM_STATE;

	map<string, vector<Entity*> >::iterator itr = entities.find(menuname);
	if (menuInfo.fState == MFS_CHECKED)
	{
		if (itr != entities.end())
			entities.erase(itr);

		menuInfo.fState = MFS_UNCHECKED;
	}
	else
	{
		mc2.GetResource(resource, menuID);
		entities[menuname] = resource;
		menuInfo.fState = MFS_CHECKED;
	}

	SetMenuItemInfo(hmenu, menuID, FALSE, &menuInfo);
}


//--------------------------------------------------------------------------------------
// Create Direct3D device and swap chain
//--------------------------------------------------------------------------------------
HRESULT Application::InitDirect3D()
{
    HRESULT hr = S_OK;

    RECT rc;
    GetClientRect(hWnd, &rc);
    UINT width = rc.right - rc.left;
    UINT height = rc.bottom - rc.top;

    UINT createDeviceFlags = 0;
#ifdef _DEBUG
    createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

    D3D_DRIVER_TYPE driverTypes[] =
    {
        D3D_DRIVER_TYPE_HARDWARE,
        D3D_DRIVER_TYPE_WARP,
        D3D_DRIVER_TYPE_REFERENCE,
	};
    UINT numDriverTypes = ARRAYSIZE(driverTypes);

    D3D_FEATURE_LEVEL featureLevels[] =
    {
        D3D_FEATURE_LEVEL_11_1,
        D3D_FEATURE_LEVEL_11_0,
        D3D_FEATURE_LEVEL_10_1,
        D3D_FEATURE_LEVEL_10_0,
	};
	UINT numFeatureLevels = ARRAYSIZE(featureLevels);

    for(UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
    {
        driverType = driverTypes[driverTypeIndex];
        hr = D3D11CreateDevice(nullptr, driverType, nullptr, createDeviceFlags, featureLevels, numFeatureLevels,
                                D3D11_SDK_VERSION, &pd3dDevice, &featureLevel, &pDeviceContext);

        if( hr == E_INVALIDARG )
        {
            // DirectX 11.0 platforms will not recognize D3D_FEATURE_LEVEL_11_1 so we need to retry without it
            hr = D3D11CreateDevice(
				nullptr,	//Adapter 
				driverType, //D3D_DRIVER_TYPE_HARDWARE
				nullptr,	//Module
				D3D11_CREATE_DEVICE_BGRA_SUPPORT, //createDeviceFlags,
				&featureLevels[1], 	numFeatureLevels - 1, //highest available feature level
                D3D11_SDK_VERSION, 
				&pd3dDevice, 
				&featureLevel,			// Actual feature level
				&pDeviceContext);	// Device context
		}
		if (SUCCEEDED(hr))
			break;
	}
	if (FAILED(hr))
		return hr;

    // Obtain DXGI factory from device (since we used nullptr for pAdapter above)
    IDXGIFactory1* dxgiFactory = nullptr;
    {
        IDXGIDevice* dxgiDevice = nullptr;
        hr = pd3dDevice->QueryInterface(__uuidof(IDXGIDevice), reinterpret_cast<void**>(&dxgiDevice));
        if( SUCCEEDED(hr) )
        {
            IDXGIAdapter* adapter = nullptr;
            hr = dxgiDevice->GetAdapter(&adapter);
            if( SUCCEEDED(hr) )
            {
                hr = adapter->GetParent(__uuidof(IDXGIFactory1), reinterpret_cast<void**>(&dxgiFactory));
                adapter->Release();
			}
            dxgiDevice->Release();
		}
	}
    if( FAILED(hr) )
        return hr;

	hr = CreateSwapChain(dxgiFactory, width, height);
    if( FAILED(hr) )
        return hr;

	hr = CreateRenderTargetView();
	if( FAILED(hr) )
		return hr;

	hr = CreateDepthStencilView(width, height);
	if( FAILED(hr) )
		return hr;

	SetupViewport(width, height);

	hr = CreateVertexShader();
	if( FAILED(hr) )
		return hr;

	hr = CreatePixelShader();
	if( FAILED(hr) )
		return hr;

	hr = CreateRasterizerState();
	if( FAILED(hr) )
		return hr;

	hr = CreateBlendState();
	if( FAILED(hr) )
		return hr;

	InitCoordTransform(width, height);

	// Initialize the original point of rotation
	//RotateOrigin = XMFLOAT3(25.0f, 15.0f, 0.0f);
	//RotateOrigin = XMFLOAT3(100.0f, 87.9f, 0.0f);

    return S_OK;
}


//--------------------------------------------------------------------------------------
// Create swap chain
//--------------------------------------------------------------------------------------
HRESULT Application::CreateSwapChain(IDXGIFactory1* dxgiFactory, UINT width, UINT height)
{
	HRESULT hr = S_OK;

    IDXGIFactory2* dxgiFactory2 = nullptr;
    hr = dxgiFactory->QueryInterface(__uuidof(IDXGIFactory2), reinterpret_cast<void**>(&dxgiFactory2));
    if( dxgiFactory2 )
    {
        // DirectX 11.1 or later
        hr = pd3dDevice->QueryInterface(__uuidof(ID3D11Device1), reinterpret_cast<void**>(&pd3dDevice1));
        if( SUCCEEDED(hr) )
        {
            (void) pDeviceContext->QueryInterface(__uuidof(ID3D11DeviceContext1), reinterpret_cast<void**>(&pDeviceContext1));
		}

        DXGI_SWAP_CHAIN_DESC1 sd;
        ZeroMemory(&sd, sizeof(sd));
        sd.Width = width;
        sd.Height = height;
        sd.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
        sd.SampleDesc.Count = 1;
        sd.SampleDesc.Quality = 0;
        sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
        sd.BufferCount = 1;

        hr = dxgiFactory2->CreateSwapChainForHwnd(pd3dDevice, hWnd, &sd, nullptr, nullptr, &pSwapChain1);
        if( SUCCEEDED(hr) )
        {
            hr = pSwapChain1->QueryInterface(__uuidof(IDXGISwapChain), reinterpret_cast<void**>(&pSwapChain));
		}

        dxgiFactory2->Release();
	}
    else
    {
        // DirectX 11.0 systems
        DXGI_SWAP_CHAIN_DESC sd;
        ZeroMemory(&sd, sizeof(sd));
        sd.BufferCount = 1;
        sd.BufferDesc.Width = width;
        sd.BufferDesc.Height = height;
        sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
        sd.BufferDesc.RefreshRate.Numerator = 60;
        sd.BufferDesc.RefreshRate.Denominator = 1;
        sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
        sd.OutputWindow = hWnd;
        sd.SampleDesc.Count = 1;
        sd.SampleDesc.Quality = 0;
        sd.Windowed = TRUE;

        hr = dxgiFactory->CreateSwapChain(pd3dDevice, &sd, &pSwapChain);
   }

    // Note this tutorial doesn't handle full-screen swapchains so we block the ALT+ENTER shortcut
    dxgiFactory->MakeWindowAssociation(hWnd, DXGI_MWA_NO_ALT_ENTER);

    dxgiFactory->Release();

	return hr;
}


//--------------------------------------------------------------------------------------
// Create a render target view
//--------------------------------------------------------------------------------------
HRESULT Application::CreateRenderTargetView()
{
	HRESULT hr = S_OK;

    ID3D11Texture2D* pBackBuffer = nullptr;
    hr = pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&pBackBuffer));
    if( FAILED(hr) )
        return hr;

    hr = pd3dDevice->CreateRenderTargetView(pBackBuffer, nullptr, &pRenderTargetView);
    pBackBuffer->Release();

    return hr;
}


//--------------------------------------------------------------------------------------
// Create depth stencil texture
//--------------------------------------------------------------------------------------
HRESULT Application::CreateDepthStencilView(UINT width, UINT height)
{
	HRESULT hr = S_OK;

    D3D11_TEXTURE2D_DESC descDepth;
	ZeroMemory(&descDepth, sizeof(descDepth));
    descDepth.Width = width;
    descDepth.Height = height;
    descDepth.MipLevels = 1;
    descDepth.ArraySize = 1;
    descDepth.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
    descDepth.SampleDesc.Count = 1;
    descDepth.SampleDesc.Quality = 0;
    descDepth.Usage = D3D11_USAGE_DEFAULT;
    descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
    descDepth.CPUAccessFlags = 0;
    descDepth.MiscFlags = 0;
    hr = pd3dDevice->CreateTexture2D(&descDepth, nullptr, &pDepthStencil);
    if( FAILED(hr) )
        return hr;

    // Create the depth stencil view
    D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
	ZeroMemory(&descDSV, sizeof(descDSV));
    descDSV.Format = descDepth.Format;
    descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
    descDSV.Texture2D.MipSlice = 0;
    hr = pd3dDevice->CreateDepthStencilView(pDepthStencil, &descDSV, &pDepthStencilView);

	pDeviceContext->OMSetRenderTargets(1, &pRenderTargetView, pDepthStencilView);

    return hr;
}


//--------------------------------------------------------------------------------------
// Setup the viewport
//--------------------------------------------------------------------------------------
void Application::SetupViewport(UINT width, UINT height)
{
    D3D11_VIEWPORT vp;
    vp.Width = (FLOAT)width;
    vp.Height = (FLOAT)height;
    vp.MinDepth = 0.0f;
    vp.MaxDepth = 1.0f;
    vp.TopLeftX = 0;
    vp.TopLeftY = 0;
    pDeviceContext->RSSetViewports(1, &vp);
}


//--------------------------------------------------------------------------------------
// Create the vertex shader
//--------------------------------------------------------------------------------------
HRESULT Application::CreateVertexShader()
{
	HRESULT hr = S_OK;

	// Compile the vertex shader
	ID3DBlob* pVSBlob = nullptr;
    hr = CompileShaderFromFile(L"basic.fx", "VS", "vs_4_0", &pVSBlob);
    if( FAILED(hr) )
    {
        MessageBox(nullptr,
                    "The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", "Error", MB_OK);
        return hr;
   }

	// Create the vertex shader
	hr = pd3dDevice->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), nullptr, &pVertexShader);
	if( FAILED(hr) )
	{	
		pVSBlob->Release();
        return hr;
	}

	// Define the input layout
	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0},
		{"COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0},
	};
	UINT numElements = ARRAYSIZE(layout);

    // Create the input layout
	hr = pd3dDevice->CreateInputLayout(layout, numElements, pVSBlob->GetBufferPointer(),
                                          pVSBlob->GetBufferSize(), &pVertexLayout);
	pVSBlob->Release();
	if( FAILED(hr) )
        return hr;

    // Set the input layout
    pDeviceContext->IASetInputLayout(pVertexLayout);

	return hr;
}


//--------------------------------------------------------------------------------------
// Create the pixel shader
//--------------------------------------------------------------------------------------
HRESULT Application::CreatePixelShader()
{
	HRESULT hr = S_OK;

	// Compile the pixel shader
	ID3DBlob* pPSBlob = nullptr;
    hr = CompileShaderFromFile(L"basic.fx", "PS", "ps_4_0", &pPSBlob);
    if( FAILED(hr) )
    {
        MessageBox(nullptr,
                    "The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", "Error", MB_OK);
        return hr;
   }

	// Create the pixel shader
	hr = pd3dDevice->CreatePixelShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), nullptr, &pPixelShader);
	pPSBlob->Release();
    if( FAILED(hr) )
        return hr;

	return hr;
}


//--------------------------------------------------------------------------------------
// Helper for compiling shaders with D3DCompile
//
// With VS 11, we could load up prebuilt .cso files instead...
//--------------------------------------------------------------------------------------
HRESULT Application::CompileShaderFromFile(WCHAR* szFileName, LPCSTR szEntryPoint, LPCSTR szShaderModel, ID3DBlob** ppBlobOut)
{
    HRESULT hr = S_OK;

    DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#ifdef _DEBUG
    // Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
    // Setting this flag improves the shader debugging experience, but still allows 
    // the shaders to be optimized and to run exactly the way they will run in 
    // the release configuration of this program.
    dwShaderFlags |= D3DCOMPILE_DEBUG;

    // Disable optimizations to further improve shader debugging
    dwShaderFlags |= D3DCOMPILE_SKIP_OPTIMIZATION;
#endif

    ID3DBlob* pErrorBlob = nullptr;
    hr = D3DCompileFromFile(szFileName, nullptr, nullptr, szEntryPoint, szShaderModel, 
        dwShaderFlags, 0, ppBlobOut, &pErrorBlob);
    if( FAILED(hr) )
    {
        if( pErrorBlob )
        {
            OutputDebugStringA(reinterpret_cast<const char*>(pErrorBlob->GetBufferPointer()));
            pErrorBlob->Release();
       }
        return hr;
   }
    if( pErrorBlob ) pErrorBlob->Release();

    return S_OK;
}


//--------------------------------------------------------------------------------------
// Create the rasterizer state
//--------------------------------------------------------------------------------------
HRESULT Application::CreateRasterizerState()
{
    HRESULT hr = S_OK;

	D3D11_RASTERIZER_DESC rsDesc;
	ZeroMemory(&rsDesc, sizeof(D3D11_RASTERIZER_DESC));
	rsDesc.CullMode = D3D11_CULL_NONE;
	rsDesc.DepthClipEnable = TRUE;

	rsDesc.FillMode = D3D11_FILL_SOLID;
	hr = pd3dDevice->CreateRasterizerState(&rsDesc, &pRasterizerSolid);
	if (FAILED(hr))
		return hr;

	rsDesc.FillMode = D3D11_FILL_WIREFRAME;
	hr = pd3dDevice->CreateRasterizerState(&rsDesc, &pRasterizerWireframe);
	if (FAILED(hr))
		return hr;

	return hr;
}


//--------------------------------------------------------------------------------------
// Create the blend state
//--------------------------------------------------------------------------------------
HRESULT Application::CreateBlendState()
{
    HRESULT hr = S_OK;

	D3D11_BLEND_DESC BlendDesc;
	ZeroMemory(&BlendDesc, sizeof(BlendDesc));
	BlendDesc.AlphaToCoverageEnable = FALSE;
	BlendDesc.IndependentBlendEnable = FALSE;

	// デフォルト
	GetAlphaBlendDesc(&(BlendDesc.RenderTarget[0]));
	hr = pd3dDevice->CreateBlendState(&BlendDesc, &pBlendAlpha);
	if (FAILED(hr))
		return hr;

	// 加算
	GetAddBlendDesc(&(BlendDesc.RenderTarget[0]));
	hr = pd3dDevice->CreateBlendState(&BlendDesc, &pBlendAdd);
	if (FAILED(hr))
		return hr;

	// 減算
	GetSubtractBlendDesc(&(BlendDesc.RenderTarget[0]));
	hr = pd3dDevice->CreateBlendState(&BlendDesc, &pBlendSubtract);
	if (FAILED(hr))
		return hr;

	// 乗算
	GetMultiplyBlendDesc(&(BlendDesc.RenderTarget[0]));
	hr = pd3dDevice->CreateBlendState(&BlendDesc, &pBlendMultiply);
	if (FAILED(hr))
		return hr;

	return hr;
}


//--------------------------------------------------------------------------------------
// Get the blend descs
//--------------------------------------------------------------------------------------
void Application::GetAlphaBlendDesc(D3D11_RENDER_TARGET_BLEND_DESC* renderTarget)
{
	renderTarget->BlendEnable = FALSE;
	renderTarget->SrcBlend = D3D11_BLEND_ONE;
	renderTarget->DestBlend = D3D11_BLEND_ZERO;
	renderTarget->BlendOp = D3D11_BLEND_OP_ADD;
	renderTarget->SrcBlendAlpha = D3D11_BLEND_ONE;
	renderTarget->DestBlendAlpha = D3D11_BLEND_ZERO;
	renderTarget->BlendOpAlpha = D3D11_BLEND_OP_ADD;
	renderTarget->RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
}

void Application::GetAddBlendDesc(D3D11_RENDER_TARGET_BLEND_DESC* renderTarget)
{
	renderTarget->BlendEnable = TRUE;
	renderTarget->SrcBlend = D3D11_BLEND_SRC_ALPHA;
	renderTarget->DestBlend = D3D11_BLEND_ONE;
	renderTarget->BlendOp = D3D11_BLEND_OP_ADD;
	renderTarget->SrcBlendAlpha = D3D11_BLEND_ONE;
	renderTarget->DestBlendAlpha = D3D11_BLEND_ZERO;
	renderTarget->BlendOpAlpha = D3D11_BLEND_OP_ADD;
	renderTarget->RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
}

void Application::GetSubtractBlendDesc(D3D11_RENDER_TARGET_BLEND_DESC* renderTarget)
{
	renderTarget->BlendEnable = TRUE;
	renderTarget->SrcBlend = D3D11_BLEND_SRC_ALPHA;
	renderTarget->DestBlend = D3D11_BLEND_ONE;
	renderTarget->BlendOp = D3D11_BLEND_OP_REV_SUBTRACT;
	renderTarget->SrcBlendAlpha = D3D11_BLEND_ONE;
	renderTarget->DestBlendAlpha = D3D11_BLEND_ZERO;
	renderTarget->BlendOpAlpha = D3D11_BLEND_OP_ADD;
	renderTarget->RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
}

void Application::GetMultiplyBlendDesc(D3D11_RENDER_TARGET_BLEND_DESC* renderTarget)
{
	renderTarget->BlendEnable = TRUE;
	renderTarget->SrcBlend = D3D11_BLEND_ZERO;
	renderTarget->DestBlend = D3D11_BLEND_SRC_COLOR;
	renderTarget->BlendOp = D3D11_BLEND_OP_ADD;
	renderTarget->SrcBlendAlpha = D3D11_BLEND_ONE;
	renderTarget->DestBlendAlpha = D3D11_BLEND_ZERO;
	renderTarget->BlendOpAlpha = D3D11_BLEND_OP_ADD;
	renderTarget->RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
}


void Application::InitCoordTransform(UINT width, UINT height)
{
    // Initialize the world matrices
	World = XMMatrixIdentity();

    // Initialize the view matrix
	View = camera.View();

    // Initialize the projection matrix
	Projection = XMMatrixPerspectiveFovLH(XM_PIDIV4, width / (FLOAT)height, 0.1f, 1000.0f);
}


//--------------------------------------------------------------------------------------
// Clean up the objects we've created
//--------------------------------------------------------------------------------------
void Application::CleanupDirect3D()
{
	if (pDeviceContext)		pDeviceContext->ClearState();

	if (pBlendAlpha)			pBlendAlpha->Release();
	if (pBlendAdd)				pBlendAdd->Release();
	if (pBlendSubtract)			pBlendSubtract->Release();
	if (pBlendMultiply)			pBlendMultiply->Release();

	if (pRasterizerSolid)		pRasterizerSolid->Release();
	if (pRasterizerWireframe)	pRasterizerWireframe->Release();

	if (pVertexLayout)			pVertexLayout->Release();
	if (pPixelShader)			pPixelShader->Release();
	if (pPixelShaderSolid)		pPixelShaderSolid->Release();
	if (pVertexShader)			pVertexShader->Release();
	if (pDepthStencil)			pDepthStencil->Release();
	if (pDepthStencilView)		pDepthStencilView->Release();
	if (pRenderTargetView)		pRenderTargetView->Release();
	if (pSwapChain1)			pSwapChain1->Release();
	if (pSwapChain)				pSwapChain->Release();
	if (pDeviceContext1)		pDeviceContext1->Release();
	if (pDeviceContext)		pDeviceContext->Release();
	if (pd3dDevice1)			pd3dDevice1->Release();
	if (pd3dDevice)				pd3dDevice->Release();
}


//--------------------------------------------------------------------------------------
// Create DirectInput device
//--------------------------------------------------------------------------------------
HRESULT Application::InitDirectInput()
{
	HRESULT hr = S_OK;

	// Initialize the main direct input interface
	hr = DirectInput8Create(hInstance, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&pDirectInput, NULL);
	if (FAILED(hr))
		return hr;

	// Initialize the direct input interface for the mouse.
	hr = pDirectInput->CreateDevice(GUID_SysMouse, &pMouse, NULL);
	if (FAILED(hr))
		return hr;

	// Set the data format for the mouse using the pre-defined mouse data format.
	hr = pMouse->SetDataFormat(&c_dfDIMouse);
	if (FAILED(hr))
		return hr;

	// Set the cooperative level of the mouse to share with other programs.
	hr = pMouse->SetCooperativeLevel(hWnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);
	if (FAILED(hr))
		return hr;

	// Acquire the mouse.
	hr = pMouse->Acquire();
	if (FAILED(hr))
		return hr;

	return hr;
}


//--------------------------------------------------------------------------------------
// Read the mouse state
//--------------------------------------------------------------------------------------
HRESULT Application::ReadMouse()
{
	HRESULT hr = S_OK;

	// Read the mouse device.
	hr = pMouse->GetDeviceState(sizeof(DIMOUSESTATE), (LPVOID)&mouseState);
	if (FAILED(hr))
	{
		// If the mouse lost focus or was not acquired then try to get control back.
		if ((hr == DIERR_INPUTLOST) || (hr == DIERR_NOTACQUIRED))
		{
			pMouse->Acquire();
		}
		else
			return hr;
	}

	return hr;
}


//--------------------------------------------------------------------------------------
// Clean up the objects we've created
//--------------------------------------------------------------------------------------
void Application::CleanupDirectInput()
{
	// Release the mouse.
	if (pMouse)
	{
		pMouse->Unacquire();
		pMouse->Release();
	}

	// Release the main interface to direct input.
	if (pDirectInput)
	{
		pDirectInput->Release();
	}
}


//--------------------------------------------------------------------------------------
// Create entities
//--------------------------------------------------------------------------------------
HRESULT Application::InitEntities()
{
	HRESULT hr = S_OK;

	//CBSplineCurve* curve = (CBSplineCurve*)file.Load("entities/CurveS.txt");
	//CBSplineSurface* surface = (CBSplineSurface*)file.Load("entities/Surface1.txt");

	//entities.push_back(curve);
	//entities.push_back(surface);

	//curve->SetDrawState(false, false);
	//surface->SetDrawState(true, false, false);

	/*VECTOR intersectPoint = vm->New(3);

	surface->IntersectionForCurve(curve, intersectPoint);

	entities.push_back(new CPoint(intersectPoint, XMFLOAT4(1.0f, 0.1f, 0.1f, 1.0f)));

	vm->Delete(intersectPoint);*/

	/*MC2* mc2 = new MC2();

	BezierCurve* beziercurve = mc2->Func_MCP_2014_12();
	entities.push_back(beziercurve);

	BezierBase* bezierbase = new BezierBase();
	entities.push_back(bezierbase);
	delete mc2;

	entities.push_back(new Axes());*/

	entities["Axes"].push_back(new Axes());

	return hr;
}


//--------------------------------------------------------------------------------------
// Clean up entities
//--------------------------------------------------------------------------------------
void Application::CleanupEntities()
{
	entities.clear();
}


//--------------------------------------------------------------------------------------
// Frame
//--------------------------------------------------------------------------------------
HRESULT Application::Frame()
{
	HRESULT hr = S_OK;

	hr = Update();
	if (FAILED(hr))
		return hr;

	Render();

	return hr;
}


//--------------------------------------------------------------------------------------
// Update per frame
//--------------------------------------------------------------------------------------
HRESULT Application::Update()
{
	HRESULT hr = S_OK;

	hr = ReadMouse();
	if (FAILED(hr))
		return hr;

	bool clickedLeft = (mouseState.rgbButtons[0] & 0x80);	// 左クリック
	bool clickedRight = (mouseState.rgbButtons[1] & 0x80);	// 右クリック
	bool clickedWheel = (mouseState.rgbButtons[2] & 0x80);	// ホイールボタン
	bool scrolledWheel = (mouseState.lZ != 0.0f);			// ホイール回転

	if (clickedLeft)
	{
	}

	// 球面移動
	//else if (clickedRight)
	//{
	//	//camera.Turn(mouseState.lY / 5.0f, -mouseState.lX / 5.0f);
	//	camera.Rotate(mouseState.lX / 100.0f, -mouseState.lY / 100.0f);
	//	//worldの方を動かす

	//}

	// 焦点移動
	else if (clickedWheel)
	{
		float velocity = exp(camera.GetZoomRate() * 0.01f);
		//worldの方を動かす
		//camera.Shift(mouseState.lX * velocity / 15.0f, -mouseState.lY * velocity / 15.0f);
	}

	// 拡大縮小
	else if (scrolledWheel)
	{
		camera.Zoom(-mouseState.lZ / 24.0f);
	}

	camera.Rotate(mouseState.lX / 100.0f, mouseState.lY / 100.0f, clickedRight);

	// 操作しているときのみカメラ更新
	if (clickedRight || clickedWheel || scrolledWheel)
	{
		camera.Update();
	}

	return hr;
}


//--------------------------------------------------------------------------------------
// Render per frame
//--------------------------------------------------------------------------------------
void Application::Render()
{
	//
    // Control Camera
	//
	/*VECTOR target = vm->New(3);
	std::map<std::string, vector<Entity*>>::iterator it = entities.begin();
	while (it != entities.end())
	{
		if (it->first == "Axes")
		{

		}
		else
		{
			vector<Entity*> &v = it->second;
			for (auto AT : v)
			{
				AT->GetTarget(target);
				camera.SetTarget(target[0], target[1], target[2]);
			}
		}
		
		++it;
	}

	vm->Delete(target);*/


	View = camera.View();

	//
    // Clear the back buffer
    //
    pDeviceContext->ClearRenderTargetView(pRenderTargetView, Colors::White);

    //
    // Clear the depth buffer to 1.0 (max depth)
    //
    pDeviceContext->ClearDepthStencilView(pDepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);

    //
    // Render the model
    //


	map<string, vector<Entity*> >::iterator itr = entities.find("Debug");
	if (itr != entities.end())
			entities.erase(itr);

	VECTOR x = vm->New(3);
	VECTOR y = vm->New(3);
	VECTOR z = vm->New(3);
	VECTOR pos = vm->New(3);

	camera.GetAxe(x, y, z);
	vm->Scale(x, x, 3, 10.0);
	vm->Scale(y, y, 3, 10.0);
	vm->Scale(z, z, 3, 10.0);

	CLine* AxeX = new CLine(pos, x, XMFLOAT4(0.5f, 0.5f, 0.0f, 0.0f));
	CLine* AxeY = new CLine(pos, y, XMFLOAT4(0.0f, 0.5f, 0.5f, 0.0f));
	//CLine* AxeZ = new CLine(pos, z, XMFLOAT4(0.0f, 0.0f, 1.0f, 0.0f));

	vector<Entity*> debug;
	debug.push_back(AxeX);
	debug.push_back(AxeY);
	//debug.push_back(AxeZ);
	entities["Debug"] = debug;

	vm->Delete(x);
	vm->Delete(y);
	vm->Delete(z);
	vm->Delete(pos);

	std::map<std::string, vector<Entity*>>::iterator it = entities.begin();
	while (it != entities.end())
	{
		vector<Entity*> &v = it->second;
		for (auto AT : v)
			AT->Draw();

		++it;
	}

    //
    // Present our back buffer to our front buffer
    //
    pSwapChain->Present(0, 0);
}


//--------------------------------------------------------------------------------------
// Render the model
//--------------------------------------------------------------------------------------
void Application::RenderModel(Model* pModel, BLEND_MODE blendMode)
{
	ID3D11Buffer* vtxBuf = pModel->GetVtxBuf();
	ID3D11Buffer* idxBuf = pModel->GetIdxBuf();
	ID3D11Buffer* cstBuf = pModel->GetCstBuf();

	UINT nIndices = pModel->GetNumIndices();

	D3D_PRIMITIVE_TOPOLOGY topo = pModel->GetTopo();

	// Update matrix variables and lighting variables
	ConstantBuffer cb =
	{
		XMMatrixTranspose(World),
		XMMatrixTranspose(View),
		XMMatrixTranspose(Projection)
	};

	pDeviceContext->UpdateSubresource(cstBuf, 0, nullptr, &cb, 0, 0);
	pDeviceContext->VSSetShader(pVertexShader, nullptr, 0);
	pDeviceContext->VSSetConstantBuffers(0, 1, &cstBuf);
	pDeviceContext->PSSetShader(pPixelShader, nullptr, 0);
	pDeviceContext->PSSetConstantBuffers(0, 1, &cstBuf);
	pDeviceContext->RSSetState(pModel->IsWireframe() ? pRasterizerWireframe : pRasterizerSolid);

	ID3D11BlendState* pBlendState = nullptr;
	float blendFactor[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
	switch (blendMode)
	{
		case BLEND_ALPHA:	 pBlendState = pBlendAlpha;		break;
		case BLEND_ADD:		 pBlendState = pBlendAdd;		break;
		case BLEND_SUBTRACT: pBlendState = pBlendSubtract;	break;
		case BLEND_MULTIPLY: pBlendState = pBlendMultiply;	break;
	}
	pDeviceContext->OMSetBlendState(pBlendState, blendFactor, 0xffffffff);

	// Set vertex buffer
	UINT stride = sizeof(D3DVertex);
	UINT offset = 0;
	pDeviceContext->IASetVertexBuffers(0, 1, &vtxBuf, &stride, &offset);

	// Set index buffer
	pDeviceContext->IASetIndexBuffer(idxBuf, DXGI_FORMAT_R16_UINT, 0);

	// Set primitive topology
	pDeviceContext->IASetPrimitiveTopology(topo);

	pDeviceContext->DrawIndexed(nIndices, 0, 0);
}


//--------------------------------------------------------------------------------------
// HSV to RGB
//--------------------------------------------------------------------------------------
void Application::HSV2RGB(short* hsv, short* rgb)
{
	float f;
	int i, p, q, t;

	i = (int)floor(hsv[0] / 60.0f) % 6;
	f = (float)(hsv[0] / 60.0f) - (float)floor(hsv[0] / 60.0f);
	p = (int)floor(hsv[2] * (1.0f - (hsv[1] / 255.0f)) + 0.5);
	q = (int)floor(hsv[2] * (1.0f - (hsv[1] / 255.0f) * f) + 0.5);
	t = (int)floor(hsv[2] * (1.0f - (hsv[1] / 255.0f) * (1.0f - f)) + 0.5);

	switch(i)
	{
		case 0: rgb[0] = hsv[2];	rgb[1] = t;			rgb[2] = p;			break;
		case 1: rgb[0] = q;			rgb[1] = hsv[2];	rgb[2] = p;			break;
		case 2: rgb[0] = p;			rgb[1] = hsv[2];	rgb[2] = t;			break;
		case 3: rgb[0] = p;			rgb[1] = q;			rgb[2] = hsv[2];	break;
		case 4: rgb[0] = t;			rgb[1] = p;			rgb[2] = hsv[2];	break;
		case 5: rgb[0] = hsv[2];	rgb[1] = p;			rgb[2] = q;			break;
	}
}


//--------------------------------------------------------------------------------------
// Error Message
//--------------------------------------------------------------------------------------
void Application::ErrorMessage(LPCSTR lpText, LPCSTR lpCaption, UINT uType)
{
	MessageBox(hWnd, lpText, lpCaption, uType);
}


//--------------------------------------------------------------------------------------
// Quit
//--------------------------------------------------------------------------------------
void Application::Quit()
{
	PostQuitMessage(0);
}


//--------------------------------------------------------------------------------------
// Run
//--------------------------------------------------------------------------------------
int Application::Run()
{
    if( FAILED(InitWindow(hInstance, nCmdShow)))
        return 0;

	this->ShowWindow(nCmdShow);
	this->UpdateWindow();

    if( FAILED(InitDirect3D()) )
    {
        CleanupDirect3D();
        return 0;
	}

    if( FAILED(InitDirectInput()) )
    {
        CleanupDirectInput();
        return 0;
	}

	if( FAILED(InitEntities()) )
	{
		CleanupEntities();
		return 0;
	}

	MSG msg = { 0 };
	do {
		//既定の処理はプロシージャなどに任せて……
		if (::PeekMessage(&msg, nullptr, 0, 0, PM_NOREMOVE)) {
			if (!GetMessage(&msg, nullptr, 0, 0)) /* メッセージ処理 */
				break;


			TranslateMessage(&msg);  //キーボード利用を可能にする
			DispatchMessage(&msg);  //制御をWindowsに戻す
		}
		//そうでない処理は自前でする
		else {
			//自前の処理であるInnerPeekMessage()を呼び出し
			this->InnerPeekMessage();
			Sleep(1);
		}
	} while (msg.message != WM_QUIT);//イベントドリブン処理ループを抜けるまで実行

    CleanupDirect3D();
	CleanupDirectInput();
	CleanupEntities();

	return (int)msg.wParam;
}

void Application::InnerPeekMessage()
{
	Frame();
}