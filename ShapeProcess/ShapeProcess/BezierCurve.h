#pragma once
#include <vector>
#include "BernStain.h"
#include "Point.h"
#include "Entity.h"
#include "common.h"
#include "Model.h"
#include "BezierBase.h"
#include "Line.h"

using namespace std;

class BezierCurve : public BezierBase
{
private:
	bool IsCreatedModel;
	bool drawTangentVec = false;
	bool drawSecdiffVec = false;
	bool drawCtrlPointsCLine = false;
	bool drawCtrlCPoints = false;

	int nCtrlPoints;

	XMFLOAT4 modelColor;	//モデルの色
	XMFLOAT4 polyColor;		//折れ線の色

	std::vector<CLine*>	polyLine;		// 折れ線による曲線表示
	std::vector<CPoint*> ctrlCPoints;	// 制御点列の表示
	std::vector<CPoint*> posPoints;		// 位置ベクトルの表示
	std::vector<CPoint*> tangentPoints;	// 接線ベクトルの表示
	std::vector<CPoint*> secdiffPoints; // 法線ベクトルの表示

	short t2xyz(SCALAR _t, VECTOR _p, int _dim = 0);

	MATRIX CalAllPostion(UINT);

	void SetCtrlPoints(MATRIX _passPoints, VECTOR _t, int _nPassPoints);

public:

	void Draw();
	Model cvModel;

	// [ IN ] 制御点、制御点数
	BezierCurve(MATRIX _ctrlPoints, short _nCtrlPoints = 4, 
		XMFLOAT4 _modelColor = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f),
		XMFLOAT4 _polyColor = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f));

	// [ IN ] 通過点
	BezierCurve(MATRIX _passPoints, VECTOR _t, short _nPassPoints,
		XMFLOAT4 _modelColor = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f),
		XMFLOAT4 _polyColor = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f));

	// [ IN ] 曲線
	BezierCurve(const BezierCurve& bc, 
		XMFLOAT4 _modelColor = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f),
		XMFLOAT4 _polyColor = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f));

	// 代入演算子
	BezierCurve& operator=(const BezierCurve &bc);
	
	// デストラクタ
	~BezierCurve();

	//時間t:0〜1における位置座標群を入力された精度に応じて出力
	short CreModel();

	short CrePoly();

	// 位置ベクトルの算出
	short Position(
		const SCALAR _t,	//時間t
		VECTOR _pos			//位置座標
	);

	// 接線ベクトルの算出 
	short Tangent
	(
		const SCALAR _t,	//時間t
		VECTOR _tan			//接線ベクトル
	);

	// 法線ベクトルの算出
	short Secdiff(
		const SCALAR _t,
		VECTOR _sec
		);

	// 曲線長の長さの算出
	bool CalLength(SCALAR*);

	// 離散点化
	short Discretize(short _nPartitions, std::vector<VECTOR>* _discreatePoints);

	// 指定位置の分割
	short Split(SCALAR _t, BezierCurve** _curveA, BezierCurve** _curveB) const;

	// 部分曲線の取得
	short Triming(SCALAR _t0, SCALAR _t1, BezierCurve** _trmcurve);

	// 曲線のオフセット
	short Offset(double _distance, VECTOR _t, UINT _nPassPoints);

	// 最近点計算
	short NearestPoint(VECTOR _anyPoint, VECTOR _nearestPoint, SCALAR* _t, SCALAR* _distance);

	// ミニマクスボックスの取得
	short GetMMBox(MMBox** mmbox);
};
