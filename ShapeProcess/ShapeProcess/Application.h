/*
Programeed : Takumi Adachi / C&G SYSTEMS 2017.8.30
*/

#pragma once

#include "common.h"
#include "SPFile.h"
#include "DXCamera.h"
#include "CollisionDetector.h"
#include "resource.h"
#include "ExWindow.h"
#include "MC2.h"

#define SCREEN_WIDTH	640		// ウィンドウの幅
#define SCREEN_HEIGHT	480		// ウィンドウの高さ

class Entity;
class Model;

// アプリケーション
class Application : public wcl::Window
{
private:
	HINSTANCE	hInstance;
	HINSTANCE	hPrevInstance;
	LPWSTR		lpCmdLine;
	int			nCmdShow;

	//HWND		hWnd;

	ID3D11RasterizerState *pRasterizerSolid, *pRasterizerWireframe;
	ID3D11BlendState *pBlendAlpha, *pBlendAdd, *pBlendSubtract, *pBlendMultiply;

	XMMATRIX	World;
	XMMATRIX	View;
	XMMATRIX	Projection;

	XMFLOAT3	RotateOrigin;

	IDirectInput8* pDirectInput;
	IDirectInputDevice8* pMouse;
	DIMOUSESTATE mouseState;

	SPFile		file;
	//Camera	camera;
	DXCamera	camera;
	CollisionDetector collisDetector;

	//std::vector<Entity*> entities;
	std::map<std::string, std::vector<Entity*> > entities;
	MC2 mc2;
	vector<Entity*> resource;

	HRESULT		InitWindow(HINSTANCE hInstance, int nCmdShow);
	virtual LRESULT LocalWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	//static LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
	//virtual LRESULT WndProc(HWND, UINT, WPARAM, LPARAM);

	HRESULT		InitDirect3D();
	HRESULT		CreateSwapChain(IDXGIFactory1* dxgiFactory, UINT width, UINT height);
	HRESULT		CreateRenderTargetView();
	HRESULT		CreateDepthStencilView(UINT width, UINT height);
	void		SetupViewport(UINT width, UINT height);
	HRESULT		CreateVertexShader();
	HRESULT		CreatePixelShader();
	HRESULT		CompileShaderFromFile(WCHAR* szFileName, LPCSTR szEntryPoint, LPCSTR szShaderModel, ID3DBlob** ppBlobOut);
	HRESULT		CreateRasterizerState();
	HRESULT		CreateBlendState();
	void		GetAlphaBlendDesc(D3D11_RENDER_TARGET_BLEND_DESC* renderTarget);
	void		GetAddBlendDesc(D3D11_RENDER_TARGET_BLEND_DESC* renderTarget);
	void		GetSubtractBlendDesc(D3D11_RENDER_TARGET_BLEND_DESC* renderTarget);
	void		GetMultiplyBlendDesc(D3D11_RENDER_TARGET_BLEND_DESC* renderTarget);
	void		InitCoordTransform(UINT width, UINT height);
	void		CleanupDirect3D();

	HRESULT		InitDirectInput();
	HRESULT		ReadMouse();
	void		CleanupDirectInput();

	HRESULT		InitEntities();
	void		CleanupEntities();

	HRESULT		Frame();
	HRESULT		Update();
	void		Render();
	void		MC2_EventHandler(const int);
	void		FileHandler();
public:
	D3D_DRIVER_TYPE         driverType;
	D3D_FEATURE_LEVEL       featureLevel;
	ID3D11Device*           pd3dDevice;
	ID3D11Device1*          pd3dDevice1;
	ID3D11DeviceContext*    pDeviceContext;
	ID3D11DeviceContext1*   pDeviceContext1;
	IDXGISwapChain*         pSwapChain;
	IDXGISwapChain1*        pSwapChain1;
	ID3D11RenderTargetView* pRenderTargetView;
	ID3D11Texture2D*        pDepthStencil;
	ID3D11DepthStencilView* pDepthStencilView;
	ID3D11VertexShader*     pVertexShader;
	ID3D11PixelShader*      pPixelShader;
	ID3D11PixelShader*      pPixelShaderSolid;
	ID3D11InputLayout*      pVertexLayout;

	IDXGIDevice* dxgiDevice;

	Application(HINSTANCE _hInstance, HINSTANCE _hPrevInstance, LPWSTR _lpCmdLine, int _nCmdShow);
	~Application();

	void RenderModel(Model* pModel, BLEND_MODE blendMode = BLEND_ALPHA);

	void HSV2RGB(short* hsv, short* rgb);
	void ErrorMessage(LPCSTR lpText, LPCSTR lpCaption, UINT uType);
	void Quit();

	int Run();
	virtual void InnerPeekMessage();
};