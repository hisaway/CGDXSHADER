/*
Programeed : Takumi Adachi / C&G SYSTEMS 2017.8.30
*/

#pragma once

#include "BSplineBase.h"

class CBSplineSurface;

// Bスプライン曲線
class CBSplineCurve :public CBSplineBase
{
private:
	short order;		// 階数
	short nCtrlPoints;	// 制御点数
	short nKnots;		// ノット数
	VECTOR knot;		// ノットベクトル

	short nUpdateCtrlPoints;
	short nUpdateKnots;
	VECTOR updateKnot;

	//MATRIX polyPoints;			// 折れ線点列
	std::vector<CPoint*> polyPoints;// 折れ線点
	std::vector<CLine*> polyEdges;	// 折れ線エッジ

	Model curveModel;		// 曲線モデル

	XMFLOAT4 modelColor;	// モデルの色
	XMFLOAT4 polyColor;		// 折れ線の色

	bool drawPolyPoints;	// 折れ線点列	 を描画するか
	bool drawPolyEdges;		// 折れ線エッジ

	// パラメータ t -> ベクトル (x, y, z)
	short t2xyz(SCALAR _t, VECTOR _p, short _diffTimes = 0);

	// 折れ線点列の生成
	//short CreatePolyPoints(short _nPoints);

	// モデルの生成
	short CreateModel();

	// 折れ線の生成
	short CreatePoly();

	// セットアップ
	void Setup(short _order, MATRIX _ctrlPoints, short _nCtrlPoints, VECTOR _knot);

	// リリース
	void Release();

public:
	// コンストラクタ : [in] 階数, 制御点, ノットベクトル
	CBSplineCurve(short _order, MATRIX _ctrlPoints, short _nCtrlPoints, VECTOR _knot,
		XMFLOAT4 _modelColor = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f),
		XMFLOAT4 _polyColor = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f));

	// コンストラクタ : [in] 通過点
	CBSplineCurve(MATRIX _passPoints, short _nPassPoints,
		XMFLOAT4 _modelColor = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f),
		XMFLOAT4 _polyColor = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f));

	// デストラクタ
	~CBSplineCurve();

	// 制御点列の設定
	short SetCtrlPoints(MATRIX _passPoints, short _nPassPoints);

	// ノットベクトルの設定
	short SetKnot(VECTOR _knot);
	short SetKnot(MATRIX _passPoints, short _nPassPoints);

	// ノットの挿入
	short InsertKnot(SCALAR _t, short* _insertedIndex = NULL);

	// 基底関数
	short BasisFunc(
		SCALAR _t,
		short _reqOrder,
		VECTOR _blendFunc,
		short _diffTimes = 0
	);

	// 位置ベクトル
	short Position(SCALAR _t, VECTOR _pos);

	// 接線ベクトル
	short Tangent(SCALAR _t, VECTOR _tan);

	// 2回微分ベクトル
	short Secdiff(SCALAR _t, VECTOR _sec);

	// 離散点化
	short Discretize(short _nPartitions, std::vector<VECTOR>* _discretePoints);

	// 最近点
	short NearestPoint(VECTOR _outPoint, SCALAR* _t, VECTOR _nearPoint, SCALAR* _distance);

	// 分割
	short Split(SCALAR _t, CBSplineCurve** curveA, CBSplineCurve** curveB);

	// MMボックス取得
	short GetMMBox(MMBox** mmbox);

	// 曲面との交点
	short IntersectionForSurface(CBSplineSurface* surface, VECTOR intersectPoint);

	// 折れ線の参照
	short ReferPoly(short _idx, SCALAR _t, VECTOR _pos);

	// 更新
	void Update();

	// 描画
	void Draw();

	// 描画状態設定
	void SetDrawState(bool _drawPolyPoints, bool _drawPolyEdges)
	{
		drawPolyPoints = _drawPolyPoints;
		drawPolyEdges = _drawPolyEdges;
	}

	// 最小・最大パラメータ取得
	SCALAR MinParam(){ return knot[0]; }
	SCALAR MaxParam(){ return knot[nKnots - 1]; }
};