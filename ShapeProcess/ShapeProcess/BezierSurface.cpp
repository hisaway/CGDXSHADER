/*
	Programmed : Yuki Hisae / C&G SYTEMS 2017.12.07
*/

#include "BezierCurve.h"
#include "BezierSurface.h"
#include "ShapeProcess.h"
#include "Point.h"
#include "Line.h"
#include "TrimLoop.h"
#include "LineStrip.h"

#define MODEL_POLYGONS 20
#define VEC_SIZE (4)
#define MAT_SIZE (4*4)

// コンストラクタ : [in] 制御点
BezierSurface::BezierSurface(
	MATRIX _ctrlPoints,
	XMFLOAT4 _modelColor,
	XMFLOAT4 _meshColor
)
	:
	modelColor(_modelColor),
	meshColor(_meshColor),
	drawWireframe(true),
	drawMeshPoints(true),
	drawMeshEdges(true),
	createdModel(false)//,
	//origin(ORIGIN_STANDARD)
{
	Setup(_ctrlPoints);
}

// コンストラクタ : [in] 通過点
BezierSurface::BezierSurface(
	MATRIX _passPoints, 
	VECTOR _uv,
	short _nUPassPoints, 
	short _nVPassPoints, 
	XMFLOAT4 _modelColor,
	XMFLOAT4 _meshColor)
{

}

// デストラクタ
BezierSurface::~BezierSurface()
{
	Release();

	if (createdModel)
	{
		ReleaseModel();
	}
	vm->Delete(nCtrlPoints);
}

// セットアップ
void BezierSurface::Setup(MATRIX _ctrlPoints)
{
	// 制御点列の初期化
	ctrlPoints = vm->New(3, MAT_SIZE);
	__super::SetCtrlPoints(_ctrlPoints, MAT_SIZE);

	SetTargetPos(MAT_SIZE);
}

// リリース
void BezierSurface::Release()
{
	vm->Delete(ctrlPoints);
}

// セットアップ(表示系)
void BezierSurface::SetupModel()
{
	CreateModel();
	//CreateMesh();
}

// リリース( 表示系 )
void BezierSurface::ReleaseModel()
{
	//ClearPoints(&meshPoints);
	//ClearEdges(&meshEdges);

	surfaceModel.Cleanup();
	surfaceWireframe.Cleanup();
}

// (u, v) - > (x, y, z)
short BezierSurface::uv2xyz(
	VECTOR _uv, 
	VECTOR _p, 
	short _uDiffTimes, 
	short _vDiffTimes
) const 
{
	SCALAR uBsFuncVal[4];
	SCALAR vBsFuncVal[4];

	MATRIX uBsFuncMat = vm->New(VEC_SIZE, VEC_SIZE);
	MATRIX vBsFuncMat = vm->New(VEC_SIZE, VEC_SIZE);
	MATRIX ret1 = vm->New(VEC_SIZE, VEC_SIZE);
	MATRIX ret2 = vm->New(VEC_SIZE, VEC_SIZE);
	MATRIX ctrlPointMat = vm->New(VEC_SIZE, VEC_SIZE);

	if (__super::Bernstain(_uv[0], uBsFuncVal, _uDiffTimes) != 0) return 1;
	if (__super::Bernstain(_uv[1], vBsFuncVal, _vDiffTimes) != 0) return 2;

	for (int i = 0; i < VEC_SIZE; i++)
	{
		uBsFuncMat[0][i] = uBsFuncVal[i];
		vBsFuncMat[i][0] = vBsFuncVal[i];
	}

	for (int k = 0; k < 3; k++)
	{
		for (int i = 0; i < VEC_SIZE; i++)
		{
			for(int j = 0; j < VEC_SIZE; j++)
			{ 
				ctrlPointMat[i][j] = ctrlPoints[k][(short)4 * i + j];
			}
		}

		vm->Mul(uBsFuncMat, ctrlPointMat, ret1, VEC_SIZE, VEC_SIZE);
		vm->Mul(ret1, vBsFuncMat, ret2, VEC_SIZE, VEC_SIZE);

		_p[k] = ret2[0][0];
	}

	//vm->Delete(uBsFuncVal);
	//vm->Delete(vBsFuncVal);
	vm->Delete(uBsFuncMat);
	vm->Delete(vBsFuncMat);
	vm->Delete(ctrlPointMat);
	vm->Delete(ret1);
	vm->Delete(ret2);

	return 0;
}

// 位置ベクトル
short BezierSurface::Position(
	VECTOR _uv, 
	VECTOR _pos
) const 
{
	return uv2xyz(_uv, _pos);
}

// 接線ベクトル
short BezierSurface::Tangent(
	VECTOR _uv, 
	VECTOR _pu, 
	VECTOR _pv)

{
	if (uv2xyz(_uv, _pu, 1, 0) != 0) return 1;
	if (uv2xyz(_uv, _pv, 0, 1) != 0) return 2;

	return 0;
}

// 法線ベクトル
short BezierSurface::Secdiff(
	VECTOR _uv, 
	VECTOR _puu, 
	VECTOR _pvv, 
	VECTOR _puv)

{
	if (uv2xyz(_uv, _puu, 2, 0) != 0) return 1;
	if (uv2xyz(_uv, _pvv, 0, 2) != 0) return 2;
	if (uv2xyz(_uv, _puv, 1, 1) != 0) return 3;

	return 0;
}

// アイソ曲線の取得
short BezierSurface::IsoCurve(
	const SCALAR _t,
	const bool _forU,
	BezierCurve** isoCurve
) const
{
	// 初期化
	// 定数
	const size_t DIM = 3; //(X, Y, Z)
	double t2[VEC_SIZE] = { 0.0, 0.3, 0.7, 1.0 };

	VECTOR uv = vm->New(2);


	// 保管所
	MATRIX passPoints = vm->New(DIM, VEC_SIZE);
	VECTOR pos = vm->New(DIM);


	// 機能
	for (size_t i = 0; i < VEC_SIZE; ++i)
	{
		if (_forU)
		{
			uv[0] = _t;
			uv[1] = t2[i];
		}
		else {
			uv[0] = t2[i];
			uv[1] = _t;
		}
		Position(uv, pos);
		for (size_t d = 0; d < DIM; ++d)
		{
			passPoints[d][i] = pos[d];
		}
	}
	
	// 出力
	*isoCurve = new BezierCurve(passPoints, t2, VEC_SIZE);

	// メモリ開放
	vm->Delete(pos);
	vm->Delete(uv);
	vm->Delete(passPoints);

	// 正常完了
	return 0;
}


// モデルの生成
short BezierSurface::CreateModel()
{
	UINT nVertices = MODEL_POLYGONS * MODEL_POLYGONS;
	UINT nIndices = (MODEL_POLYGONS - 1) * (MODEL_POLYGONS - 1) * 3 * 2;

	D3DVertex* vertices = new D3DVertex[nVertices];
	WORD* indices = new WORD[nIndices];

	VECTOR uv = vm->New(2);
	VECTOR pos = vm->New(3);


	// Vertices Positions
	for (int i = 0; i < MODEL_POLYGONS; i++)
	{
		for (int j = 0; j < MODEL_POLYGONS; j++)
		{
			uv[0] = i / (SCALAR)(MODEL_POLYGONS - 1);
			uv[1] = j / (SCALAR)(MODEL_POLYGONS - 1);

			if (Position(uv, pos) != 0)
			{
				return 1;
			}

			vertices[i * MODEL_POLYGONS + j].Pos = XMFLOAT3(pos[0], pos[1], pos[2]);
		}
	}

	vm->Delete(uv);
	vm->Delete(pos);

	// Indices
	int k, l;
	for (int i = 0; i < MODEL_POLYGONS - 1; i++)
	{
		for (int j = 0; j < MODEL_POLYGONS - 1; j++)
		{
			k = i * MODEL_POLYGONS + j;
			l = (i * (MODEL_POLYGONS - 1) + j) * 6;

			indices[l + 0] = k;
			indices[l + 1] = k + 1;
			indices[l + 2] = k + MODEL_POLYGONS;

			indices[l + 3] = k + 1;
			indices[l + 4] = k + MODEL_POLYGONS + 1;
			indices[l + 5] = k + MODEL_POLYGONS;
		}
	}

	for (UINT i = 0; i < nVertices; i++)
	{
		vertices[i].Color = modelColor;
	}

	if (FAILED(surfaceModel.Init(vertices, nVertices, indices, nIndices, D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST, false)))
	{
		return 2;
	}

	for (UINT i = 0; i < nVertices; i++)
	{
		vertices[i].Color = meshColor;
	}

	if (FAILED(surfaceWireframe.Init(vertices, nVertices, indices, nIndices, D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST, true)))
	{
		return 3;
	}

	delete vertices;
	delete indices;

	return 0;

}

// モデルの描画
void BezierSurface::Draw()
{
	if (!createdModel)
	{
		SetupModel();
		createdModel = true;
	}

	if (drawWireframe)
	{
		surfaceWireframe.Draw();
	}

	if (drawMeshPoints)
	{
		std::vector<CPoint*>::iterator it = meshPoints.begin();
		while (it != meshPoints.end())
		{
			(*it)->Draw();
			++it;
		}
	}

	if (drawMeshEdges)
	{
		std::vector<CLine*>::iterator it = meshEdges.begin();
		while (it != meshEdges.end())
		{
			(*it)->Draw();
			++it;
		}
	}

	surfaceModel.Draw(BLEND_MULTIPLY);
}

// 最近点計算
short BezierSurface::NearestPoint(
	VECTOR _anyPoint, 
	VECTOR _uv, 
	VECTOR _nearestPoint, 
	SCALAR* _distance
)
{
	VECTOR duv = vm->New(2);
	VECTOR appliedUV = vm->New(2);
	VECTOR pos = vm->New(3);
	VECTOR pu = vm->New(3);
	VECTOR pv = vm->New(3);
	VECTOR toOut = vm->New(3);
	VECTOR nearToOut = vm->New(3);

	SCALAR puLength, pvLength;

	// 最大・最小パラメータの取得
	std::vector<SCALAR> uKnotParams, vKnotParams;

	// 候補距離
	SCALAR candidDist;

	// uv変化量
	SCALAR changeAmount;

	// 試行回数
	int trialCount;

	// 正射影を用いる
	for (int i = 0; i < 49; i++)
	{
		// 初期パラメータ
		_uv[0] = (i / 7)  / 6;
		_uv[1] = (i % 7)  / 6;

		// 初期変化uv
		duv[0] = 1.0, duv[1] = 1.0;

		changeAmount = sqrt(duv[0] * duv[0] + duv[1] * duv[1]);
		trialCount = 0;

		// uv変化量:小 or 試行回数:多 まで計算を行う
		while (changeAmount > 10e-3 && trialCount < 50)
		{
			Position(_uv, pos);
			Tangent(_uv, pu, pv);

			puLength = vm->Length(pu, 3);
			pvLength = vm->Length(pv, 3);

			vm->Sub(_anyPoint, pos, toOut, 3);

			duv[0] = vm->Dot(toOut, pu, 3) / puLength / puLength * 0.7;
			duv[1] = vm->Dot(toOut, pv, 3) / pvLength / pvLength * 0.7;

			_uv[0] += duv[0];
			_uv[1] += duv[1];

			// uvパラメータ範囲外に到達 -> 計算打ち切り
			if (_uv[0] < 0.0 || _uv[0] > 1.0 || _uv[1] < 0.0 || _uv[1] > 1.0)
			{
				break;
			}

			changeAmount = sqrt(duv[0] * duv[0] + duv[1] * duv[1]);
			trialCount++;
		}

		// パラメータ制限
		if (_uv[0] < 0.0) _uv[0] = 0.0;
		else if (_uv[0] > 1.0) _uv[0] = 1.0;
		if (_uv[1] < 0.0) _uv[1] = 0.0;
		else if (_uv[1] > 1.0) _uv[1] = 1.0;

		// 候補距離の追加
		Position(_uv, pos);
		vm->Sub(pos, _anyPoint, toOut, 3);

		if ((i == 0) ||
			(i >= 1 && vm->Length(toOut, 3) < candidDist))
		{
			candidDist = vm->Length(toOut, 3);
			appliedUV[0] = _uv[0];
			appliedUV[1] = _uv[1];
		}
	}

	_uv[0] = appliedUV[0];
	_uv[1] = appliedUV[1];

	if (_nearestPoint != NULL)
	{
		Position(_uv, _nearestPoint);
		vm->Sub(_nearestPoint, _anyPoint, nearToOut, 3);

		if (_distance != NULL) *_distance = vm->Length(nearToOut, 3);
	}

	vm->Delete(duv);
	vm->Delete(appliedUV);
	vm->Delete(pos);
	vm->Delete(pu);
	vm->Delete(pv);
	vm->Delete(toOut);
	vm->Delete(nearToOut);

	return 0;
}

short BezierSurface::Split(SCALAR _t, bool _forU, BezierSurface ** _surfaceA, BezierSurface ** _surfaceB)
{
	MATRIX cpPoly = vm->New(3, VEC_SIZE);
	MATRIX tmpA = vm->New(3, VEC_SIZE);
	MATRIX tmpB = vm->New(3, VEC_SIZE);

	MATRIX cpA = vm->New(3, MAT_SIZE);
	MATRIX cpB = vm->New(3, MAT_SIZE);

	for (int k = 0; k < 3; k++)
	{
		for (int i = 0; i < VEC_SIZE; i++)
		{
			for (int j = 0; j < VEC_SIZE; j++)
			{
				if (_forU) {
					cpPoly[k][j] = ctrlPoints[k][(short)4 * j + i];
				}
				else
				{
					cpPoly[k][j] = ctrlPoints[k][(short)4 * i + j];
				}
			}
			__super::Split(_t, cpPoly, tmpA, tmpB);

			for (int j = 0; j < VEC_SIZE; j++)
			{
				if (_forU)
				{
					cpA[k][(short)4 * j + i] = tmpA[k][j];
					cpB[k][(short)4 * j + i] = tmpB[k][j];
				}

				else
				{
					cpA[k][(short)4 * i + j] = tmpA[k][j];
					cpB[k][(short)4 * i + j] = tmpB[k][j];
				}	
			}
		}
	}

	*_surfaceA = new BezierSurface(cpA, XMFLOAT4(0.6f, 0.0f, 0.6f, 1.0f), XMFLOAT4(0.6f, 0.0f, 0.6f, 1.0f));
	*_surfaceB = new BezierSurface(cpB, XMFLOAT4(0.0f, 0.6f, 0.6f, 1.0f), XMFLOAT4(0.0f, 0.6f, 0.6f, 1.0f));

	vm->Delete(cpPoly);
	vm->Delete(tmpA);
	vm->Delete(tmpB);
	vm->Delete(cpA);
	vm->Delete(cpB);

	return 0;
}

short BezierSurface::Trim(const SCALAR _u0, const SCALAR _u1, const SCALAR _v0, const SCALAR _v1, BezierSurface ** _trmSurface)
{
	BezierSurface *tmpA;
	BezierSurface *tmpB;
	BezierSurface *tmpC;

	SCALAR newU1 = (_u1 - _u0) / (1.0 - _u0);
	SCALAR newV1 = (_v1 - _v0) / (1.0 - _v0);

	Split(_u0, true, &tmpA, &tmpB);
	tmpB->Split(newU1, true, &tmpA, &tmpC);
	tmpA->Split(_v0, false, &tmpB, &tmpC);
	tmpC->Split(newV1, false, _trmSurface, &tmpA);

	delete tmpA;
	delete tmpB;
	delete tmpC;

	return 0;
}

// ミニマクスボックスの取得
short BezierSurface::GetMMBox(MMBox** mmbox)
{
	return __super::GetMMBox(mmbox, 16);
}

// トリミング
//short BezierSurface::Trim(LOOP _loop)
//{
//	double t = 0;
//	VECTOR uv = vm->New(2);
//	VECTOR pos = vm->New(3);
//
//	for (int i = 0; i < MODEL_POLYGONS; i++)
//	{
//		t = (double)i / MODEL_POLYGONS;
//		for (auto AT : _loop)
//		{
//			AT->Postion(t, uv);	// return (x, y, z)
//
//			//Create Model
//
//
//		}
//	}
//	vm->Delete(uv);
//}

short BezierSurface::IntersectionCurveByMMBox(
	const BezierCurve& curve, 
	VECTOR* intersect
) const
{
	return ShapeProcess::IntersectionPointByMMBox(curve, *this, intersect);
}

short BezierSurface::IntersectionCvForSurface(
	const UINT divNum,				//[IN]
	const BezierSurface& surface,			//[IN]
	vector<BezierCurve*> intersectcurve,		//[OUT]
	vector<BezierCurve*> intersect_uvcurveA,	//[OUT]
	vector<BezierCurve*> intersect_uvcurvveB	//[OUT]
)const
{
	CLineStrip *intersectLine, *il_uvLineA, *il_uvLineB;
	vector<MATRIX> vctMat;

	IntersectionPoints2Surface(10, surface, &intersectLine, &il_uvLineA, &il_uvLineB);

	vm->Parse(intersectLine->GetPnts(), 4, 3, intersectLine->SIZE(), &vctMat);

	vector<MATRIX>::iterator it = vctMat.begin();
	while (it != vctMat.end())
	{
		intersectcurve.push_back(new BezierCurve(*it));
		++it;
	}
	
	return 0;
}

short BezierSurface::Intersection2Surface(
	const UINT divNum,					//[IN]
	const BezierSurface &surfaceA,
	const BezierSurface &surfaceB,
	vector<BezierCurve*> intersectline,		//[OUT]
	vector<BezierCurve*> intersect_uvlineA,	//[OUT]
	vector<BezierCurve*> intersect_uvlineB	//[OUT]
)
{
	return 
	surfaceA.IntersectionCvForSurface
	(
		divNum,
		surfaceB,
		intersectline,
		intersect_uvlineA,
		intersect_uvlineB
		);
}

// 曲面との交点群
short BezierSurface::IntersectionPoints2Surface(
	const UINT divNum,
	const BezierSurface& surface,			//[IN]
	CLineStrip** intersectline,		//[OUT]
	CLineStrip** intersect_uvlineA,	//[OUT]
	CLineStrip** intersect_uvlineB	//[OUT]
) const
{
	//const size_t div = 10;
	SCALAR t;
	BezierCurve *isoCv;
	VECTOR intersect;

	//MATRIX intersectPnts = vm->New(3, divNum);
	MATRIX intersect_uvPntsA = vm->New(3, divNum);
	MATRIX intersect_uvPntsB = vm->New(3, divNum);

	MATRIX intersectPntsU = vm->New(3, divNum);
	MATRIX intersectPntsV = vm->New(3, divNum);
	
	vector<point3> intersectPnts;
	intersectPnts.reserve(divNum);

	//アイソ曲線との交点
	for (size_t n = 0; n < divNum+1; ++n)
	{
		t = (SCALAR)n / divNum;
		
		// Uパラメータを固定(Vアイソ曲線)
		this->IsoCurve(t, true, &isoCv);
		if (!surface.IntersectionCurveByMMBox(*isoCv, &intersect))
		{
			/*for (UINT i = 0; i < 3; i++)
				intersectPntsU[i][n] = intersect[i];*/
				intersectPnts.push_back(intersect);
				//(n x 3)行列
		}


		/* //Vパラメータを固定(Uアイソ曲線)
		this->IsoCurve(t, false, &isoCv);
		if (!surface.IntersectionCurveByMMBox(*isoCv, intersect))
		{
			intersectPntsV[n] = intersect;
		}*/

		//if()
	}

	// Be careful size of MATRIX 
	*intersectline = new CLineStrip(intersectPnts.data(), intersectPnts.size());

	return 0;
}

short BezierSurface::IntersectionPoints2Surface(
	const UINT divNum,				//[IN]
	const BezierSurface& surfaceA,	//[IN]
	const BezierSurface& surfaceB,	//[IN]
	CLineStrip** intersectline,		//[OUT]
	CLineStrip** intersect_uvlineA,	//[OUT]
	CLineStrip** intersect_uvlineB	//[OUT]
)
{
	return 0;
}