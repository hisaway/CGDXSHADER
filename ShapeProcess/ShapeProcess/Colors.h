#pragma once

#include "common.h"
//#define WHITE XMFLOAT(1.0f, 1.0f, 1.0f, 1.0f)

typedef XMFLOAT4 COLOR;

struct ColorData
{
	COLOR WHITE = COLOR(1.0f, 1.0f, 1.0f, 1.0f);
	COLOR BLACK = COLOR(0.0f, 0.0f, 0.0f, 1.0f);
	COLOR GRAY = COLOR(0.8f, 0.8f, 0.8f, 0.5f);
	COLOR GREEN = COLOR(0.1f, 0.3f, 0.6f, 0.5f);
};

