/*
Programeed : Takumi Adachi / C&G SYSTEMS 2017.8.30
*/

#pragma once

#include "Entity.h"

// 辺
class CLine :public Entity
{
private:
	VECTOR start, end;	// 始点、終点

	Model edgeModel;

	XMFLOAT4 color;

public:
	// コンストラクタ
	CLine(VECTOR _start, VECTOR _end, XMFLOAT4 _color = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f));

	// デストラクタ
	~CLine();

	// 位置ベクトル
	short Position(SCALAR _t, VECTOR _pos);

	// 描画
	void Draw();
};