/*
Programeed : Takumi Adachi / C&G SYSTEMS 2017.8.30
*/
#include "Application.h"

Application* app;
VMUtil* vm;

//--------------------------------------------------------------------------------------
// Entry point to the program. Initializes everything and goes into a message processing 
// loop. Idle time is used to render the scene.
//--------------------------------------------------------------------------------------
int WINAPI wWinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPWSTR lpCmdLine, _In_ int nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	app = new Application(hInstance, hPrevInstance, lpCmdLine, nCmdShow);

	int wParam = app->Run();

	delete app;

    return wParam;
}