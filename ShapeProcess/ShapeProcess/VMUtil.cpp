/*
	Programmed	: Takumi Adachi / C&G SYSTEMS 2017.8.30
	Modified	: Yuki Hisae / C&G SYSTEMS 2018.4.30	
*/

#include "VMUtil.h"
#include <stdlib.h>
#include <math.h>

// コンストラクタ
VMUtil::VMUtil()
	:outfp(NULL)
{
}

// ベクトルの領域確保
VECTOR VMUtil::New(int n)
{
	VECTOR v = new SCALAR[n];

	if (v == NULL)
	{
		fprintf(stderr, "ベクトルのメモリが確保できません\n");
		exit(1);
	}

	for(int i = 0; i < n; i++) v[i] = 0.0;

	return v;
}

// 行列の領域確保
MATRIX VMUtil::New(int nrow, int ncol)
{
	int i;

	MATRIX a = new VECTOR[nrow + 1];

	if (a == NULL)
	{
		fprintf(stderr, "行列のメモリが確保できません\n");
		exit(1);
	}

	for (i = 0; i < nrow; i++)
	{
		a[i] = new SCALAR[ncol];
		if (a[i] == NULL)
		{
			while (--i >= 0)
				delete a[i];
			delete a;
			fprintf(stderr, "行列のメモリが確保できません\n");
			exit(1);
		}
	}

	a[nrow] = NULL;

	for(int i = 0; i < nrow; i++)
		for(int j = 0; j < ncol; j++)
			a[i][j] = 0.0;

	return a;
}

// ベクトルの領域解放
void VMUtil::Delete(VECTOR v)
{
	delete[] v;
}

// 行列の領域解放
void VMUtil::Delete(MATRIX a)
{
	MATRIX b;

	b = a;
	while (*b != NULL)
		delete[] *b++;

	delete[] a;
}

// ベクトルの和
void VMUtil::Add(VECTOR a, VECTOR b, VECTOR c, int n)
{
	for(int i = 0; i < n; i++)
		c[i] = a[i] + b[i];
}

// ベクトルの差
void VMUtil::Sub(VECTOR a, VECTOR b, VECTOR c, int n)
{
	for(int i = 0; i < n; i++)
		b[i] *= -1;

	Add(a, b, c, n);

	for(int i = 0; i < n; i++)
		b[i] *= -1;
}

// ベクトルの内積
SCALAR VMUtil::Dot(VECTOR u, VECTOR v, int dim)
{
	SCALAR in = 0;
	int i;

	for(i = 0; i < dim; i++)
		in += u[i] * v[i];

	return in;
}

// ベクトルの外積
void VMUtil::Cross(VECTOR u, VECTOR v, VECTOR w)
{
	w[0] = u[1] * v[2] - u[2] * v[1];
	w[1] = u[2] * v[0] - u[0] * v[2];
	w[2] = u[0] * v[1] - u[1] * v[0];
}

// ベクトルの単位化
void VMUtil::Normalize(VECTOR x, VECTOR y, int n)
{
	SCALAR xmag = Length(x, n);

	for(int i = 0; i < n; i++)
		y[i] = x[i] / xmag;
}

// ベクトルの大きさ
SCALAR VMUtil::Length(VECTOR x, int n)
{
	return sqrt(Dot(x, x, n));
}

void VMUtil::Scale(VECTOR a, VECTOR b, int n, SCALAR nScale)
{
	for (int i = 0; i < n; i++)
		b[i] = a[i]*nScale;
}

// 行列の和
void VMUtil::Add(MATRIX a, MATRIX b, MATRIX c, int nrow, int ncol)
{
	int i, j;

	for(i=0; i<nrow; i++)
		for(j=0; j<ncol; j++)
			c[i][j] = a[i][j] + b[i][j];
}

// 行列の積
void VMUtil::Mul(MATRIX a, MATRIX b, MATRIX c, int nrow, int ncol)
{
	int i, j, k;

	for(i = 0; i < nrow; i++)
	{
		for(j = 0; j < nrow; j++)
		{
			c[i][j] = 0;
			for(k = 0; k < ncol; k++)
			{
				c[i][j] += a[i][k] * b[k][j];
			}
		}
	}
}

// 連立一次方程式の解
void VMUtil::SimultLinearEquations(MATRIX a, VECTOR b, VECTOR x, int n)
{
	int i, j, k, m;
	SCALAR l, max, sum;

	MATRIX pa = New(n, n);
	VECTOR pb = New(n);

	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < n; j++)
		{
			pa[i][j] = a[i][j];
		}
		pb[i] = b[i];
	}

	// 前進消去
	for(i = 0; i < n - 1; i++)
	{
		// 部分ピボット選択
		max = 0;
		for(j = i; j < n; j++)
		{
			if( fabs(pa[j][i]) > max )
			{
				max = fabs(pa[j][i]);	// 同じ列の中で最大のものを選択
				m = j;					// 最大のものの行を m とする
			}
		}

		if( m != i )
		{
			// 現在の操作対象の行とm行を入れ替える
			VECTOR temp = New(n);

			for(j = i; j < n; j++)
			{
				temp[j] = pa[i][j];
				pa[i][j] = pa[m][j];
				pa[m][j] = temp[j];
			}
			temp[0] = pb[i];
			pb[i] = pb[m];
			pb[m] = temp[0];

			Delete(temp);
		}

		// 係数行列が上三角行列となるような，行単位の加算を行う
		for(j = i + 1; j < n; j++)
		{
			l = - pa[j][i] / pa[i][i];
			for(k = i + 1; k < n; k++)
				pa[j][k] += l * pa[i][k];
			pb[j] += l * pb[i];
		}
	}

	// 後退代入
	pb[n - 1] /= pa[n - 1][n - 1];	// 最後の変数はその係数で割り算するのみ
	for(i = n - 2; i > -1; i--)
	{
		// 求める変数以外を移項して，その係数で割り算する
		sum = 0;
		for(j = i + 1; j < n; j++)
			sum += pa[i][j] * pb[j];
		pb[i] = (pb[i] - sum) / pa[i][i];
	}

	for(int i = 0; i < n; i++)
	{
		x[i] = pb[i];
	}

	Delete(pa);
	Delete(pb);
}

// 二次方程式の解
void VMUtil::SimultQuadraticEquation(SCALAR a, SCALAR b, SCALAR c, VECTOR x1, VECTOR x2)
{
	x1[0] = x1[1] = x2[0] = x2[1] = 0.0;

	if( a == 0.0 )
	{
		if( b != 0.0 ) x1[0] = -c / b;
		return;
	}

	SCALAR d = b * b - 4 * a * c;

	// 判別式に数値による条件分岐
	if ( d > 0.0 )
	{
		x1[0] = ( -b + sqrt(d) ) / (2 * a);
		x2[0] = ( -b - sqrt(d) ) / (2 * a);
	}
	else if ( d == 0.0 )
	{
		x1[0] = x2[0] = -b / (2 * a);
	}
	else {
		x1[0] = x2[0] = -b / (2 * a);
		x1[1] =  sqrt(-d) / (2 * a);
		x2[1] = -sqrt(-d) / (2 * a);
	}
}

// ベクトルの表示
void VMUtil::Print(VECTOR v, int n, char *format)
{
	int i;

	for(i = 0; i < n; i++)
		printf(format, v[i]);
	printf("\n");
}

// 行列の表示
void VMUtil::Print(MATRIX a, int ncol, char *format)
{
	int i;

	for(i = 0; a[i] != NULL; i++)
		Print(a[i], ncol, format);
}

// ベクトルのファイル書き込み
void VMUtil::FilePrint(VECTOR v, int n, char *format)
{
	int i;

	for(i = 0; i < n; i++)
		fprintf(outfp, format, v[i]);
	fprintf(outfp, "\n");
}

// 行列のファイル書き込み
void VMUtil::FilePrint(MATRIX a, int ncol, char *format)
{
	int i;

	for(i = 0; a[i] != NULL; i++)
		FilePrint(a[i], ncol, format);
}

// ファイルポインタの設定
void VMUtil::SetFilePointer(FILE* fp)
{
	outfp = fp;
}




/********************/

void VMUtil::Transpose(const MATRIX a, MATRIX b, const int nrow, const int ncol)
{
	if (a == nullptr)
		return;

	if (b == nullptr)
		return;

	for (int i = 0; i < nrow; i++)
	{
		for (int j = 0; j < ncol; j++)
		{
			b[j][i] = a[i][j];
		}
	}
}

bool VMUtil::Parse(
	const MATRIX a,
	const int divUnit,
	const int nrow,
	const int ncol,
	std::vector<MATRIX> *vctMat
)
{
	MATRIX tmp, A = nullptr;
	int nRow, nCol;

	//3次元ベクトルのみ扱う
	if (nrow != 3 && ncol != 3)
		return false;

	// 配列のサイズは分割単位より大きくなくてはならない
	// divUnit > 3
	if (nrow < divUnit && ncol < divUnit)
		return false;
	
	if (ncol > 3) // MATRIX[3][]
	{
		nRow = ncol;
		nCol = nRow;
		A = New(nRow, nCol);
		
		Transpose(a, A, nrow, ncol);
	}
	if (nrow > 3) // MATRIX[][3]
	{
		A = a;
		nRow = nrow;
		nCol = ncol;
	}
	for (int i = 0; i < (nRow / divUnit) + 1; ++i)
	{
		tmp = New(divUnit, 3);
		for (int j = 0; j < 3; ++j)
		{
			for (int k = 0; k < divUnit; ++k)
			{
				if (nRow - divUnit*i >= divUnit)
				{
					tmp[k][j] = A[i*divUnit + k][j];
				}
				else
				{
					//int index = i*divUnit + k - (i+1)*divUnit - nrow;
					int index = nRow - divUnit + k;
					tmp[k][j] = A[index][j];
				}
			}
		}

		if (ncol > 3)
		{
			MATRIX tmp2 = New(3, divUnit);
			Transpose(tmp, tmp2, divUnit, 3);
			vctMat->push_back(tmp2);
		}
		else
		{
			vctMat->push_back(tmp);
		}
	}
		
	return true;
}

