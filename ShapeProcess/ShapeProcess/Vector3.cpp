///*
//Programmed : Yuki Hisae / C&G SYSTEMS
//*/
//
//
//#include "Vector3.h"
//
//////////////////////////////////////////////////////////
//// 3次元ベクトルクラス
//////////////////////////////////////////////////////////
//
//// ***** コンストラクタ
//template <typename T>
//vector3<T>::vector3()
//{
//	Set(0, 0, 0);
//}
//template <typename T>
//vector3<T>::vector3(T x_, T y_, T z_)
//{
//	Set(x_, y_, z_);
//}
//
//template <typename T>
//void vector3<T>::Normalize()
//{
//	T nrm = x*x + y*y + z*z;
//	x /= nrm;
//	y /= nrm;
//	z /= nrm;
//}
//
//
//// 演算子
//// 代入 =
//template <typename T>
//void vector3<T>::operator =  (vector3 V)
//{
//	Set(V.x, V.y, V.z);
//}
//// 等号 ==
//template <typename T>
//bool vector3<T>::operator == (vector3 V)
//{
//	return (x == V.x && y == V.y && z == V.z);
//}
//// 加算 +
//template <typename T>
//vector3<T> vector3<T>::operator +  (vector3 V)
//{
//	return vector3(x + V.x, y + V.y, z + V.z);
//}
//// 減算 -
//template <typename T>
//vector3<T> vector3<T>::operator -  (vector3 V)
//{
//	return vector3(x - V.x, y - V.y, z - V.z);
//}
//// 係数倍 *
//template <typename T>
//vector3<T> vector3<T>::operator *  (T k)
//{
//	return vector3(k*x, k*y, k*z);
//}
//
//
//// スカラー積
//template <typename T>
//T vector3<T>::DotP(vector3 a, vector3 b)
//{
//	return (a.x * b.x) + (a.y * b.y) + (a.z * b.z);
//}
//
//// ベクトル積
//template <typename T>
//static vector3<T> vector3<T>::CrossP(vector3 a, vector3 b) {
//	vector3 v;
//	v.x = (a.y * b.z) - (a.z * b.y);
//	v.y = (a.z * b.x) - (a.x * b.z);
//	v.z = (a.x * b.y) - (a.y * b.x);
//	return v;
//}
//
//template <typename T>
//static vector3<T> vector3<T>::Normalize(vector3 a)
//{
//	return a.Normalize();
//}
//
//template <typename T>
//void vector3<T>::Set(T x_, T y_, T z_)
//{
//	x = x_;
//	y = y_;
//	z = z_;
//}