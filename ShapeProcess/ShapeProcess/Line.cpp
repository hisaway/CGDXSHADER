#include "Line.h"
#include "Point.h"
#include "Application.h"

// コンストラクタ
CLine::CLine(VECTOR _start, VECTOR _end, XMFLOAT4 _color)
	:color(_color)
{
	start = vm->New(3);
	end	  = vm->New(3);

	for(int i = 0; i < 3; i++)
	{
		start[i] = _start[i];
		end[i]	 = _end[i];
	}

	CreModel();
}

CLine::CLine(CPoint* _start, CPoint* _end, XMFLOAT4 _color)
	:color(_color)
{
	start = vm->New(3);
	end = vm->New(3);

	_start->Position(start);
	_end->Position(end);
	
	CreModel();
}

// デストラクタ
CLine::~CLine()
{
	vm->Delete(start);
	vm->Delete(end);

	edgeModel.Cleanup();
}

void CLine::CreModel()
{
	D3DVertex vertices[2] =
	{
		{ XMFLOAT3(start[0], start[1], start[2]), color },
		{ XMFLOAT3(end[0], end[1], end[2]), color }
	};

	WORD indices[2] = { 0, 1 };

	edgeModel.Init(vertices, 2, indices, 2, D3D_PRIMITIVE_TOPOLOGY_LINELIST);
}

// 位置ベクトル
short CLine::Position(SCALAR _t, VECTOR _pos)
{
	VECTOR dir = vm->New(3);

	vm->Sub(end, start, dir, 3);

	_pos[0] = start[0] + dir[0] * _t;
	_pos[1] = start[1] + dir[1] * _t;
	_pos[2] = start[2] + dir[2] * _t;

	vm->Delete(dir);

	return 0;
}

// 描画
void CLine::Draw()
{
	edgeModel.Draw();
}