/*
	FileName	: SPFile.h
	Programed	: Yuki Hisae / C&G SYSTEMS 2018.4.30
*/
#pragma once

#include <vector>
#include <string>

#include "File.h" 

class Entity;
class CPoint;
class BezierCurve;
class BezierSurface;

using namespace std;

class SPFile :
	public File
{
public:
	SPFile();
	~SPFile();

	int OpenReadFile();

	Entity* Load(char* path);
	void Load(vector<Entity*>& resource);
	void LoadASC(char* path, vector<Entity*>& resource);

private:
	char filename[256];


	// Entity作成
	virtual Entity* CreateEntity(short entityType, std::vector<std::string>& dataStrings);

	void CreateEntities(short entityType, std::vector<std::string>& dataStrings, vector<Entity*>& resource);

	// 点群生成
	void CreatePoints(std::vector<std::string>& dataStrings, vector<Entity*>& resource);

	// Bezier曲線生成
	BezierCurve* CreateCurve(vector<string> &dataStrings);
	void CreateCurve(vector<string> &dataStrings, vector<Entity*>& resource);

	// Bezier曲面生成
	BezierSurface* CreateSurface(vector<string> &dataStrings);
};

