/*
	FileName	: SPFile.cpp
	Programed	: Yuki Hisae / C&G SYSTEMS 2018.4.30
*/
#include <windows.h>
#include <locale.h>
#include <fstream>

#include "SPFile.h"
#include "Point.h"
#include "BezierCurve.h"
#include "BezierSurface.h"

SPFile::SPFile()
	: File()
{
	//formatEntityTypes.push_back("BezierCurve");
	//formatEntityTypes.push_back("BezierSurface");
	formatEntityTypes.push_back("Points");
}


SPFile::~SPFile()
{
}


//オープンファイルダイアログを使用したファイル入力
int SPFile::OpenReadFile()
{
	// Source :http://www-higashi.ist.osaka-u.ac.jp/~k-maeda/vcpp/sec5-2openfiledlg.html
	OPENFILENAME    ofn;
	CHAR  _filename[256], msg[300];
	HANDLE          hFile;
	DWORD           dwBytes;

	size_t wLen = 0;
	errno_t err = 0;
	CHAR buf[1024];

	setlocale(LC_ALL, "Japanese");

	_filename[0] = '\0';  //忘れるとデフォルトファイル名に変な文字列が表示される
	memset(&ofn, 0, sizeof(OPENFILENAME));  //構造体を0でクリア
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.lpstrFilter = "text file(*.txt)\0*.txt\0all file(*.*)\0*.*\0\0";
	//err = mbstowcs_s(&wLen, wStrW, 20, filename, _TRUNCATE);
	ofn.lpstrFile = _filename;
	ofn.nMaxFile = sizeof(_filename);
	ofn.Flags = OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;
	ofn.lpstrDefExt = "txt";

	if (GetOpenFileName(&ofn) != TRUE)
		return 0;

	/*hFile = CreateFile(filename, GENERIC_READ, 0, NULL,
		OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hFile == INVALID_HANDLE_VALUE) {
		wsprintf(msg, "%s failed to open.", filename);
		MessageBox(NULL, msg, "ファイルを開く", MB_OK);
		return FALSE;
	}*/
	//SetFilePointer(hFile, 0, NULL, FILE_BEGIN);
	//ReadFile(hFile, buf, sizeof(buf) - 1, &dwBytes, NULL);
	//*((u_char*)buf + dwBytes) = 0;  //読み終わりに'\0'を挿入

	//CloseHandle(hFile);

	strcpy_s(this->filename, 256, _filename);
	return 1;
}

Entity* SPFile::Load(char* path) {
	return __super::Load(path);
 }

void SPFile::Load(vector<Entity*>& resource) {
	std::ifstream ifs(this->filename);
	std::string lineStr;

	// 読み込み失敗時
	if (ifs.fail())
	{
		DisplayErrorMessage(ERR_UNREADABLE, filename);
		return;
	}

	std::vector<std::string> dataStrings;
	size_t tabPos;
	bool entityTypeDetected = false;
	short entityType = ENT_POINT;

	while (std::getline(ifs, lineStr))
	{
		// 空行 -> 読みとばす
		if (lineStr.length() == 0)
		{
			continue;
		}

		// エンティティタイプ未特定時
		if (!entityTypeDetected)
		{
			if (lineStr.find('[') != -1)
			{
				// タイプ特定
				std::vector<std::string>::iterator it = formatEntityTypes.begin();
				while (it != formatEntityTypes.end())
				{
					if (lineStr.find((*it)) != -1)
					{
						entityTypeDetected = true;
						break;
					}

					entityType++;
					++it;
				}

				// タイプを検出できなかった -> エラー
				if (!entityTypeDetected)
				{
					DisplayErrorMessage(ERR_INCORRECT_FORMAT, filename);
					return;
				}
			}
		}

		// エンティティタイプ特定済
		else {
			// [...] はコメント扱い
			if (lineStr.find('[') != -1)
			{
				continue;
			}

			// 全ての tab を space に置き換え
			tabPos = lineStr.find('\t');
			while (tabPos != -1)
			{
				lineStr.replace(tabPos, 1, " ");
				tabPos = lineStr.find('\t');
			}

			// space による文字列の分割
			StringSplit(lineStr, ' ', &dataStrings);
		}
	}

	CreateEntities(entityType, dataStrings, resource);
}

void SPFile::LoadASC(char* path, vector<Entity*>& resource)
{
	std::ifstream ifs(path);
	std::string lineStr;

	// 読み込み失敗時
	if (ifs.fail())
	{
		DisplayErrorMessage(ERR_UNREADABLE, path);
		return;
	}

	std::vector<std::string> dataStrings;
	size_t tabPos;
	bool entityTypeDetected = false;
	short entityType = ENT_POINT;

	while (std::getline(ifs, lineStr))
	{
		// 空行 -> 読みとばす
		if (lineStr.length() == 0)
		{
			continue;
		}

		// エンティティタイプ未特定時
		if (!entityTypeDetected)
		{
			if (lineStr.find('[') != -1)
			{
				// タイプ特定
				std::vector<std::string>::iterator it = formatEntityTypes.begin();
				while (it != formatEntityTypes.end())
				{
					if (lineStr.find((*it)) != -1)
					{
						entityTypeDetected = true;
						break;
					}

					entityType++;
					++it;
				}

				// タイプを検出できなかった -> エラー
				if (!entityTypeDetected)
				{
					DisplayErrorMessage(ERR_INCORRECT_FORMAT, path);
					return;
				}
			}
		}

		// エンティティタイプ特定済
		else {
			// [...] はコメント扱い
			if (lineStr.find('[') != -1)
			{
				continue;
			}

			// 全ての tab を space に置き換え
			tabPos = lineStr.find('\t');
			while (tabPos != -1)
			{
				lineStr.replace(tabPos, 1, " ");
				tabPos = lineStr.find('\t');
			}

			// space による文字列の分割
			StringSplit(lineStr, ' ', &dataStrings);
		}
	}

	CreateEntities(entityType, dataStrings, resource);
}

Entity* SPFile::CreateEntity(short entityType, std::vector<std::string>& dataStrings)
{
	switch (entityType)
	{
	case ENT_POINT:
		return 0;

	case ENT_EDGE:
		return 0;

	case ENT_CURVE:
		return CreateCurve(dataStrings);

	case ENT_SURFACE:
		return CreateSurface(dataStrings);
	}
}

void SPFile::CreateEntities(short entityType, std::vector<std::string>& dataStrings, vector<Entity*>& resource)
{
	switch (entityType)
	{
	case ENT_POINT:
		CreatePoints(dataStrings, resource);
		
	case ENT_EDGE:
		return ;

	case ENT_CURVE:
		CreateCurve(dataStrings, resource);

	case ENT_SURFACE:
		return ;
	}
}

//点群生成
void SPFile::CreatePoints(std::vector<std::string>& dataStrings, vector<Entity*>& resource) {
	VECTOR vec_tmp = vm->New(3);

	int index = 0;
	int line_num = 0;
	vector<VECTOR> pnts;
	pnts.reserve(dataStrings.size());

	while (1) {
		if (line_num + 7 > dataStrings.size()) {
			break;
		}

		vec_tmp[0] = (float)atof(dataStrings.at(line_num + 2).data()) - 30;
		vec_tmp[1] = (float)atof(dataStrings.at(line_num + 5).data()) - 30;
		vec_tmp[2] = (float)atof(dataStrings.at(line_num + 7).data()) - 300;
		line_num += 8;

		pnts.push_back(vec_tmp);


		//resource.push_back(new CPoint(vec_tmp));
	}

	// ここでpnts変数を使ってアルゴリズムを組む
	// (sample) 
	/*
	MATRIX ctrlPoints = vm->New(3, 4);
	XMFLOAT4 modelColor, polyColor;
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			ctrlPoints[j][i] = pnts[i][j];
		}
	}
	modelColor.x = (float)atof(dataStrings.at(0).data());
	modelColor.y = (float)atof(dataStrings.at(1).data());
	modelColor.z = (float)atof(dataStrings.at(2).data());
	modelColor.w = (float)atof(dataStrings.at(3).data());
	PopFront(&dataStrings, 4);

	polyColor.x = (float)atof(dataStrings.at(0).data());
	polyColor.y = (float)atof(dataStrings.at(1).data());
	polyColor.z = (float)atof(dataStrings.at(2).data());
	polyColor.w = (float)atof(dataStrings.at(3).data());
	PopFront(&dataStrings, 4);

	BezierCurve* curve = new BezierCurve
	(
		ctrlPoints,
		modelColor,
		polyColor
	);

	// 終了する前に、入力引数のresourceにオブジェクトを追加して終了する
	resource.push_back(curve);*/


	vm->Delete(vec_tmp);
}

BezierCurve* SPFile::CreateCurve(vector<string> &dataStrings)
{
	short nCtrlPoints = 4;
	MATRIX ctrlPoints = vm->New(3, nCtrlPoints);

	XMFLOAT4 modelColor, polyColor;

	for (int i = 0; i < nCtrlPoints; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			ctrlPoints[j][i] = (float)atof(dataStrings.at(i * 3 + j).data());
		}
	}
	PopFront(&dataStrings, nCtrlPoints * 3);

	modelColor.x = (float)atof(dataStrings.at(0).data());
	modelColor.y = (float)atof(dataStrings.at(1).data());
	modelColor.z = (float)atof(dataStrings.at(2).data());
	modelColor.w = (float)atof(dataStrings.at(3).data());
	PopFront(&dataStrings, 4);

	polyColor.x = (float)atof(dataStrings.at(0).data());
	polyColor.y = (float)atof(dataStrings.at(1).data());
	polyColor.z = (float)atof(dataStrings.at(2).data());
	polyColor.w = (float)atof(dataStrings.at(3).data());
	PopFront(&dataStrings, 4);

	BezierCurve* curve = new BezierCurve
	(
		ctrlPoints,
		modelColor,
		polyColor
	);

	vm->Delete(ctrlPoints);

	return curve;
}

void SPFile::CreateCurve(vector<string> &dataStrings, vector<Entity*>& resource) {
	resource.push_back(CreateCurve(dataStrings));
}

BezierSurface* SPFile::CreateSurface(vector<string> &dataStrings)
{
	VECTOR nCtrlPoints = vm->New(2);

	nCtrlPoints[0] = 4;
	nCtrlPoints[1] = 4;

	MATRIX ctrlPoints = vm->New(3, nCtrlPoints[0] * nCtrlPoints[1]);

	XMFLOAT4 modelColor, polyColor;

	for (int i = 0; i < nCtrlPoints[0] * nCtrlPoints[1]; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			ctrlPoints[j][i] = (float)atof(dataStrings.at(i * 3 + j).data());
		}
	}
	PopFront(&dataStrings, nCtrlPoints[0] * nCtrlPoints[1] * 3);

	modelColor.x = (float)atof(dataStrings.at(0).data());
	modelColor.y = (float)atof(dataStrings.at(1).data());
	modelColor.z = (float)atof(dataStrings.at(2).data());
	modelColor.w = (float)atof(dataStrings.at(3).data());
	PopFront(&dataStrings, 4);

	polyColor.x = (float)atof(dataStrings.at(0).data());
	polyColor.y = (float)atof(dataStrings.at(1).data());
	polyColor.z = (float)atof(dataStrings.at(2).data());
	polyColor.w = (float)atof(dataStrings.at(3).data());
	PopFront(&dataStrings, 4);

	BezierSurface* surface = new BezierSurface(ctrlPoints, modelColor, polyColor);

	vm->Delete(ctrlPoints);
	
	vm->Delete(nCtrlPoints);

	return surface;
}
