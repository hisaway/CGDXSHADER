/*
Programeed : Takumi Adachi / C&G SYSTEMS 2017.8.30
*/

#pragma once

#include "Entity.h"
#include "Colors.h"

class Axes :public Entity
{
private:
	Model xAxis, yAxis, zAxis;
	Model xSubAxes[51], ySubAxes[51], zSubAxes[51];
	Model field;

	bool drawSubAxes;
	bool drawField;

	short CreateAxes();
	short CreateField();

public:
	Axes(bool _drawSubAxes = true, bool _drawField = true);
	~Axes();

	// �`��
	void Draw();
};