//
///******************************************************************
//
//ヘッダファイル内主要関数
//
//・BEZIER_CURVE　　　
//
//　機　能　：　パラメータｔにおける曲線の座標、接線を出力とする。
//
// void func_BEZIER_C(
// double t,                        i パラメータ
// double Point3f[4][3], 　　　　　 i 座標
// double vec2d[3], 　　　　　　　　o パラメータtにおける座標
// double tan_line2d[3])            o 出力
//
//
// ・BEZIER_SURFACE
//
// 　機　能　：　パラメータu, vにおける曲面の座標、U方向接線ベクトル、V方向接線ベクトルを出力とする。
//
//  void func_BEZIER_S(
//  double u,
//  double v,
//  double Point3f[4][4][3],
//  double vec3d[3],
//  double pu[3],
//  double pv[3])
//
//
//
//  **********************************************************************/
//
//
//
//
//#ifndef _BASIC_H
//#define _BASIC_H
//
#include<math.h>


//
//// P(u, v):BEZIER曲面
//double funcPs(int d, double u, double v, double cpS[4][4][3]) {
//
//	int i, j;
//	double ans;
//
//	ans = 0;
//
//	for (i = 0; i<4; i++) {
//		for (j = 0; j<4; j++) {
//			ans += bernstain(u, i) * bernstain(v, j) * cpS[i][j][d];
//		}
//	}
//	return ans;
//}
//// P(t):BEZIER曲線
//double funcPc(int d, double t, double cpC[4][3]) {
//
//	double ans = 0;
//	int i;
//
//	for (i = 0; i <= 3; i++) {
//		ans += bernstain(t, i) * (cpC[i][d]);
//	}
//	return ans;
//}
//
//double diffU(int d, double u, double v, double cpS[4][4][3]) {
//
//	double ans;
//	double delta = 0.001;
//
//	ans = (funcPs(d, u + delta, v, cpS) - funcPs(d, u, v, cpS)) / delta;
//
//	return ans;
//}
//double diffV(int d, double u, double v, double cpS[4][4][3]) {
//
//	double ans;
//	double delta = 0.001;
//
//	ans = (funcPs(d, u, v + delta, cpS) - funcPs(d, u, v, cpS)) / delta;
//
//	return ans;
//}
//
///*********************************************************************/
//// P'(t):BEZIER微分関数
//double diffT(int d, double t, double cpC[4][3]) {
//
//	double ans;
//	double delta = 0.001;
//
//	ans = (funcPc(d, t + delta, cpC) - funcPc(d, t, cpC)) / ((t + delta) - t);
//
//	return ans;
//}
//////////////////////////////////////////////////////////////////////////
//
////使用関数BERSTEIN
//void func_BERNSTEIN(double t, double Bt[4]) {
//
//	int i;
//
//	for (i = 0; i<4; i++) {
//		Bt[i] = bernstain(t, i);
//	}
//}
//
////使用関数BERSTEIN一階微分
//void func_dtBERNSTEIN(double t, double Bt[4], double dtBt[4])
//{
//	int i;
//	double delta = 0.001;
//	for (i = 0; i<4; i++) {
//		dtBt[i] = (Bt[i] + bernstain(t + delta, i)) / delta;
//	}
//
//}
//
////使用関数P'(t)算出
//double funcPt(int d, double t, double cpC[4][3]) {
//
//	double Pt = 0;
//	int i;
//	double Bt[4];
//	double dtBt[4];
//	func_BERNSTEIN(t, Bt);
//	func_dtBERNSTEIN(t, Bt, dtBt);
//
//
//	for (i = 0; i <= 3; i++) {
//		Pt += dtBt[i] * (cpC[i][d]);
//	}
//	return Pt;
//}
//
////P''(t)算出
//void CalPtt(double Point3d[4][3], double t, double Ptt[3])
//{
//
//	double delta = 0.001;
//	int d;
//
//
//	for (d = 0; d <= 2; d++) {
//		Ptt[d] = (funcPt(d, t + delta, Point3d) - funcPt(d, t, Point3d)) / delta;
//	}
//}
//
////使用関数BEZIER_CURVE
//void func_BEZIER_C(double t, double Point3f[4][3], double vec2d[3], double tan_line2d[3]) {
//
//	int d;
//
//	for (d = 0; d <= 2; d++) {
//		vec2d[d] = funcPc(d, t, Point3f);
//		tan_line2d[d] = funcPc(d, t, Point3f) + diffT(d, t, Point3f);
//	}
//}
//
////使用関数BEZIER_SURFACE
//void func_BEZIER_S(
//	double u,
//	double v,
//	double Point3f[4][4][3],
//	double vec3d[3],
//	double pu[3],
//	double pv[3]) {
//
//	int d;
//	for (d = 0; d <= 2; d++) {
//		vec3d[d] = funcPs(d, u, v, Point3f);
//		pu[d] = funcPs(d, u, v, Point3f) + diffU(d, u, v, Point3f);
//		pv[d] = funcPs(d, u, v, Point3f) + diffV(d, u, v, Point3f);
//	}
//}
//#endif