#include "BezierCurve.h"

#define MODEL_POLYGONS 50
#define EDGE_POLYGONS  10

#define _USE_MATH_DEFINES
#include <math.h>

BezierCurve::BezierCurve(
	MATRIX _ctrlPoints,
	short _nCtrlPoints,
	XMFLOAT4 _modelColor,
	XMFLOAT4 _polyColor)
	: nCtrlPoints(_nCtrlPoints),
	drawTangentVec(false),
	drawSecdiffVec(false),
	drawCtrlCPoints(true),
	drawCtrlPointsCLine(false),
	IsCreatedModel(false),
	modelColor(_modelColor), 
	polyColor(_polyColor)
{
	__super::SetCtrlPoints(_ctrlPoints, _nCtrlPoints);
	SetTargetPos(nCtrlPoints);
	CreModel();
	CrePoly();
}

BezierCurve::BezierCurve(MATRIX _passPoints, VECTOR _t, short _nPassPoints,
	XMFLOAT4 _modelColor,	XMFLOAT4 _polyColor)
	:modelColor(_modelColor), polyColor(_polyColor)
{
	nCtrlPoints = 4;

	ctrlPoints = vm->New(3, nCtrlPoints);

	SetCtrlPoints(_passPoints, _t, _nPassPoints);

	CreModel();
	CrePoly();
}

BezierCurve::BezierCurve(const BezierCurve & bc, 
	XMFLOAT4 _modelColor, XMFLOAT4 _polyColor)
	: drawTangentVec(false),
	drawSecdiffVec(false),
	drawCtrlCPoints(true),
	drawCtrlPointsCLine(false),
	IsCreatedModel(false),
	modelColor(_modelColor),
	polyColor(_polyColor)
{
	nCtrlPoints = bc.nCtrlPoints;
	__super::SetCtrlPoints(bc.ctrlPoints, bc.nCtrlPoints);
	SetTargetPos(nCtrlPoints);
	CreModel();
	CrePoly();
}

BezierCurve& BezierCurve::operator=(const BezierCurve &bc)
{
	//
	
	vm->Delete(ctrlPoints);

	//
	nCtrlPoints = bc.nCtrlPoints;
	__super::SetCtrlPoints(bc.ctrlPoints, bc.nCtrlPoints);
	SetTargetPos(nCtrlPoints);
	CreModel();
	CrePoly();

	return *this;
}

BezierCurve::~BezierCurve()
{
	
}

// 通過点から制御点生成
void  BezierCurve::SetCtrlPoints(MATRIX _passPoints, VECTOR _t, int _nPassPoints)
{
	__super::PassPoints2CtrlPoints(_passPoints, _t, _nPassPoints, ctrlPoints, nCtrlPoints);

	/*if (_nPassPoints - 4 != 0)
		return;*/
	
	/*if (fabs(_t[0]) < TOLERANCE)
		for (int i = 0; i < 3; i++) ctrlPoints[i][0] = _passPoints[i][0];

	if (fabs(_t[3] - 1.0) < TOLERANCE)
		for (int i = 0; i < 3; i++) ctrlPoints[i][3] = _passPoints[i][3];

	MATRIX a = vm->New(2, 2);
	MATRIX xMat = vm->New(3, 2);
	VECTOR b, x;
	
	b = vm->New(2);
	x = vm->New(2);
	VECTOR bsFunc1 = vm->New(_nPassPoints);
	VECTOR bsFunc2 = vm->New(_nPassPoints);

	if (Bernstain(_t[1], bsFunc1, 0) != 0)
		return;

	a[0][0] = bsFunc1[1];
	a[0][1] = bsFunc1[2];

	if (Bernstain(_t[2], bsFunc2, 0) != 0)
		return;

	a[1][0] = bsFunc2[1];
	a[1][1] = bsFunc2[2];
	
	for (int i = 0; i < 3; i++)
	{
		b[0] = _passPoints[i][1] - bsFunc1[0] * _passPoints[i][0] - bsFunc1[3] * _passPoints[i][3];
		b[1] = _passPoints[i][2] - bsFunc2[0] * _passPoints[i][0] - bsFunc2[3] * _passPoints[i][3];

		vm->SimultLinearEquations(a, b, x, 2);

		xMat[i][0] = x[0];
		xMat[i][1] = x[1];
	}

	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 2; j++)
			ctrlPoints[i][j+1] = xMat[i][j];

	vm->Delete(a);
	vm->Delete(xMat);
	vm->Delete(b);
	vm->Delete(x);
	vm->Delete(bsFunc1);
	vm->Delete(bsFunc2);*/
}

short BezierCurve::t2xyz(SCALAR _t, VECTOR _p, int _dim)
{
	VECTOR bsFunc = vm->New(nCtrlPoints);

	if (Bernstain(_t, bsFunc, _dim) != 0)
	{
		return 1;
	}

	for (int i = 0; i < 3; i++)
	{
		_p[i] = 0.0;

		for (int j = 0; j < nCtrlPoints; j++) {
			_p[i] += bsFunc[j] * ctrlPoints[i][j];
		}
	}

	//Set OutPut
	vm->Delete(bsFunc);

	return 0;
}

short BezierCurve::CreModel()
{
	UINT nVertices = MODEL_POLYGONS;
	UINT nIndices = MODEL_POLYGONS;

	D3DVertex* vertices = new D3DVertex[nVertices];
	WORD* indices = new WORD[nIndices];

	SCALAR t;
	VECTOR pos = vm->New(3);

	SCALAR ymin = 0.0;
	SCALAR ymax = 0.0;

	for (UINT i = 0; i < nVertices; i++)
	{
		t = i / (SCALAR)(MODEL_POLYGONS - 1);

		if (Position(t, pos) != 0)
		{
			return 1;
		}

		vertices[i].Pos = XMFLOAT3(pos[0], pos[1], pos[2]);

		if (ymin > pos[1]) ymin = pos[1];
		if (ymax < pos[1]) ymax = pos[1];

		if (Tangent(t, pos) != 0)
		{
			return 1;
		}


	}

	vm->Delete(pos);

	// Indices
	for (UINT i = 0; i < nIndices; i++)
	{
		indices[i] = i;
	}

	// Vertices Color
	for (UINT i = 0; i < nVertices; i++)
	{
		vertices[i].Color = modelColor;
	}

	if (FAILED(cvModel.Init(vertices, nVertices, indices, nIndices, D3D_PRIMITIVE_TOPOLOGY_LINESTRIP)))
	{
		return 2;
	}

	delete vertices;
	delete indices;

	IsCreatedModel = true;

	return 0;
}

short BezierCurve::CrePoly()
{
	//Clear Array
	polyLine.clear();
	ctrlCPoints.clear();
	posPoints.clear();
	tangentPoints.clear();
	secdiffPoints.clear();

	VECTOR start = vm->New(3);
	VECTOR end = vm->New(3);
	VECTOR tangent = vm->New(3);
	VECTOR secdiff = vm->New(3);
	

	// 制御点列の表示
	for (int i = 0; i < nCtrlPoints - 1; i++)
	{
		for (int j = 0; j < 3; j++) {
			start[j] = ctrlPoints[j][i];
			end[j] = ctrlPoints[j][i + 1];
		}

		polyLine.push_back(new CLine(start, end, XMFLOAT4(0.4f, 0.1f, 0.7f, 1.0f)));
		
		if (i == 0)
			ctrlCPoints.push_back(new CPoint(start, XMFLOAT4(0.4f, 0.1f, 0.7f, 1.0f)));

		ctrlCPoints.push_back(new CPoint(end, XMFLOAT4(0.4f, 0.1f, 0.7f, 1.0f)));

	}

	SCALAR t;
	

	for (int i = 0; i < EDGE_POLYGONS; i++)
	{
		t = i / (SCALAR)(EDGE_POLYGONS - 1);

		/*if (Position(t, start) != 0)
		{
			return 1;
		}*/

		//posPoints.push_back(new CPoint(start, XMFLOAT4(0.4f, 0.1f, 0.7f, 1.0f)));

		if (drawTangentVec)
		{
			if (Tangent(t, tangent) != 0)
				return 1;

			vm->Normalize(tangent, tangent, 3);
			for (int j = 0; j < 3; j++)
			{
				tangent[j] = start[j] + tangent[j];
			}
			tangentPoints.push_back(new CPoint(tangent, XMFLOAT4(0.4f, 0.1f, 0.7f, 1.0f)));
		}
	

		
		
		if (drawSecdiffVec)
		{
			if (Secdiff(t, secdiff) != 0)
				return 1;

			vm->Normalize(secdiff, secdiff, 3);
			for (int j = 0; j < 3; j++)
			{
				secdiff[j] = start[j] + secdiff[j];
			}

			secdiffPoints.push_back(new  CPoint(secdiff, XMFLOAT4(0.4f, 0.1f, 0.7f, 1.0f)));
		}

		/*if(i != 0)
			polyEdges.push_back(new CLine(start, end, XMFLOAT4(0.4f, 0.1f, 0.7f, 1.0f)));
		
		for (int ii = 0; ii < 3; ii++)
			end[ii] = start[ii];*/
	}
	vm->Delete(start);
	vm->Delete(end);
	vm->Delete(tangent);
	return 0;
}

short BezierCurve::Position(
	const SCALAR _t,	//時間t
	VECTOR _pos			//位置座標
){
	return t2xyz(_t, _pos);
}

short BezierCurve::Tangent
(
	const SCALAR _t,	//時間t
	VECTOR _tan			//位置座標
) {
	return t2xyz(_t, _tan, 1);
}

short BezierCurve::Secdiff(
	const SCALAR _t, 
	VECTOR _sec
){
	return t2xyz(_t, _sec, 2);
}

void BezierCurve::Draw()
{
	cvModel.Draw();

	if (drawCtrlCPoints)
	{
		std::vector<CPoint*>::iterator it = ctrlCPoints.begin();
		while (it != ctrlCPoints.end())
		{
			(*it)->Draw();
			++it;
		}
	}

	if (drawCtrlPointsCLine)
	{
		std::vector<CLine*>::iterator it = polyLine.begin();
		while (it != polyLine.end())
		{
			(*it)->Draw();
			++it;
		}
	}

	if (drawTangentVec)
	{
		std::vector<CPoint*>::iterator start = posPoints.begin();
		std::vector<CPoint*>::iterator end = tangentPoints.begin();

		while (start != posPoints.end())
		{
			CLine((*start), (*end), XMFLOAT4(0.4f, 0.1f, 0.7f, 1.0f)).Draw();
			//(*it)->Draw();
			++start;
			++end;
		}
	}

	if (drawSecdiffVec)
	{
		std::vector<CPoint*>::iterator start = posPoints.begin();
		std::vector<CPoint*>::iterator end = secdiffPoints.begin();

		while (start != posPoints.end())
		{
			CLine((*start), (*end), XMFLOAT4(0.4f, 0.8f, 0.2f, 1.0f)).Draw();
			//(*it)->Draw();
			++start;
			++end;
		}
	}
}

bool BezierCurve::CalLength(SCALAR* _length)
{
	SCALAR length = 0;

	MATRIX allpos = CalAllPostion(MODEL_POLYGONS);

	if (allpos == nullptr)
		return false;

	VECTOR vec = vm->New(3);
	
	for (UINT i = 0; i < MODEL_POLYGONS-1; i++)
	{
		vm->Sub(allpos[i + 1], allpos[i - 1], vec, 3);
		length += vm->Length(vec, 3);
	}

	delete allpos;

	*_length = length;
	return true;
}

short BezierCurve::Discretize(short _nPartitions, std::vector<VECTOR>* _discreatePoints)
{

	return 0;
}

short BezierCurve::Split(SCALAR _t, BezierCurve** _curveA, BezierCurve** _curveB) const
{
	MATRIX cpA = vm->New(3, 4);
	MATRIX cpB = vm->New(3, 4);

	__super::Split(_t, ctrlPoints, cpA, cpB);

	*_curveA = new BezierCurve(cpA, 4, XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	*_curveB = new BezierCurve(cpB, 4, XMFLOAT4(0.0f, 1.0f, 0.0f, 1.0f), XMFLOAT4(0.0f, 1.0f, 0.0f, 1.0f));

	vm->Delete(cpA);
	vm->Delete(cpB);

	return 0;
}

MATRIX BezierCurve::CalAllPostion(UINT ac)
{
	SCALAR t;
	MATRIX	allpos = vm->New(ac, 3);

	for (UINT i = 0; i < ac; i++)
	{
		t = i / (SCALAR)(ac - 1);

		if (Position(t, allpos[i]) != 0)
		{
			return nullptr;
		}
	}

	return allpos;
}

short BezierCurve::Offset(double _distance, VECTOR _t, UINT _nPassPoints)
{

	double downZ[3] = { 0, 0, -1 };

	VECTOR posVec = vm->New(3);
	VECTOR tanVec = vm->New(3);
	VECTOR offsetVec = vm->New(3);
	MATRIX passPoints = vm->New(3, _nPassPoints);
	SCALAR norum;

	//Offset passpoints on _t 
	for (size_t i = 0; i < _nPassPoints; i++)
	{
		Position(_t[i], posVec);
		Tangent(_t[i], tanVec);
		vm->Normalize(tanVec, tanVec, 3);
		vm->Cross(tanVec, downZ, offsetVec);
		
		norum = pow(vm->Length(offsetVec, 3), _distance);
		for (size_t j = 0; j < 3; j++)
		{
			offsetVec[j] /= norum;
		}

		vm->Add(posVec, offsetVec, offsetVec, 3);

		for (size_t j = 0; j < 3; j++)
		{
			passPoints[j][i] = offsetVec[j];
		}
	}

	SetCtrlPoints(passPoints, _t, _nPassPoints);

	CreModel();
	CrePoly();

	delete posVec;
	delete tanVec;
	delete offsetVec;
	delete passPoints;

	return 0;
}

short BezierCurve::NearestPoint(VECTOR _anyPoint, VECTOR _nearestPoint, SCALAR* _t, SCALAR* _distance)
{
	VECTOR toOut0 = vm->New(3);
	VECTOR toOut1 = vm->New(3);
	VECTOR tan0 = vm->New(3);
	VECTOR tan1 = vm->New(3);
	VECTOR nearToOut = vm->New(3);

	SCALAR leftPrm, rightPrm, midPrm; // 
	SCALAR appliedLeftPrm, appliedRightPrm;
	SCALAR angle0, angle1, angle2;
	SCALAR appliedAngle0, appliedAngle1;

	int candidCount = 0;	// 候補パラメータの数
	SCALAR candidDist;		// 候補


	// 最初に8分割行い、どの区間に該当するか
	for (int i = 0; i < 7; i++)
	{
		leftPrm =  i / 7.0;
		rightPrm = (i + 1) / 7.0;

		Position(leftPrm, toOut0);
		vm->Sub(_anyPoint, toOut0, toOut0, 3);
		Tangent(leftPrm, tan0);

		Position(rightPrm, toOut1);
		vm->Sub(_anyPoint, toOut1, toOut1, 3);
		Tangent(rightPrm, tan1);

		angle0 = acos(vm->Dot(toOut0, tan0, 3) / (vm->Length(toOut0, 3) * vm->Length(tan0, 3)));
		angle1 = acos(vm->Dot(toOut1, tan1, 3) / (vm->Length(toOut1, 3) * vm->Length(tan1, 3)));

		// 該当区間であるか
		if ((angle0 <= M_PI / 2.0 && angle1 > M_PI / 2.0) ||
			(angle0 > M_PI / 2.0 && angle1 <= M_PI / 2.0))
		{
			midPrm = (leftPrm + rightPrm) / 2.0;
			Position(midPrm, toOut0);
			vm->Sub(_anyPoint, toOut0, toOut0, 3);

			// 2個以上の候補 -> 最短距離のパラメータを適用
			if ((candidCount == 0) ||
				(candidCount >= 1 && vm->Length(toOut0, 3) < candidDist))
			{
				candidDist = vm->Length(toOut0, 3);
				appliedLeftPrm = leftPrm;
				appliedRightPrm = rightPrm;
				appliedAngle0 = angle0;
				appliedAngle1 = angle1;
			}

			candidCount++;
		}
	}

	// 該当区間があった場合
	// -> 接線ベクトルと直交する位置の特定
	if (candidCount > 0)
	{
		leftPrm = appliedLeftPrm;
		rightPrm = appliedRightPrm;
		angle0 = appliedAngle0;
		angle1 = appliedAngle1;

		// パラメータがほぼ等しくなるまで計算
		while (rightPrm - leftPrm >= 0.0001)
		{
			midPrm = (leftPrm + rightPrm) / 2.0;

			Position(midPrm, toOut0);
			vm->Sub(_anyPoint, toOut0, toOut0, 3);
			Tangent(midPrm, tan0);

			angle2 = acos(vm->Dot(toOut0, tan0, 3) / (vm->Length(toOut0, 3) * vm->Length(tan0, 3)));

			if (angle0 <= angle1)
			{
				if (angle2 <= M_PI / 2.0)
				{
					leftPrm = midPrm;
					rightPrm = rightPrm;
					continue;
				}
				else {
					leftPrm = leftPrm;
					rightPrm = midPrm;
					continue;
				}
			}
			else {
				if (angle2 <= M_PI / 2.0)
				{
					leftPrm = leftPrm;
					rightPrm = midPrm;
					continue;
				}
				else {
					leftPrm = midPrm;
					rightPrm = rightPrm;
					continue;
				}
			}
		}

		*_t = (leftPrm + rightPrm) / 2.0;

		if (*_t < 0) *_t = 0;
		else if (*_t > 1) *_t = 1;
	}

	// 該当区間がなかった場合
	// -> 最近点は両端のどちらか
	else {
		SCALAR dist1, dist2;

		Position(0, toOut0);
		vm->Sub(_anyPoint, toOut0, toOut0, 3);
		dist1 = vm->Length(toOut0, 3);

		Position(1, toOut0);
		vm->Sub(_anyPoint, toOut0, toOut0, 3);
		dist2 = vm->Length(toOut0, 3);

		*_t = (dist1 <= dist2) ? 0 : 1;
	}

	if (_nearestPoint != NULL) Position(*_t, _nearestPoint);

	vm->Sub(_nearestPoint, _anyPoint, nearToOut, 3);

	if (_distance != NULL) *_distance = vm->Length(nearToOut, 3);

	vm->Delete(toOut0);
	vm->Delete(toOut1);
	vm->Delete(tan0);
	vm->Delete(tan1);
	vm->Delete(nearToOut);

	return 0;
}

short BezierCurve::GetMMBox(MMBox** mmbox)
{
	return __super::GetMMBox(mmbox, nCtrlPoints);
}

short BezierCurve::Triming(SCALAR _t0, SCALAR _t1, BezierCurve** _trmcurve)
{
	BezierCurve *trmcurve, *a, *b, *c;

	// _t0 == _t1 
	if (fabs(_t0 - _t1) < TOLERANCE)
		return 1;

	if (0 > _t0 || 1 <= _t0)
		return 2;

	if (0 >= _t1 || 1 < _t1)
		return 3;

	if (_t0 > _t1)
	{
		SCALAR tmp = _t1;
		_t1 = _t0;
		_t0 = _t1;
	}

	_t1 = (_t1 - _t0) / (1 - _t0);

	Split(_t0, &a, &b);
	b->Split(_t1, _trmcurve, &c);

	delete a;
	delete b;
	delete c;

	return 0;
}