#pragma once
#include "Entity.h"

class CLine;
class BezierCurve;

typedef BezierCurve UVCurve;

// トリムループ
// 曲面からトリミングする際に使用する
class Loop :
	public Entity
{
private:
	std::vector<UVCurve*> coedge;
	Model loopModel;
public:
	Loop();
	// Loop(CIRCLE);
	
	~Loop();

	// UV曲線をトリムループに追加
	void add(UVCurve* _uvcurve);

	// UV直線を曲線として取り込む
	void add(MATRIX _uv);
	
	// ループ中の曲線のUV位置群を返す
	void UVPosition(SCALAR _t, MATRIX _uv);
	
	bool IsClosed();
	
	
};

