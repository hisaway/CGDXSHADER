/*
Programeed : Takumi Adachi / C&G SYSTEMS 2017.8.30
*/
#include "BSplineBase.h"
#include "MMBox.h"
#include <algorithm>

// 制御点列の設定
short CBSplineBase::SetCtrlPoints(MATRIX _ctrlPoints, short _nCtrlPoints)
{
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < _nCtrlPoints; j++)
		{
			ctrlPoints[i][j] = _ctrlPoints[i][j];

			updateCtrlPoints[i][j] = ctrlPoints[i][j];
		}
	}

	return 0;
}

// 通過点からノットベクトルを得る
short CBSplineBase::PassPoints2Knot(MATRIX _passPoints, short _nPassPoints, short _order, VECTOR _knot, short _nKnots)
{
	// 通過点間の距離からノットベクトルを設定する
	VECTOR point0 = vm->New(3);
	VECTOR point1 = vm->New(3);
	VECTOR edge = vm->New(3);
	VECTOR lens = vm->New(_nPassPoints - 1);

	SCALAR totalLen = 0.0;
	SCALAR procsLen = 0.0;

	for (int i = 0; i < _nPassPoints - 1; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			point0[j] = _passPoints[j][i];
			point1[j] = _passPoints[j][i + 1];
			vm->Sub(point1, point0, edge, 3);
			lens[i] = vm->Length(edge, 3);
		}
	}

	for (int i = 0; i < _nPassPoints - 1; i++)
	{
		totalLen += lens[i];
	}

	for (int i = 0; i < _nPassPoints - 1; i++)
	{
		lens[i] *= (SCALAR)_nPassPoints / totalLen;
	}

	for (int i = 0; i < _nKnots; i++)
	{
		if (i >= _order && i <= _nKnots - _order)
		{
			procsLen += lens[i - _order];
		}

		_knot[i] = procsLen;
	}

	vm->Delete(point0);
	vm->Delete(point1);
	vm->Delete(edge);
	vm->Delete(lens);

	return 0;
}

// 通過点から制御点列を得る
short CBSplineBase::PassPoints2CtrlPoints(MATRIX _passPoints, short _nPassPoints, short _order, VECTOR _knot, short _nKnots, MATRIX _ctrlPoints, short _nCtrlPoints)
{
	// 制御点の座標を解とする連立一次方程式を解く
	MATRIX coeff0 = vm->New(_nCtrlPoints, _nCtrlPoints);
	VECTOR coeff1 = vm->New(_nCtrlPoints);
	VECTOR solu = vm->New(_nCtrlPoints);
	VECTOR bFunc = vm->New(_nCtrlPoints);

	for (int i = 0; i < 3; i++)
	{
		// 係数ベクトル a
		coeff0[0][0] = 1.0;
		coeff0[_nCtrlPoints - 1][_nCtrlPoints - 1] = 1.0;

		BasisFunc(_knot[0], _order, _order, _nCtrlPoints, _knot, _nKnots, bFunc, 2);
		for (int j = 0; j < _nCtrlPoints; j++)
		{
			coeff0[1][j] = bFunc[j];
		}

		BasisFunc(_knot[_nKnots - 1], _order, _order, _nCtrlPoints, _knot, _nKnots, bFunc, 2);
		for (int j = 0; j < _nCtrlPoints; j++)
		{
			coeff0[_nCtrlPoints - 2][j] = bFunc[j];
		}

		for (int j = 0; j < _nPassPoints - 2; j++)
		{
			BasisFunc(_knot[_order + j], _order, _order, _nCtrlPoints, _knot, _nKnots, bFunc);

			for (int k = 0; k < _nCtrlPoints; k++)
			{
				coeff0[j + 2][k] = bFunc[k];
			}
		}

		// 係数ベクトル b
		coeff1[0] = _passPoints[i][0];
		coeff1[_nCtrlPoints - 1] = _passPoints[i][_nPassPoints - 1];

		coeff1[1] = 0.0;
		coeff1[_nCtrlPoints - 2] = 0.0;

		for (int j = 0; j < _nPassPoints - 2; j++)
		{
			coeff1[j + 2] = _passPoints[i][j + 1];
		}

		// 解を求める
		vm->SimultLinearEquations(coeff0, coeff1, solu, _nCtrlPoints);

		for (int j = 0; j < _nCtrlPoints; j++)
		{
			_ctrlPoints[i][j] = solu[j];
		}
	}

	vm->Delete(coeff0);
	vm->Delete(coeff1);
	vm->Delete(solu);
	vm->Delete(bFunc);

	return 0;
}

// 基底関数
short CBSplineBase::BasisFunc(
	SCALAR _t,
	short _order,
	short _reqOrder,
	short _nCtrlPoints,
	VECTOR _knot,
	short _nKnots,
	VECTOR _blendFunc,
	short _diffTimes
) {
	if (_knot == NULL)
	{
		return 1;
	}

	// 入力引数が適切であるか
	if (_t < _knot[0] || _knot[_nKnots - 1] < _t)
	{
		return 2;
	}

	if (_reqOrder < 1 || _order < _reqOrder)
	{
		return 3;
	}

	if (_blendFunc == NULL)
	{
		return 4;
	}

	if (_diffTimes < 0 || _diffTimes > 2)
	{
		return 5;
	}

	// 混ぜ合わせ関数 算出用行列
	MATRIX blendMatD0 = vm->New(_order, _nKnots - 1);		// 0回微分値
	MATRIX blendMatD1 = vm->New(_order, _nKnots - 1);		// 1回微分値
	MATRIX blendMatD2 = vm->New(_order, _nKnots - 1);		// 2回微分値

	if (blendMatD0 == NULL || blendMatD1 == NULL || blendMatD2 == NULL)
	{
		return 6;
	}

	// 境界パラメータへの対処
	if (_t == _knot[_nKnots - 1])
	{
		_t -= 10e-12;
	}

	// 該当ノット区間の探索
	short start = 0;
	short end = _nKnots;
	short mid = (start + end) / 2;

	while (mid != start)
	{
		if (_t < _knot[mid]) end = mid;
		else				start = mid;
		mid = (start + end) / 2;
	}

	// 1階の基底関数値
	blendMatD0[0][start] = 1.0;

	// M階の基底関数値を求める (by de Boor Cox 漸化式)
	SCALAR b01, b02, b11, b12, b21, b22;
	for (int k = 2; k <= _reqOrder; k++)
	{
		for (int j = start - k + 1; j <= start; j++)
		{
			b01 = b02 = b11 = b12 = b21 = b22 = 0.0;

			if (j + k - 1 < _nKnots)
			{
				if (_knot[j] != _knot[j + k - 1])
				{
					b01 = (_t - _knot[j]) / (_knot[j + k - 1] - _knot[j]) * blendMatD0[k - 2][j];

					if (_diffTimes >= 1) b11 = (k - 2) / (_knot[j + k - 1] - _knot[j]) * blendMatD0[k - 2][j];

					if (_diffTimes == 2) b21 = (k - 2) / (_knot[j + k - 1] - _knot[j]) * blendMatD1[k - 2][j];
				}
			}

			if (j + k < _nKnots)
			{
				if (_knot[j + 1] != _knot[j + k])
				{
					b02 = (_knot[j + k] - _t) / (_knot[j + k] - _knot[j + 1]) * blendMatD0[k - 2][j + 1];

					if (_diffTimes >= 1) b12 = -(k - 2) / (_knot[j + k] - _knot[j + 1]) * blendMatD0[k - 2][j + 1];

					if (_diffTimes == 2) b22 = -(k - 2) / (_knot[j + k] - _knot[j + 1]) * blendMatD1[k - 2][j + 1];
				}
			}

			blendMatD0[k - 1][j] = b01 + b02;
			blendMatD1[k - 1][j] = b11 + b12;
			blendMatD2[k - 1][j] = b21 + b22;
		}
	}

	// 求めるべき基底関数値
	for (int j = 0; j < _nCtrlPoints; j++)
	{
		switch (_diffTimes)
		{
		case 0: _blendFunc[j] = blendMatD0[_reqOrder - 1][j]; break;
		case 1: _blendFunc[j] = blendMatD1[_reqOrder - 1][j]; break;
		case 2: _blendFunc[j] = blendMatD2[_reqOrder - 1][j]; break;
		}
	}

	vm->Delete(blendMatD0);
	vm->Delete(blendMatD1);
	vm->Delete(blendMatD2);

	return 0;
}

// 点の全消去
short CBSplineBase::ClearPoints(std::vector<CPoint*>* _pPoints)
{
	if (_pPoints->size() > 0)
	{
		std::vector<CPoint*>::iterator it = _pPoints->begin();
		while (it != _pPoints->end())
		{
			delete (*it);
			++it;
		}

		_pPoints->clear();
	}

	return 0;
}

// エッジの全消去
short CBSplineBase::ClearEdges(std::vector<CLine*>* _pEdges)
{
	if (_pEdges->size() > 0)
	{
		std::vector<CLine*>::iterator it = _pEdges->begin();
		while (it != _pEdges->end())
		{
			delete (*it);
			++it;
		}

		_pEdges->clear();
	}

	return 0;
}

// 離散点パラメータの取得
short CBSplineBase::GetDisptParams(VECTOR _knot, short _nKnots, short _nPartitions, std::vector<SCALAR>* _disptParams)
{
	if (_nPartitions <= 0)
	{
		return 1;
	}

	// 重複なしのノットを取得する
	std::vector<SCALAR> knotParams;
	GetUnDuplicatedKnots(_knot, _nKnots, &knotParams);

	// 離散点のパラメータを取得する
	_disptParams->push_back(_knot[0]);

	SCALAR interval;
	for (unsigned int i = 1; i < knotParams.size(); i++)
	{
		interval = (knotParams[i] - knotParams[i - 1]) / _nPartitions;

		for (unsigned int j = 0; j < _nPartitions; j++)
		{
			_disptParams->push_back(knotParams[i - 1] + interval * (j + 1));
		}
	}

	return 0;
}

// ノットの挿入
short CBSplineBase::InsertKnot(
	SCALAR _t,
	VECTOR _knot,
	short _nKnots,
	VECTOR _newKnot,
	short* _insertIdx
) {
	// 挿入インデックスを決める
	if (_t < _knot[0])
	{
		*_insertIdx = 0;
	}
	else if (_knot[_nKnots - 1] < _t)
	{
		*_insertIdx = _nKnots;
	}
	else {
		for (int i = 0; i < _nKnots - 1; i++)
		{
			if (_knot[i] <= _t && _t <= _knot[i + 1])
			{
				*_insertIdx = i + 1;
			}
		}
	}

	// 挿入インデックスを元にノットの挿入を行う
	bool inserted = false;
	for (int i = 0; i < _nKnots + 1; i++)
	{
		if (i == *_insertIdx)
		{
			_newKnot[i] = _t;
			inserted = true;
			continue;
		}
		_newKnot[i] = (!inserted) ? _knot[i] : _knot[i - 1];
	}

	return 0;
}

// 重複なしのノットの取得
short CBSplineBase::GetUnDuplicatedKnots(VECTOR _knot, short _nKnots, std::vector<SCALAR>* _knotParams)
{
	_knotParams->push_back(_knot[0]);

	for (int i = 1; i < _nKnots; i++)
	{
		if (_knot[i] - _knot[i - 1] >= 10e-6)
		{
			_knotParams->push_back(_knot[i]);
		}
	}

	return 0;
}

// MMボックス取得
short CBSplineBase::GetMMBox(MMBox** _mmbox, short _nCtrlPoints)
{
	VECTOR minPos = vm->New(3);
	VECTOR maxPos = vm->New(3);

	std::vector<SCALAR> xs, ys, zs;

	for (int i = 0; i < _nCtrlPoints; i++)
	{
		xs.push_back(ctrlPoints[0][i]);
		ys.push_back(ctrlPoints[1][i]);
		zs.push_back(ctrlPoints[2][i]);
	}

	std::sort(xs.begin(), xs.end());
	std::sort(ys.begin(), ys.end());
	std::sort(zs.begin(), zs.end());

	minPos[0] = xs.front();
	minPos[1] = ys.front();
	minPos[2] = zs.front();

	maxPos[0] = xs.back();
	maxPos[1] = ys.back();
	maxPos[2] = zs.back();

	*_mmbox = new MMBox(minPos, maxPos);

	vm->Delete(minPos);
	vm->Delete(maxPos);

	return 0;
}

// 制御点の平行移動
short CBSplineBase::Shift(VECTOR _amount, short _nCtrlPoints)
{
	MATRIX shiftedCtrlPoints = vm->New(4, _nCtrlPoints);

	for (int i = 0; i < _nCtrlPoints; i++)
	{
		shiftedCtrlPoints[0][i] = ctrlPoints[0][i] + _amount[0] * ctrlPoints[3][i];
		shiftedCtrlPoints[1][i] = ctrlPoints[1][i] + _amount[1] * ctrlPoints[3][i];
		shiftedCtrlPoints[2][i] = ctrlPoints[2][i] + _amount[2] * ctrlPoints[3][i];
		shiftedCtrlPoints[3][i] = ctrlPoints[3][i];
	}

	SetCtrlPoints(shiftedCtrlPoints, _nCtrlPoints);

	vm->Delete(shiftedCtrlPoints);

	return 0;
}