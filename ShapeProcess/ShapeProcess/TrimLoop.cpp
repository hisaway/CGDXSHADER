#include "TrimLoop.h"
#include "BezierCurve.h"

Loop::Loop()
{

}


Loop::~Loop()
{

}

//直線を曲線としてトリムループに加える
void Loop::add(MATRIX _uv)
{
	MATRIX ctrlpoints = vm->New(3, 4);
	for (int i = 0; i < 4; i++)
	{
		if (i == 0 || i == 3)
		{
			for (int j = 0; j < 3; j++)
			{
				ctrlpoints[j][i] = _uv[j][i];
			}
			continue;
		}
		else
		{
			for (int j = 0; j < 3; j++)
			{
				ctrlpoints[j][i] = (_uv[j][3] - _uv[j][0]) * i / 3.0;
			}
		}
	}

	BezierCurve* bc = new BezierCurve(ctrlpoints, 4);
	coedge.push_back(bc);
	vm->Delete(ctrlpoints);
}

void Loop::UVPosition(SCALAR t, MATRIX _uv)
{
	/*for (auto AT : coedge)
	{
		AT
	}*/
}

//曲線をトリムループに追加
void Loop::add(UVCurve* _uvcurve)
{
	coedge.push_back(_uvcurve);
}

bool Loop::IsClosed()
{
	// Do Anything

	return true;
}