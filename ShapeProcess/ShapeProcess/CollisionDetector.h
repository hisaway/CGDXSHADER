/*
	Programeed	: Takumi Adachi / C&G SYSTEMS 2017.08.30
	Modified	: Yuki Hisae	/ C&G SYSTEMS 2018.03.27  
*/

#pragma once

#include <vector>
#include <map>

class MMBox;
struct IntersectMMBoxPair;

// 衝突判定
class CollisionDetector
{
private:
	std::map<short, MMBox*> mmboxes;

	// ミニマクスボックスの干渉判定
	short CheckMMBoxPair(MMBox* _box1, MMBox* _box2);

public:
	// コンストラクタ
	CollisionDetector();

	// デストラクタ
	~CollisionDetector();

	//参照
	MMBox* GetMMBox(short id);

	// 追加
	void Add(short id, MMBox* object);

	// 削除
	void Delete(short id);

	// 全消去
	void Clear();

	// 衝突判定
	short Check(std::vector<IntersectMMBoxPair>* pairs);
};