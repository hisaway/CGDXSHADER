//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ で生成されたインクルード ファイル。
// ShapeProcess.rc で使用
//
#define IDR_MENU1                       101
#define IDI_ICON1                       102
#define ID_40001                        40001
#define ID_40002                        40002
#define ID_40003                        40003
#define ID_40004                        40004
#define MC2_00001                       40005
#define MC2_00002                       40006
#define MC2_00003                       40007
#define MC2_00006_1                     40008
#define MC2_00006_0                     40009
#define ID_40011                        40011
#define MC2_00000                       40012
#define ID_40013                        40013
#define MC2_000011                      40014
#define ID_40015                        40015
#define MC2_000041                      40016
#define ID_40017                        40017
#define MCP_000091                      40018
#define MC2_000091                      40019
#define ID_40020                        40020
#define MC2_000081                      40021
#define OpenFile                        40022
#define ID_40023                        40023
#define IDD_SplitBS                     40025
#define ID_40026                        40026
#define ID_TrmBS                        40027
#define ID_CD                           40028
#define ID_SplitBS                      40029
#define ID_40030                        40030
#define ID_40031                        40031
#define ID_                             40032
#define ID_CalI                         40033
#define ID_IP                           40034
#define ID_40035                        40035
#define ID_BS2BS                        40036
#define ID_40037                        40037
#define ID_IsoCv                        40038
#define ID_Menu                         40039
#define ID_Surf                         40040
#define ID_ScanPoints                   40041

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        103
#define _APS_NEXT_COMMAND_VALUE         40042
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
