/*
Programeed : Takumi Adachi / C&G SYSTEMS 2017.8.30
*/

#include "Entity.h"

Entity::Entity() 
	: minPos(nullptr),maxPos(nullptr)
{
	minPos = vm->New(3);
	maxPos = vm->New(3);
}

Entity::~Entity() 
{
	vm->Delete(minPos);
	vm->Delete(maxPos);
}