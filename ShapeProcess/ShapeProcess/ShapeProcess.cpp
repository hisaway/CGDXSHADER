/*
Programeed : Takumi Adachi / C&G SYSTEMS 2017.8.30
*/

#include "ShapeProcess.h"
#include "BSplineCurve.h"
#include "BSplineSurface.h"
#include "BezierCurve.h"
#include "BezierSurface.h"

#include "MMBox.h"

#include <vector>
#include <map>
#include <algorithm>

CollisionDetector ShapeProcess::collisDetector;

// 曲線 - 曲面 の交点の算出（ボックス干渉法）
short ShapeProcess::IntersectionPointByMMBox(
	CBSplineCurve* curve, 
	CBSplineSurface* surface, 
	VECTOR intersectPoint)
{
	// 分割曲線・曲面
	std::map<short, CBSplineCurve*> splitCurve;
	std::map<short, CBSplineSurface*> splitSurface;

	// 1分割で生成される曲線・曲面
	CBSplineCurve*		bisplitCurve[2];
	CBSplineSurface*	bisplitSurface[4];

	// 曲線・曲面のボックス
	MMBox* curveBoxes[10];
	MMBox* surfaceBoxes[100];

	// 曲線・曲面のパラメータ
	SCALAR curveParamStart = curve->MinParam();
	SCALAR curveParamStep = (curve->MaxParam() - curve->MinParam()) / 10.0;

	SCALAR surfaceParamStart[2] = {
		surface->MinUParam(),
		surface->MinVParam()
	};

	SCALAR surfaceParamStep[2] = {
		(surface->MaxUParam() - surface->MinUParam()) / 10.0,
		(surface->MaxVParam() - surface->MinVParam()) / 10.0
	};


	// 分割曲線とそのボックスを追加する
	{
		CBSplineCurve* firstSplits[10];

		firstSplits[0] = curve;
		for(int i = 0; i < 9; i++)
		{
			firstSplits[i]->Split(
				(i + 1) * curveParamStep + curveParamStart,
				&firstSplits[i],
				&firstSplits[i + 1]
			);
		}

		for(int i = 0; i < 10; i++)
		{
			splitCurve.insert(std::make_pair(i, firstSplits[i]));

			firstSplits[i]->GetMMBox(&curveBoxes[i]);
			collisDetector.Add(i, curveBoxes[i]);
		}
	}
	
	// 分割曲面とそのボックスを追加する
	{
		CBSplineSurface* firstSplits[10][10];

		firstSplits[0][0] = surface;
		for(int i = 0; i < 9; i++)
		{
			firstSplits[0][i]->Split(
				(i + 1) * surfaceParamStep[0] + surfaceParamStart[0],
				true,
				&firstSplits[0][i],
				&firstSplits[0][i + 1]
			);
		}
		for(int i = 0; i < 10; i++)
		{
			for(int j = 0; j < 9; j++)
			{
				firstSplits[j][i]->Split(
					(j + 1) * surfaceParamStep[1] + surfaceParamStart[1],
					false,
					&firstSplits[j][i],
					&firstSplits[j + 1][i]
				);
			}
		}

		for(int i = 0; i < 100; i++)
		{
			splitSurface.insert(std::make_pair(i + 100, firstSplits[i / 10][i % 10]));

			firstSplits[i / 10][i % 10]->GetMMBox(&surfaceBoxes[i]);
			collisDetector.Add(i + 100, surfaceBoxes[i]);
		}
	}

	// 干渉ボックスのペア
	std::vector<IntersectMMBoxPair> intersectPairs;

	// 干渉ボックスの ID
	std::vector<short> intersectIds;

	// 試行回数
	int trialCount = 0;

	while(true)
	{
		// 今までの追加分の干渉判定
		collisDetector.Check(&intersectPairs);

		// 曲線 - 曲面 の交わりがない -> 終了
		if( intersectPairs.size() == 0 )
		{
			return 1;
		}

		// 干渉ペアから干渉ボックスの ID を抜き出す
		{
			std::vector<IntersectMMBoxPair>::iterator it = intersectPairs.begin();
			while( it != intersectPairs.end() )
			{
				if( it->Id1() < trialCount * 500 + 10 && it->Id2() > trialCount * 500 + 99 )
				{
					intersectIds.push_back(it->Id1());
					intersectIds.push_back(it->Id2());
				}
				++it;
			}
		}

		// 干渉ボックスの ID 重複を消す
		std::sort(intersectIds.begin(), intersectIds.end());
		intersectIds.erase(std::unique(intersectIds.begin(), intersectIds.end()), intersectIds.end());

		// 干渉ペアを囲むボックスの対角線が十分に小さい or 試行回数が十分に大きい
		// -> 収束計算終了
		if( intersectPairs[0].Length() < 0.1 || trialCount >= 20 )
		{
			intersectPoint[0] = intersectPairs[0].Center()[0];
			intersectPoint[1] = intersectPairs[0].Center()[1];
			intersectPoint[2] = intersectPairs[0].Center()[2];
			break;
		}

		// 干渉ペア全消去
		intersectPairs.clear();

		// 干渉判定対象のボックス全消去
		collisDetector.Clear();

		// 干渉ボックスの分割を行い、それらを再び干渉判定の対象として追加する
		{
			short nCurveBoxes = 0;
			short nSurfaceBoxes = 0;

			SCALAR minParam;
			SCALAR maxParam;

			std::vector<short>::iterator it = intersectIds.begin();
			while( it != intersectIds.end() )
			{
				// 曲線
				if( *it < trialCount * 500 + 100 )
				{
					minParam = splitCurve[*it]->MinParam();
					maxParam = splitCurve[*it]->MaxParam();

					splitCurve[*it]->
						Split((minParam + maxParam) / 2.0, &bisplitCurve[0], &bisplitCurve[1]);

					splitCurve.erase(*it);

					splitCurve.insert(std::make_pair((trialCount + 1) * 500 + nCurveBoxes + 0, bisplitCurve[0]));
					splitCurve.insert(std::make_pair((trialCount + 1) * 500 + nCurveBoxes + 1, bisplitCurve[1]));

					bisplitCurve[0]->GetMMBox(&curveBoxes[0]);
					bisplitCurve[1]->GetMMBox(&curveBoxes[1]);

					collisDetector.Add((trialCount + 1) * 500 + nCurveBoxes + 0, curveBoxes[0]);
					collisDetector.Add((trialCount + 1) * 500 + nCurveBoxes + 1, curveBoxes[1]);

					nCurveBoxes += 2;
				}

				// 曲面
				else {
					minParam = splitSurface[*it]->MinUParam();
					maxParam = splitSurface[*it]->MaxUParam();

					splitSurface[*it]->
						Split((minParam + maxParam) / 2.0, true, &bisplitSurface[0], &bisplitSurface[2]);

					splitSurface.erase(*it);

					minParam = bisplitSurface[0]->MinVParam();
					maxParam = bisplitSurface[0]->MaxVParam();

					bisplitSurface[0]->
						Split((minParam + maxParam) / 2.0, false, &bisplitSurface[0], &bisplitSurface[1]);

					minParam = bisplitSurface[2]->MinVParam();
					maxParam = bisplitSurface[2]->MaxVParam();

					bisplitSurface[2]->
						Split((minParam + maxParam) / 2.0, false, &bisplitSurface[2], &bisplitSurface[3]);

					splitSurface.insert(std::make_pair((trialCount + 1) * 500 + 100 + nSurfaceBoxes + 0, bisplitSurface[0]));
					splitSurface.insert(std::make_pair((trialCount + 1) * 500 + 100 + nSurfaceBoxes + 1, bisplitSurface[1]));
					splitSurface.insert(std::make_pair((trialCount + 1) * 500 + 100 + nSurfaceBoxes + 2, bisplitSurface[2]));
					splitSurface.insert(std::make_pair((trialCount + 1) * 500 + 100 + nSurfaceBoxes + 3, bisplitSurface[3]));

					bisplitSurface[0]->GetMMBox(&surfaceBoxes[0]);
					bisplitSurface[1]->GetMMBox(&surfaceBoxes[1]);
					bisplitSurface[2]->GetMMBox(&surfaceBoxes[2]);
					bisplitSurface[3]->GetMMBox(&surfaceBoxes[3]);

					collisDetector.Add((trialCount + 1) * 500 + 100 + nSurfaceBoxes + 0, surfaceBoxes[0]);
					collisDetector.Add((trialCount + 1) * 500 + 100 + nSurfaceBoxes + 1, surfaceBoxes[1]);
					collisDetector.Add((trialCount + 1) * 500 + 100 + nSurfaceBoxes + 2, surfaceBoxes[2]);
					collisDetector.Add((trialCount + 1) * 500 + 100 + nSurfaceBoxes + 3, surfaceBoxes[3]);

					nSurfaceBoxes += 4;
				}

				++it;
			}

			intersectIds.clear();
		}

		trialCount++;
	}

	return 0;
}



short ShapeProcess::IntersectionPointByMMBox(
	const BezierCurve& curve,
	const BezierSurface& surface, 
	VECTOR intersectPoint
)
{
	// 分割曲線・曲面
	std::map<short, BezierCurve*> splitCurve;
	std::map<short, BezierSurface*> splitSurface;

	// 1分割で生成される曲線・曲面
	BezierCurve*	bisplitCurve[2];
	BezierSurface*	bisplitSurface[4];

	// 曲線・曲面のボックス
	MMBox* curveBoxes[10];
	MMBox* surfaceBoxes[100];

	// 曲線・曲面の分割数
	SCALAR nDiv = 10.0;

	// 分割曲線とそのボックスを追加する
	{
		BezierCurve* firstSplits[10];
		firstSplits[0] = new BezierCurve(curve);

		for (int i = 0; i < nDiv-1; i++)
		{
			firstSplits[i]->Split(
				1.0 / (nDiv-i),
				&firstSplits[i],
				&firstSplits[i + 1]
			);
		}

		for (int i = 0; i < 10; i++)
		{
			splitCurve.insert(std::make_pair(i, firstSplits[i]));

			firstSplits[i]->GetMMBox(&curveBoxes[i]);
			collisDetector.Add(i, curveBoxes[i]);
		}
	}

	// 分割曲面とそのボックスを追加する
	{
		BezierSurface* firstSplits[10][10];
		firstSplits[0][0] = new BezierSurface(surface);

		for (int i = 0; i < nDiv-1; i++)
		{
			firstSplits[0][i]->Split(
				1.0 / (nDiv - i),
				true,
				&firstSplits[0][i],
				&firstSplits[0][i + 1]
			);
		}
		for (int i = 0; i < 10; i++)
		{
			for (int j = 0; j < 9; j++)
			{
				firstSplits[j][i]->Split(
					1.0 / (nDiv - i),
					false,
					&firstSplits[j][i],
					&firstSplits[j + 1][i]
				);
			}
		}

		for (int i = 0; i < 100; i++)
		{
			splitSurface.insert(std::make_pair(i + 100, firstSplits[i / 10][i % 10]));

			firstSplits[i / 10][i % 10]->GetMMBox(&surfaceBoxes[i]);
			collisDetector.Add(i + 100, surfaceBoxes[i]);
		}
	}

	// 干渉ボックスのペア
	std::vector<IntersectMMBoxPair> intersectPairs;

	// 干渉ボックスの ID
	std::vector<short> intersectIds;

	// 試行回数
	int trialCount = 0;

	while (true)
	{
		// 今までの追加分の干渉判定
		collisDetector.Check(&intersectPairs);

		// 曲線 - 曲面 の交わりがない -> 終了
		if (intersectPairs.size() == 0)
		{
			return 1;
		}

		// 干渉ペアから干渉ボックスの ID を抜き出す
		{
			std::vector<IntersectMMBoxPair>::iterator it = intersectPairs.begin();
			while (it != intersectPairs.end())
			{
				if (it->Id1() < trialCount * 500 + 10 && it->Id2() > trialCount * 500 + 99)
				{
					intersectIds.push_back(it->Id1());
					intersectIds.push_back(it->Id2());
				}
				++it;
			}
		}

		// 干渉ボックスの ID 重複を消す
		std::sort(intersectIds.begin(), intersectIds.end());
		intersectIds.erase(std::unique(intersectIds.begin(), intersectIds.end()), intersectIds.end());

		// 干渉ペアを囲むボックスの対角線が十分に小さい or 試行回数が十分に大きい
		// -> 収束計算終了
		if (intersectPairs[0].Length() < 0.1 || trialCount >= 20)
		{
			intersectPoint[0] = intersectPairs[0].Center()[0];
			intersectPoint[1] = intersectPairs[0].Center()[1];
			intersectPoint[2] = intersectPairs[0].Center()[2];
			break;
		}

		// 干渉ペア全消去
		intersectPairs.clear();

		// 干渉判定対象のボックス全消去
		collisDetector.Clear();

		// 干渉ボックスの分割を行い、それらを再び干渉判定の対象として追加する
		{
			short nCurveBoxes = 0;
			short nSurfaceBoxes = 0;

			std::vector<short>::iterator it = intersectIds.begin();
			while (it != intersectIds.end())
			{
				// 曲線
				if (*it < trialCount * 500 + 100)
				{
					splitCurve[*it]->
						Split(1.0 / 2.0, &bisplitCurve[0], &bisplitCurve[1]);

					splitCurve.erase(*it);

					splitCurve.insert(std::make_pair((trialCount + 1) * 500 + nCurveBoxes + 0, bisplitCurve[0]));
					splitCurve.insert(std::make_pair((trialCount + 1) * 500 + nCurveBoxes + 1, bisplitCurve[1]));

					bisplitCurve[0]->GetMMBox(&curveBoxes[0]);
					bisplitCurve[1]->GetMMBox(&curveBoxes[1]);

					collisDetector.Add((trialCount + 1) * 500 + nCurveBoxes + 0, curveBoxes[0]);
					collisDetector.Add((trialCount + 1) * 500 + nCurveBoxes + 1, curveBoxes[1]);

					nCurveBoxes += 2;
				}

				// 曲面
				else {

					splitSurface[*it]->
						Split(1.0 / 2.0, true, &bisplitSurface[0], &bisplitSurface[2]);

					splitSurface.erase(*it);

					bisplitSurface[0]->
						Split(1.0 / 2.0, false, &bisplitSurface[0], &bisplitSurface[1]);


					bisplitSurface[2]->
						Split(1.0 / 2.0, false, &bisplitSurface[2], &bisplitSurface[3]);

					splitSurface.insert(std::make_pair((trialCount + 1) * 500 + 100 + nSurfaceBoxes + 0, bisplitSurface[0]));
					splitSurface.insert(std::make_pair((trialCount + 1) * 500 + 100 + nSurfaceBoxes + 1, bisplitSurface[1]));
					splitSurface.insert(std::make_pair((trialCount + 1) * 500 + 100 + nSurfaceBoxes + 2, bisplitSurface[2]));
					splitSurface.insert(std::make_pair((trialCount + 1) * 500 + 100 + nSurfaceBoxes + 3, bisplitSurface[3]));

					bisplitSurface[0]->GetMMBox(&surfaceBoxes[0]);
					bisplitSurface[1]->GetMMBox(&surfaceBoxes[1]);
					bisplitSurface[2]->GetMMBox(&surfaceBoxes[2]);
					bisplitSurface[3]->GetMMBox(&surfaceBoxes[3]);

					collisDetector.Add((trialCount + 1) * 500 + 100 + nSurfaceBoxes + 0, surfaceBoxes[0]);
					collisDetector.Add((trialCount + 1) * 500 + 100 + nSurfaceBoxes + 1, surfaceBoxes[1]);
					collisDetector.Add((trialCount + 1) * 500 + 100 + nSurfaceBoxes + 2, surfaceBoxes[2]);
					collisDetector.Add((trialCount + 1) * 500 + 100 + nSurfaceBoxes + 3, surfaceBoxes[3]);

					nSurfaceBoxes += 4;
				}

				++it;
			}

			intersectIds.clear();
		}
		trialCount++;
	}

	return 0;

}

short ShapeProcess::IntersectionPointByMMBox(
	const BezierCurve& curve,
	const BezierSurface& surface,
	VECTOR* intersectPoint
){
	*intersectPoint = vm->New(3);
	return ShapeProcess::IntersectionPointByMMBox(curve, surface, *intersectPoint);
}