/*
Programeed : Takumi Adachi / C&G SYSTEMS 2017.8.30
*/

#include "CollisionDetector.h"
#include "MMBox.h"

// コンストラクタ
CollisionDetector::CollisionDetector()
{
}

// デストラクタ
CollisionDetector::~CollisionDetector()
{
}

MMBox * CollisionDetector::GetMMBox(short id)
{
	return mmboxes[id];
}

// 追加
void CollisionDetector::Add(short id, MMBox* object)
{
	mmboxes.insert(std::make_pair(id, object));
}

// 削除
void CollisionDetector::Delete(short id)
{
	mmboxes.erase(id);
}

// 全消去
void CollisionDetector::Clear()
{
	mmboxes.clear();
}

// 衝突判定
short CollisionDetector::Check(std::vector<IntersectMMBoxPair>* pairs)
{
	std::map<short, MMBox*>::iterator it1, it2;
	short result = 0;

	for(it1 = mmboxes.begin(); it1 != mmboxes.end(); ++it1)
	{
		for(it2 = mmboxes.begin(); it2 != mmboxes.end(); ++it2)
		{
			// 自分自身の組と既判定の組は判定対象外
			if ((*it1).first >= (*it2).first) continue;

			// 干渉判定
			result = CheckMMBoxPair((*it1).second, (*it2).second);
			if( result == 0 || result == 2 || result == 3 ) continue;

			// 干渉ペア追加
			IntersectMMBoxPair intersectPair((*it1).first, (*it1).second, (*it2).first, (*it2).second);
			pairs->push_back(intersectPair);
		}
	}

	return 0;
}

// ミニマクスボックスの干渉判定
// ０：干渉しない 
// １：干渉する
// ２：ボックスＡがボックスＢを含む
// ３：ボックスＢがボックスＡを含む
short CollisionDetector::CheckMMBoxPair(MMBox* _box1, MMBox* _box2)
{
    if( _box1->Max()[0] < _box2->Min()[0] || _box2->Max()[0] < _box1->Min()[0] ||
		_box1->Max()[1] < _box2->Min()[1] || _box2->Max()[1] < _box1->Min()[1] ||
		_box1->Max()[2] < _box2->Min()[2] || _box2->Max()[2] < _box1->Min()[2] )
	{
		return 0;
	}

    if( _box1->Min()[0] <= _box2->Min()[0] && _box2->Max()[0] <= _box1->Max()[0] &&
		_box1->Min()[1] <= _box2->Min()[1] && _box2->Max()[1] <= _box1->Max()[1] &&
		_box1->Min()[2] <= _box2->Min()[2] && _box2->Max()[2] <= _box1->Max()[2] )
	{
		return 2;
	}

    if( _box2->Min()[0] <= _box1->Min()[0] && _box1->Max()[0] <= _box2->Max()[0] &&
		_box2->Min()[1] <= _box1->Min()[1] && _box1->Max()[1] <= _box2->Max()[1] &&
		_box2->Min()[2] <= _box1->Min()[2] && _box1->Max()[2] <= _box2->Max()[2] )
	{
		return 3;
	}

	return 1;
}