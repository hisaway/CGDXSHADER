/*
	BezierBase.h
	Programmed : Yuki Hisae / C&G SYSTEMS 
*/ 

#pragma once
#include "Entity.h"

class CPoint;
class CLine;
class MMBox;

class BezierBase :
	public Entity
{
public:
	BezierBase(XMFLOAT4 _modelColor = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f),
		XMFLOAT4 _polyColor = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f));
	~BezierBase();

	void Draw();

	short CreModel();
	Model bsModel;

private:
	XMFLOAT4 modelColor;
	XMFLOAT4 polyColor;

protected :
	MATRIX ctrlPoints;

	virtual int SetCtrlPoints(MATRIX, int );
	
	short PassPoints2CtrlPoints(
		MATRIX _passPoints, 
		VECTOR _t, 
		short _nPassPoints, 
		MATRIX _ctrlPoints, 
		short _nCtrlPoints);

	// P!:階乗関数
	int factorial(int x);

	// nCi:コンビネーション関数
	int combination(int n, int i);

	// Bernstain基底関数
	short Bernstain(SCALAR _t,  VECTOR _bsfunc, int _dim = 0) const;

	//MMBoxの生成
	short GetMMBox(MMBox** _mmbox, short _ctrlPoints);

	//Entityのターゲットを設定
	short SetTargetPos(short _ctrlPoints);

	// 制御点の分割
	static short Split(const SCALAR _t, const MATRIX& _ctrlPoints, MATRIX& _ctrlPointsA, MATRIX& _ctrlPointsB);
};

