/*
	Programed : Takumi Adachi / C&G SYSTEMS 2017.8.30
*/
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include "File.h"
#include "Point.h"
#include "BSplineCurve.h"
#include "BSplineSurface.h"

// コンストラクタ
File::File()
{
	formatEntityTypes.push_back("POINT");
	formatEntityTypes.push_back("EDGE");
	formatEntityTypes.push_back("CURVE");
	formatEntityTypes.push_back("SURFACE");
}

// デストラクタ
File::~File()
{
}

// エンティティ読み込み
Entity* File::Load(char* path)
{
	std::ifstream ifs(path);
	std::string lineStr;

	// 読み込み失敗時
	if( ifs.fail() )
	{
		DisplayErrorMessage(ERR_UNREADABLE, path);
		return 0;
	}

	std::vector<std::string> dataStrings;
	size_t tabPos;
	bool entityTypeDetected = false;
	short entityType = ENT_POINT;

	while( std::getline(ifs, lineStr) )
	{
		// 空行 -> 読みとばす
		if( lineStr.length() == 0 )
		{
			continue;
		}

		// エンティティタイプ未特定時
		if( !entityTypeDetected )
		{
			if( lineStr.find('[') != -1 )
			{
				// タイプ特定
				std::vector<std::string>::iterator it = formatEntityTypes.begin();
				while( it != formatEntityTypes.end() )
				{
					if( lineStr.find((*it)) != -1 )
					{
						entityTypeDetected = true;
						break;
					}

					entityType++;
					++it;
				}

				// タイプを検出できなかった -> エラー
				if( !entityTypeDetected )
				{
					DisplayErrorMessage(ERR_INCORRECT_FORMAT, path);
					return 0;
				}
			}
		}

		// エンティティタイプ特定済
		else {
			// [...] はコメント扱い
			if( lineStr.find('[') != -1 )
			{
				continue;
			}

			// 全ての tab を space に置き換え
			tabPos = lineStr.find('\t');
			while( tabPos != -1 )
			{
				lineStr.replace(tabPos, 1, " ");
				tabPos = lineStr.find('\t');
			}

			// space による文字列の分割
			StringSplit(lineStr, ' ', &dataStrings);
		}
	}

	return CreateEntity(entityType, dataStrings);
}

// 文字列分割
void File::StringSplit(const std::string &str, char sep, std::vector<std::string>* splitStrs)
{
    std::stringstream ss(str);
    std::string buffer;

    while( std::getline(ss, buffer, sep) )
	{
        splitStrs->push_back(buffer);
    }
}

// 先頭のデータを削除
void File::PopFront(std::vector<std::string>* dataStrings, short num)
{
	dataStrings->erase(dataStrings->begin(), dataStrings->begin() + num);
}

// エンティティの作成
Entity* File::CreateEntity(short entityType, std::vector<std::string>& dataStrings)
{
	switch (entityType)
	{
	case ENT_POINT:
		return 0;

	case ENT_EDGE:
		return 0;

	case ENT_CURVE:
		return CreateCurve(&dataStrings);

	case ENT_SURFACE:
		return CreateSurface(&dataStrings);
	}
}

// 曲線生成
CBSplineCurve* File::CreateCurve(std::vector<std::string>* dataStrings)
{
	short order = atoi(dataStrings->at(0).data());
	short nCtrlPoints = atoi(dataStrings->at(1).data());
	short nKnots = order + nCtrlPoints;

	MATRIX ctrlPoints = vm->New(3, nCtrlPoints);
	VECTOR knot		  = vm->New(nKnots);

	XMFLOAT4 modelColor, polyColor;

	PopFront(dataStrings, 2);

	for(int i = 0; i < nCtrlPoints; i++)
	{
		for(int j = 0; j < 3; j++)
		{
			ctrlPoints[j][i] = (float)atof(dataStrings->at(i * 4 + j).data());
		}
	}
	PopFront(dataStrings, nCtrlPoints * 4);

	for(int i = 0; i < nKnots; i++)
	{
		knot[i] = (float)atof(dataStrings->at(i).data());
	}
	PopFront(dataStrings, nKnots);

	modelColor.x = (float)atof(dataStrings->at(0).data());
	modelColor.y = (float)atof(dataStrings->at(1).data());
	modelColor.z = (float)atof(dataStrings->at(2).data());
	modelColor.w = (float)atof(dataStrings->at(3).data());
	PopFront(dataStrings, 4);

	polyColor.x = (float)atof(dataStrings->at(0).data());
	polyColor.y = (float)atof(dataStrings->at(1).data());
	polyColor.z = (float)atof(dataStrings->at(2).data());
	polyColor.w = (float)atof(dataStrings->at(3).data());
	PopFront(dataStrings, 4);

	CBSplineCurve* curve = new CBSplineCurve(order, ctrlPoints, nCtrlPoints, knot, modelColor, polyColor);

	vm->Delete(ctrlPoints);
	vm->Delete(knot);

	return curve;
}

// 曲面生成
CBSplineSurface* File::CreateSurface(std::vector<std::string>* dataStrings)
{
	VECTOR order		= vm->New(2);
	VECTOR nCtrlPoints  = vm->New(2);
	VECTOR nKnots		= vm->New(2);

	order[0] = atoi(dataStrings->at(0).data());
	order[1] = atoi(dataStrings->at(1).data());

	nCtrlPoints[0] = atoi(dataStrings->at(2).data());
	nCtrlPoints[1] = atoi(dataStrings->at(3).data());

	nKnots[0] = order[0] + nCtrlPoints[0];
	nKnots[1] = order[1] + nCtrlPoints[1];

	MATRIX ctrlPoints = vm->New(3, nCtrlPoints[0] * nCtrlPoints[1]);
	VECTOR uKnot = vm->New(nKnots[0]);
	VECTOR vKnot = vm->New(nKnots[1]);

	XMFLOAT4 modelColor, polyColor;

	PopFront(dataStrings, 4);

	for(int i = 0; i < nCtrlPoints[0] * nCtrlPoints[1]; i++)
	{
		for(int j = 0; j < 3; j++)
		{
			ctrlPoints[j][i] = (float)atof(dataStrings->at(i * 4 + j).data());
		}
	}
	PopFront(dataStrings, nCtrlPoints[0] * nCtrlPoints[1] * 4);

	for(int i = 0; i < nKnots[0]; i++)
	{
		uKnot[i] = (float)atof(dataStrings->at(i).data());
	}
	PopFront(dataStrings, nKnots[0]);

	for(int i = 0; i < nKnots[1]; i++)
	{
		vKnot[i] = (float)atof(dataStrings->at(i).data());
	}
	PopFront(dataStrings, nKnots[1]);

	modelColor.x = (float)atof(dataStrings->at(0).data());
	modelColor.y = (float)atof(dataStrings->at(1).data());
	modelColor.z = (float)atof(dataStrings->at(2).data());
	modelColor.w = (float)atof(dataStrings->at(3).data());
	PopFront(dataStrings, 4);

	polyColor.x = (float)atof(dataStrings->at(0).data());
	polyColor.y = (float)atof(dataStrings->at(1).data());
	polyColor.z = (float)atof(dataStrings->at(2).data());
	polyColor.w = (float)atof(dataStrings->at(3).data());
	PopFront(dataStrings, 4);

	CBSplineSurface* surface = new CBSplineSurface(order, ctrlPoints, nCtrlPoints, uKnot, vKnot, modelColor, polyColor);

	vm->Delete(ctrlPoints);
	vm->Delete(uKnot);
	vm->Delete(vKnot);

	vm->Delete(order);
	vm->Delete(nCtrlPoints);
	vm->Delete(nKnots);

	return surface;
}

// エラー表示
void File::DisplayErrorMessage(ERROR_TYPE errorType, char* path)
{
	std::string message;
	wchar_t* wmes = new wchar_t[128];
	size_t length = 0;

	switch(errorType)
	{
		case ERR_UNREADABLE:
			message = "File not found:\n";
			break;

		case ERR_INCORRECT_FORMAT:
			message = "Entity data format is incorrect:\n";
			break;
	}
	message += path;

	mbstowcs_s(&length, wmes, 128, message.data(), _TRUNCATE);

	//app->ErrorMessage(wmes, "File", MB_OK);

	delete[] wmes;
}