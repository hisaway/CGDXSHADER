/*
Programeed : Takumi Adachi / C&G SYSTEMS 2017.8.30
*/

#include "BSplineSurface.h"
#include "BSplineCurve.h"
#include "ShapeProcess.h"
#include "Point.h"
#include "Line.h"

#define MODEL_POLYGONS 10

// コンストラクタ : [in] 階数, 制御点, ノットベクトル
CBSplineSurface::CBSplineSurface(
	VECTOR _order,
	MATRIX _ctrlPoints,
	VECTOR _nCtrlPoints,
	VECTOR _uKnot,
	VECTOR _vKnot,
	XMFLOAT4 _modelColor,
	XMFLOAT4 _meshColor
) :
	modelColor(_modelColor),
	meshColor(_meshColor),
	drawWireframe(true),
	drawMeshPoints(true),
	drawMeshEdges(true),
	createdModel(false),
	origin(ORIGIN_STANDARD)
{
	order = vm->New(2);
	nCtrlPoints = vm->New(2);
	nKnots = vm->New(2);
	nUpdateCtrlPoints = vm->New(2);
	nUpdateKnots = vm->New(2);

	for (int i = 0; i < 2; i++)
	{
		order[i] = _order[i];
		nCtrlPoints[i] = _nCtrlPoints[i];

		if (order[i] < 1)
		{
			order[i] = 1;
		}

		if (nCtrlPoints[i] < order[i])
		{
			nCtrlPoints[i] = order[i];
		}
	}

	for (int i = 0; i < 2; i++)
	{
		nKnots[i] = order[i] + nCtrlPoints[i];
	}

	updateCtrlPoints = vm->New(4, nCtrlPoints[0] * nCtrlPoints[1]);
	updateUKnot = vm->New(nKnots[0]);
	updateVKnot = vm->New(nKnots[1]);

	Setup(order, _ctrlPoints, nCtrlPoints, _uKnot, _vKnot);
}

// コンストラクタ : [in] 通過点
CBSplineSurface::CBSplineSurface(
	MATRIX _passPoints,
	short _nUPassPoints,
	short _nVPassPoints,
	XMFLOAT4 _modelColor,
	XMFLOAT4 _meshColor
) :
	modelColor(_modelColor),
	meshColor(_meshColor),
	drawWireframe(true),
	drawMeshPoints(true),
	drawMeshEdges(true),
	createdModel(false),
	origin(ORIGIN_PASSPOINTS)
{
	order = vm->New(2);
	nCtrlPoints = vm->New(2);
	nKnots = vm->New(2);

	// (階数) = 4
	order[0] = order[1] = 4;

	// (制御点数) = (通過点数) + 2
	nCtrlPoints[0] = _nUPassPoints + 2;
	nCtrlPoints[1] = _nVPassPoints + 2;

	// ノット数
	nKnots[0] = order[0] + nCtrlPoints[0];
	nKnots[1] = order[1] + nCtrlPoints[1];

	// 制御点列
	ctrlPoints = vm->New(4, nCtrlPoints[0] * nCtrlPoints[1]);

	// ノットベクトル
	uKnot = vm->New(nKnots[0]);
	vKnot = vm->New(nKnots[1]);

	SetKnot(_passPoints, _nUPassPoints, _nVPassPoints);
	SetCtrlPoints(_passPoints, _nUPassPoints, _nVPassPoints);
}

// デストラクタ
CBSplineSurface::~CBSplineSurface()
{
	Release();

	if (createdModel)
	{
		ReleaseModel();
	}

	vm->Delete(order);
	vm->Delete(nCtrlPoints);
	vm->Delete(nKnots);
	vm->Delete(nUpdateCtrlPoints);
	vm->Delete(nUpdateKnots);

	vm->Delete(updateCtrlPoints);
	vm->Delete(updateUKnot);
	vm->Delete(updateVKnot);
}

// セットアップ
void CBSplineSurface::Setup(VECTOR _order, MATRIX _ctrlPoints, VECTOR _nCtrlPoints, VECTOR _uKnot, VECTOR _vKnot)
{
	for (int i = 0; i < 2; i++)
	{
		order[i] = _order[i];
		nCtrlPoints[i] = _nCtrlPoints[i];

		if (order[i] < 1)
		{
			order[i] = 1;
		}

		if (nCtrlPoints[i] < order[i])
		{
			nCtrlPoints[i] = order[i];
		}
	}

	// 制御点列の初期化
	ctrlPoints = vm->New(4, nCtrlPoints[0] * nCtrlPoints[1]);
	__super::SetCtrlPoints(_ctrlPoints, nCtrlPoints[0] * nCtrlPoints[1]);

	// ノット数
	for (int i = 0; i < 2; i++)
	{
		nKnots[i] = order[i] + nCtrlPoints[i];
	}

	// ノットベクトルの初期化
	uKnot = vm->New(nKnots[0]);
	vKnot = vm->New(nKnots[1]);
	SetKnot(_uKnot, _vKnot);

	for (int i = 0; i < 2; i++)
	{
		nUpdateCtrlPoints[i] = nCtrlPoints[i];
		nUpdateKnots[i] = nKnots[i];
	}
}

// リリース
void CBSplineSurface::Release()
{
	vm->Delete(ctrlPoints);
	vm->Delete(uKnot);
	vm->Delete(vKnot);
}

// セットアップ（表示系）
void CBSplineSurface::SetupModel()
{
	CreateModel();
	CreateMesh();
}

// リリース（表示系）
void CBSplineSurface::ReleaseModel()
{
	ClearPoints(&meshPoints);
	ClearEdges(&meshEdges);

	surfaceModel.Cleanup();
	surfaceWireframe.Cleanup();
}

// 通過点から制御点列を得る
short CBSplineSurface::PassPoints2CtrlPoints(MATRIX _passPoints, short _nPassPoints, bool _isCoordU, MATRIX _ctrlPoints, short _nCtrlPoints)
{
	if (_isCoordU) return __super::PassPoints2CtrlPoints(_passPoints, _nPassPoints, order[0], uKnot, nKnots[0], _ctrlPoints, _nCtrlPoints);
	else			return __super::PassPoints2CtrlPoints(_passPoints, _nPassPoints, order[1], vKnot, nKnots[1], _ctrlPoints, _nCtrlPoints);
}

// 制御点列の設定
short CBSplineSurface::SetCtrlPoints(MATRIX _passPoints, short _nUPassPoints, short _nVPassPoints)
{
	// U, V方向から見た通過点列
	MATRIX uCoordPassPoints = vm->New(3, _nUPassPoints);
	MATRIX vCoordPassPoints = vm->New(3, _nVPassPoints);

	// U方向アイソパラメトリック曲線群の制御点列
	std::vector<MATRIX> uCoordsCtrlPoints;

	// V方向アイソパラメトリック1曲線の制御点列
	MATRIX aVCoordCtrlPoints = vm->New(3, nCtrlPoints[1]);

	// U方向について 通過点列 -> 制御点列
	for (int i = 0; i < _nVPassPoints; i++)
	{
		// U方向アイソパラメトリック1曲線の制御点列
		MATRIX aUCoordCtrlPoints = vm->New(3, nCtrlPoints[0]);

		for (int j = 0; j < _nUPassPoints; j++)
		{
			for (int k = 0; k < 3; k++)
			{
				uCoordPassPoints[k][j] = _passPoints[k][_nVPassPoints * j + i];
			}
		}

		// 通過点列 -> 制御点列
		PassPoints2CtrlPoints(uCoordPassPoints, _nUPassPoints, true, aUCoordCtrlPoints, nCtrlPoints[0]);

		// 曲線群として追加
		uCoordsCtrlPoints.push_back(aUCoordCtrlPoints);
	}

	// V方向について 通過点列 -> 制御点列
	for (int i = 0; i < nCtrlPoints[0]; i++)
	{
		for (int j = 0; j < _nVPassPoints; j++)
		{
			for (int k = 0; k < 3; k++)
			{
				vCoordPassPoints[k][j] = uCoordsCtrlPoints[j][k][i];
			}
		}

		// 通過点列 -> 制御点列
		PassPoints2CtrlPoints(vCoordPassPoints, _nVPassPoints, false, aVCoordCtrlPoints, nCtrlPoints[1]);

		for (int j = 0; j < nCtrlPoints[1]; j++)
		{
			for (int k = 0; k < 3; k++)
			{
				ctrlPoints[k][(short)nCtrlPoints[1] * i + j] = aVCoordCtrlPoints[k][j];
			}
		}
	}

	vm->Delete(uCoordPassPoints);
	vm->Delete(vCoordPassPoints);

	std::vector<MATRIX>::iterator it = uCoordsCtrlPoints.begin();
	while (it != uCoordsCtrlPoints.end())
	{
		delete (*it);
		++it;
	}

	uCoordsCtrlPoints.clear();

	vm->Delete(aVCoordCtrlPoints);

	return 0;
}

// ノットベクトルの設定
short CBSplineSurface::SetKnot(VECTOR _uKnot, VECTOR _vKnot)
{
	for (int i = 0; i < nKnots[0]; i++)
	{
		uKnot[i] = _uKnot[i];

		updateUKnot[i] = uKnot[i];
	}

	for (int i = 0; i < nKnots[1]; i++)
	{
		vKnot[i] = _vKnot[i];

		updateVKnot[i] = vKnot[i];
	}

	return 0;
}

short CBSplineSurface::SetKnot(MATRIX _passPoints, short _nUPassPoints, short _nVPassPoints)
{
	MATRIX uCoordPassPoints = vm->New(3, _nUPassPoints);
	MATRIX vCoordPassPoints = vm->New(3, _nVPassPoints);
	VECTOR uCoordKnot = vm->New(nKnots[0]);
	VECTOR vCoordKnot = vm->New(nKnots[1]);

	// Uノットベクトルを求める
	for (int k = 0; k < _nVPassPoints; k++)
	{
		for (int i = 0; i < _nUPassPoints; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				uCoordPassPoints[j][i] = _passPoints[j][_nVPassPoints * i + k];
			}
		}

		if (__super::PassPoints2Knot(uCoordPassPoints, _nUPassPoints, order[0], uCoordKnot, nKnots[0]) != 0)
		{
			return 1;
		}

		for (int i = 0; i < nKnots[0]; i++)
		{
			uKnot[i] += uCoordKnot[i];
		}
	}

	for (int i = 0; i < nKnots[0]; i++)
	{
		uKnot[i] /= _nVPassPoints;
	}

	// Vノットベクトルを求める
	for (int k = 0; k < _nUPassPoints; k++)
	{
		for (int i = 0; i < _nVPassPoints; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				vCoordPassPoints[j][i] = _passPoints[j][_nVPassPoints * k + i];
			}
		}

		if (__super::PassPoints2Knot(vCoordPassPoints, _nVPassPoints, order[1], vCoordKnot, nKnots[1]) != 0)
		{
			return 2;
		}

		for (int i = 0; i < nKnots[1]; i++)
		{
			vKnot[i] += vCoordKnot[i];
		}
	}

	for (int i = 0; i < nKnots[1]; i++)
	{
		vKnot[i] /= _nUPassPoints;
	}

	vm->Delete(uCoordPassPoints);
	vm->Delete(vCoordPassPoints);
	vm->Delete(uCoordKnot);
	vm->Delete(vCoordKnot);

	return 0;
}

// ノットの挿入
short CBSplineSurface::InsertKnot(SCALAR _t, bool _forU, short* _insertedIndex)
{
	VECTOR newOrder;
	VECTOR newUKnot, newVKnot;
	MATRIX newCtrlPoints;
	VECTOR newNCtrlPoints;
	short insertIndex;

	newOrder = vm->New(2);
	newNCtrlPoints = vm->New(2);

	newOrder[0] = order[0];
	newOrder[1] = order[1];

	// U方向のノットの挿入
	if (_forU)
	{
		newUKnot = vm->New(nUpdateKnots[0] + 1);
		newVKnot = vm->New(nUpdateKnots[1]);
		newCtrlPoints = vm->New(4, (nUpdateCtrlPoints[0] + 1) * nUpdateCtrlPoints[1]);

		__super::InsertKnot(_t, updateUKnot, nUpdateKnots[0], newUKnot, &insertIndex);

		newNCtrlPoints[0] = nUpdateCtrlPoints[0] + 1;
		newNCtrlPoints[1] = nUpdateCtrlPoints[1];

		// 新しい制御点列を求める
		for (int k = 0; k < nUpdateCtrlPoints[1]; k++)
		{
			for (int i = 0; i < nUpdateCtrlPoints[0] + 1; i++)
			{
				SCALAR alpha;

				if (i <= insertIndex - order[0])
				{
					alpha = 1.0;
				}
				else if (insertIndex - order[0] + 1 <= i && i <= insertIndex - 1)
				{
					alpha = (_t - newUKnot[i]) / (newUKnot[i + (int)order[0]] - newUKnot[i]);
				}
				else if (insertIndex <= i)
				{
					alpha = 0.0;
				}

				for (int j = 0; j < 3; j++)
				{
					newCtrlPoints[j][(int)newNCtrlPoints[1] * i + k] =
						alpha * updateCtrlPoints[j][(int)nUpdateCtrlPoints[1] * i + k] +
						(1.0 - alpha) * updateCtrlPoints[j][(int)nUpdateCtrlPoints[1] * (i - 1) + k];
				}
			}
		}

		for (int i = 0; i < nUpdateKnots[1]; i++)
		{
			newVKnot[i] = updateVKnot[i];
		}

		nUpdateKnots[0]++;
		nUpdateCtrlPoints[0]++;
	}

	// V方向のノットの挿入
	else {
		newUKnot = vm->New(nUpdateKnots[0]);
		newVKnot = vm->New(nUpdateKnots[1] + 1);
		newCtrlPoints = vm->New(4, nUpdateCtrlPoints[0] * (nUpdateCtrlPoints[1] + 1));

		__super::InsertKnot(_t, updateVKnot, nUpdateKnots[1], newVKnot, &insertIndex);

		newNCtrlPoints[0] = nUpdateCtrlPoints[0];
		newNCtrlPoints[1] = nUpdateCtrlPoints[1] + 1;

		// 新しい制御点列を求める
		for (int k = 0; k < nUpdateCtrlPoints[0]; k++)
		{
			for (int i = 0; i < nUpdateCtrlPoints[1] + 1; i++)
			{
				SCALAR alpha;

				if (i <= insertIndex - order[1])
				{
					alpha = 1.0;
				}
				else if (insertIndex - order[1] + 1 <= i && i <= insertIndex - 1)
				{
					alpha = (_t - newVKnot[i]) / (newVKnot[i + (int)order[1]] - newVKnot[i]);
				}
				else if (insertIndex <= i)
				{
					alpha = 0.0;
				}

				for (int j = 0; j < 3; j++)
				{
					newCtrlPoints[j][(int)newNCtrlPoints[1] * k + i] =
						alpha * updateCtrlPoints[j][(int)nUpdateCtrlPoints[1] * k + i] +
						(1.0 - alpha) * updateCtrlPoints[j][(int)nUpdateCtrlPoints[1] * k + i - 1];
				}
			}
		}

		for (int i = 0; i < nUpdateKnots[0]; i++)
		{
			newUKnot[i] = updateUKnot[i];
		}

		nUpdateKnots[1]++;
		nUpdateCtrlPoints[1]++;
	}

	vm->Delete(updateUKnot);
	vm->Delete(updateVKnot);
	vm->Delete(updateCtrlPoints);

	updateUKnot = vm->New(nUpdateKnots[0]);
	updateVKnot = vm->New(nUpdateKnots[1]);
	updateCtrlPoints = vm->New(4, nUpdateCtrlPoints[0] * nUpdateCtrlPoints[1]);

	for (int i = 0; i < nUpdateKnots[0]; i++)
	{
		updateUKnot[i] = newUKnot[i];
	}
	for (int i = 0; i < nUpdateKnots[1]; i++)
	{
		updateVKnot[i] = newVKnot[i];
	}

	for (int i = 0; i < nUpdateCtrlPoints[0] * nUpdateCtrlPoints[1]; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			updateCtrlPoints[j][i] = newCtrlPoints[j][i];
		}
	}

	vm->Delete(newOrder);
	vm->Delete(newUKnot);
	vm->Delete(newVKnot);
	vm->Delete(newCtrlPoints);
	vm->Delete(newNCtrlPoints);

	if (_insertedIndex != NULL)
	{
		*_insertedIndex = insertIndex;
	}

	return 0;
}

// 基底関数
short CBSplineSurface::BasisFunc(
	SCALAR _t,
	bool _isCoordU,
	short _reqOrder,
	VECTOR _blendFunc,
	short _diffTimes
) {
	if (_isCoordU)	return __super::BasisFunc(_t, order[0], _reqOrder, nCtrlPoints[0], uKnot, nKnots[0], _blendFunc, _diffTimes);
	else			return __super::BasisFunc(_t, order[1], _reqOrder, nCtrlPoints[1], vKnot, nKnots[1], _blendFunc, _diffTimes);
}

// パラメータ (u, v) -> ベクトル (x, y, z)
short CBSplineSurface::uv2xyz(VECTOR _uv, VECTOR _p, short _uDiffTimes, short _vDiffTimes)
{
	short matSize = (nCtrlPoints[0] >= nCtrlPoints[1]) ? nCtrlPoints[0] : nCtrlPoints[1];

	VECTOR uBFuncVal = vm->New(nCtrlPoints[0]);
	VECTOR vBFuncVal = vm->New(nCtrlPoints[1]);
	MATRIX uBFuncMat = vm->New(matSize, matSize);
	MATRIX vBFuncMat = vm->New(matSize, matSize);
	MATRIX ctrlPointMat = vm->New(matSize, matSize);
	MATRIX ret1 = vm->New(matSize, matSize);
	MATRIX ret2 = vm->New(matSize, matSize);
	VECTOR n = vm->New(3);
	VECTOR d = vm->New(3);

	if (BasisFunc(_uv[0], true, order[0], uBFuncVal, _uDiffTimes) != 0) return 1;
	if (BasisFunc(_uv[1], false, order[1], vBFuncVal, _vDiffTimes) != 0) return 2;

	for (int i = 0; i < nCtrlPoints[0]; i++)
	{
		uBFuncMat[0][i] = uBFuncVal[i];
	}

	for (int i = 0; i < nCtrlPoints[1]; i++)
	{
		vBFuncMat[i][0] = vBFuncVal[i];
	}

	for (int k = 0; k < 3; k++)
	{
		// 分子
		for (int i = 0; i < nCtrlPoints[0]; i++)
		{
			for (int j = 0; j < nCtrlPoints[1]; j++)
			{
				ctrlPointMat[i][j] = ctrlPoints[k][(short)nCtrlPoints[1] * i + j];
			}
		}

		vm->Mul(uBFuncMat, ctrlPointMat, ret1, matSize, matSize);
		vm->Mul(ret1, vBFuncMat, ret2, matSize, matSize);

		n[k] = ret2[0][0];

		// 分母
		for (int i = 0; i < nCtrlPoints[0]; i++)
		{
			for (int j = 0; j < nCtrlPoints[1]; j++)
			{
				ctrlPointMat[i][j] = ctrlPoints[3][(short)nCtrlPoints[1] * i + j];
			}
		}

		vm->Mul(uBFuncMat, ctrlPointMat, ret1, matSize, matSize);
		vm->Mul(ret1, vBFuncMat, ret2, matSize, matSize);

		d[k] = ret2[0][0];

		// 分子・分母の除算
		_p[k] = n[k] / d[k];
	}

	vm->Delete(uBFuncVal);
	vm->Delete(vBFuncVal);
	vm->Delete(uBFuncMat);
	vm->Delete(vBFuncMat);
	vm->Delete(ctrlPointMat);
	vm->Delete(ret1);
	vm->Delete(ret2);
	vm->Delete(n);
	vm->Delete(d);

	return 0;
}

// 位置ベクトル
short CBSplineSurface::Position(VECTOR _uv, VECTOR _pos)
{
	return uv2xyz(_uv, _pos);
}

// 接線ベクトル
short CBSplineSurface::Tangent(VECTOR _uv, VECTOR _pu, VECTOR _pv)
{
	if (uv2xyz(_uv, _pu, 1, 0) != 0) return 1;
	if (uv2xyz(_uv, _pv, 0, 1) != 0) return 2;

	return 0;
}

// 2回微分ベクトル
short CBSplineSurface::Secdiff(VECTOR _uv, VECTOR _puu, VECTOR _pvv, VECTOR _puv)
{
	if (uv2xyz(_uv, _puu, 2, 0) != 0) return 1;
	if (uv2xyz(_uv, _pvv, 0, 2) != 0) return 2;
	if (uv2xyz(_uv, _puv, 1, 1) != 0) return 3;

	return 0;
}

// 離散点化
short CBSplineSurface::Discretize(
	short _nUPartitions,
	short _nVPartitions,
	std::vector<VECTOR>* _discretePoints,
	std::vector<std::pair<SCALAR, SCALAR>>* _parameters
)
{
	// 離散点の全消去
	_discretePoints->clear();

	SCALAR uParamStart = MinUParam();
	SCALAR uParamStep = (MaxUParam() - MinUParam()) / (_nUPartitions - 1);

	SCALAR vParamStart = MinVParam();
	SCALAR vParamStep = (MaxVParam() - MinVParam()) / (_nVPartitions - 1);

	VECTOR uv = vm->New(3);

	// パラメータから位置ベクトルに変換する
	for (int i = 0; i < _nUPartitions; i++)
	{
		for (int j = 0; j < _nVPartitions; j++)
		{
			VECTOR discretePoint = vm->New(3);

			uv[0] = i * uParamStep + uParamStart;
			uv[1] = j * vParamStep + vParamStart;

			if (_parameters != NULL)
			{
				_parameters->push_back(std::make_pair(uv[0], uv[1]));
			}

			if (Position(uv, discretePoint) != 0)
			{
				return 2;
			}

			_discretePoints->push_back(discretePoint);
		}
	}

	vm->Delete(uv);

	return 0;
}

short CBSplineSurface::DiscretizeForKnot(
	short _nUPartitions,
	short _nVPartitions,
	std::vector<VECTOR>* _discretePoints,
	std::vector<std::pair<SCALAR, SCALAR>>* _parameters
)
{
	// 離散点の全消去
	_discretePoints->clear();

	// 離散点のパラメータを取得する
	std::vector<SCALAR> disptUParams;
	std::vector<SCALAR> disptVParams;

	if (GetDisptParams(uKnot, nKnots[0], _nUPartitions, &disptUParams) != 0)
	{
		return 1;
	}

	if (GetDisptParams(vKnot, nKnots[1], _nVPartitions, &disptVParams) != 0)
	{
		return 2;
	}

	// パラメータから位置ベクトルに変換する
	VECTOR uv = vm->New(2);

	for (unsigned int i = 0; i < disptUParams.size(); i++)
	{
		for (unsigned j = 0; j < disptVParams.size(); j++)
		{
			VECTOR discretePoint = vm->New(3);

			uv[0] = disptUParams[i], uv[1] = disptVParams[j];

			if (_parameters != NULL)
			{
				_parameters->push_back(std::make_pair(uv[0], uv[1]));
			}

			if (Position(uv, discretePoint) != 0)
			{
				return 3;
			}

			_discretePoints->push_back(discretePoint);
		}
	}

	vm->Delete(uv);

	return 0;
}

// 最近点
short CBSplineSurface::NearestPoint(VECTOR _outPoint, VECTOR _uv, VECTOR _nearPoint, SCALAR* _distance)
{
	VECTOR duv = vm->New(2);
	VECTOR appliedUV = vm->New(2);
	VECTOR pos = vm->New(3);
	VECTOR pu = vm->New(3);
	VECTOR pv = vm->New(3);
	VECTOR toOut = vm->New(3);
	VECTOR nearToOut = vm->New(3);

	SCALAR puLength, pvLength;

	SCALAR uMaxPrm, uMinPrm;
	SCALAR vMaxPrm, vMinPrm;

	// 最大・最小パラメータの取得
	std::vector<SCALAR> uKnotParams, vKnotParams;

	GetUnDuplicatedKnots(uKnot, nKnots[0], &uKnotParams);
	uMaxPrm = uKnotParams.back();
	uMinPrm = uKnotParams.front();

	GetUnDuplicatedKnots(vKnot, nKnots[1], &vKnotParams);
	vMaxPrm = vKnotParams.back();
	vMinPrm = vKnotParams.front();

	// 候補距離
	SCALAR candidDist;

	// uv変化量
	SCALAR changeAmount;

	// 試行回数
	int trialCount;

	// 正射影を用いる
	for (int i = 0; i < 49; i++)
	{
		// 初期パラメータ
		_uv[0] = (i / 7) * (uMaxPrm - uMinPrm) / 6 + uMinPrm;
		_uv[1] = (i % 7) * (vMaxPrm - vMinPrm) / 6 + vMinPrm;

		// 初期変化uv
		duv[0] = 1.0, duv[1] = 1.0;

		changeAmount = sqrt(duv[0] * duv[0] + duv[1] * duv[1]);
		trialCount = 0;

		// uv変化量:小 or 試行回数:多 まで計算を行う
		while (changeAmount > 10e-3 && trialCount < 50)
		{
			Position(_uv, pos);
			Tangent(_uv, pu, pv);

			puLength = vm->Length(pu, 3);
			pvLength = vm->Length(pv, 3);

			vm->Sub(_outPoint, pos, toOut, 3);

			duv[0] = vm->Dot(toOut, pu, 3) / puLength / puLength * 0.7;
			duv[1] = vm->Dot(toOut, pv, 3) / pvLength / pvLength * 0.7;

			_uv[0] += duv[0];
			_uv[1] += duv[1];

			// uvパラメータ範囲外に到達 -> 計算打ち切り
			if (_uv[0] < uMinPrm || _uv[0] > uMaxPrm || _uv[1] < vMinPrm || _uv[1] > vMaxPrm)
			{
				break;
			}

			changeAmount = sqrt(duv[0] * duv[0] + duv[1] * duv[1]);
			trialCount++;
		}

		// パラメータ制限
		if (_uv[0] < uMinPrm) _uv[0] = uMinPrm;
		else if (_uv[0] > uMaxPrm) _uv[0] = uMaxPrm;
		if (_uv[1] < vMinPrm) _uv[1] = vMinPrm;
		else if (_uv[1] > vMaxPrm) _uv[1] = vMaxPrm;

		// 候補距離の追加
		Position(_uv, pos);
		vm->Sub(pos, _outPoint, toOut, 3);

		if ((i == 0) ||
			(i >= 1 && vm->Length(toOut, 3) < candidDist))
		{
			candidDist = vm->Length(toOut, 3);
			appliedUV[0] = _uv[0];
			appliedUV[1] = _uv[1];
		}
	}

	_uv[0] = appliedUV[0];
	_uv[1] = appliedUV[1];

	if (_nearPoint != NULL)
	{
		Position(_uv, _nearPoint);
		vm->Sub(_nearPoint, _outPoint, nearToOut, 3);

		if (_distance != NULL) *_distance = vm->Length(nearToOut, 3);
	}

	vm->Delete(duv);
	vm->Delete(appliedUV);
	vm->Delete(pos);
	vm->Delete(pu);
	vm->Delete(pv);
	vm->Delete(toOut);
	vm->Delete(nearToOut);

	return 0;
}

// 分割
short CBSplineSurface::Split(SCALAR _t, bool _forU, CBSplineSurface** surfaceA, CBSplineSurface** surfaceB)
{
	// 挿入インデックスの開始と終了の取得
	short insertBeginIndex, insertEndIndex;
	{
		short idx;
		short order = (_forU) ? this->order[0] : this->order[1];
		for (int i = 0; i < order - 1; i++)
		{
			InsertKnot(_t, _forU, &idx);

			if (i == 0) insertBeginIndex = idx;
			if (i == order - 2) insertEndIndex = idx;
		}

		Update();
	}

	VECTOR nSplitCtrlPoints;
	VECTOR nSplitKnots;

	MATRIX splitCtrlPoints;
	VECTOR splitKnot;

	// 分割曲面A
	{
		nSplitCtrlPoints = vm->New(2);
		nSplitKnots = vm->New(2);

		nSplitKnots[0] = (_forU) ? insertEndIndex + 2 : nKnots[0];
		nSplitKnots[1] = (_forU) ? nKnots[1] : insertEndIndex + 2;

		nSplitCtrlPoints[0] = (_forU) ? nSplitKnots[0] - order[0] : nCtrlPoints[0];
		nSplitCtrlPoints[1] = (_forU) ? nCtrlPoints[1] : nSplitKnots[1] - order[1];

		splitCtrlPoints = vm->New(4, nSplitCtrlPoints[0] * nSplitCtrlPoints[1]);
		splitKnot = vm->New((_forU) ? nSplitKnots[0] : nSplitKnots[1]);

		// ノット
		short endIdx = (_forU) ? nSplitKnots[0] - 1 : nSplitKnots[1] - 1;
		for (int i = 0; i < endIdx; i++)
		{
			splitKnot[i] = (_forU) ? uKnot[i] : vKnot[i];
		}
		splitKnot[endIdx] = _t;

		// 制御点
		for (int i = 0; i < 4; i++)
		{
			for (int j = 0, k = 0; j < nSplitCtrlPoints[0] * nSplitCtrlPoints[1]; j++, k++)
			{
				if (!_forU)
				{
					if (j % (short)nSplitCtrlPoints[1] == 0 && j > 0)
					{
						k += nCtrlPoints[1] - nSplitCtrlPoints[1];
					}
				}

				splitCtrlPoints[i][j] = ctrlPoints[i][k];
			}
		}

		*surfaceA = new CBSplineSurface(this->order, splitCtrlPoints, nSplitCtrlPoints,
			(_forU) ? splitKnot : this->uKnot,
			(_forU) ? this->vKnot : splitKnot,
			this->modelColor, this->meshColor);

		vm->Delete(splitCtrlPoints);
		vm->Delete(splitKnot);
	}

	// 分割曲面B
	{
		nSplitCtrlPoints = vm->New(2);
		nSplitKnots = vm->New(2);

		nSplitKnots[0] = (_forU) ? nKnots[0] - (insertBeginIndex - 1) : nKnots[0];
		nSplitKnots[1] = (_forU) ? nKnots[1] : nKnots[1] - (insertBeginIndex - 1);

		nSplitCtrlPoints[0] = (_forU) ? nSplitKnots[0] - order[0] : nCtrlPoints[0];
		nSplitCtrlPoints[1] = (_forU) ? nCtrlPoints[1] : nSplitKnots[1] - order[1];

		splitCtrlPoints = vm->New(4, nSplitCtrlPoints[0] * nSplitCtrlPoints[1]);
		splitKnot = vm->New((_forU) ? nSplitKnots[0] : nSplitKnots[1]);

		// ノット
		short endIdx = (_forU) ? nSplitKnots[0] : nSplitKnots[1];
		splitKnot[0] = _t;
		for (int i = 1; i < endIdx; i++)
		{
			splitKnot[i] = (_forU) ? uKnot[i - 1 + insertBeginIndex] : vKnot[i - 1 + insertBeginIndex];
		}

		// 制御点
		short nTotalSplitCtrlPoints = (short)nSplitCtrlPoints[0] * (short)nSplitCtrlPoints[1];
		short nTotalCtrlPoints = (short)nCtrlPoints[0] * (short)nCtrlPoints[1];
		for (int i = 0; i < 4; i++)
		{
			for (int j = 0, k = 0; j < nTotalSplitCtrlPoints; j++, k++)
			{
				if (!_forU)
				{
					if (j % (short)nSplitCtrlPoints[1] == 0 && j > 0)
					{
						k += nCtrlPoints[1] - nSplitCtrlPoints[1];
					}
				}

				splitCtrlPoints[i][nTotalSplitCtrlPoints - 1 - j] = ctrlPoints[i][nTotalCtrlPoints - 1 - k];
			}
		}

		*surfaceB = new CBSplineSurface(this->order, splitCtrlPoints, nSplitCtrlPoints,
			(_forU) ? splitKnot : this->uKnot,
			(_forU) ? this->vKnot : splitKnot,
			this->modelColor, this->meshColor);

		vm->Delete(splitCtrlPoints);
		vm->Delete(splitKnot);
	}

	return 0;
}

// MMボックス取得
short CBSplineSurface::GetMMBox(MMBox** mmbox)
{
	return __super::GetMMBox(mmbox, nCtrlPoints[0] * nCtrlPoints[1]);
}

// 曲線との交点
short CBSplineSurface::IntersectionForCurve(CBSplineCurve* curve, VECTOR intersectPoint)
{
	return ShapeProcess::IntersectionPointByMMBox(curve, this, intersectPoint);
}

// 自身に近い曲線から面上線を生成する
short CBSplineSurface::CreateCurveOnSelf(
	CBSplineCurve* _nearCurve,
	CBSplineCurve** _onXYZCurve,
	CBSplineCurve** _onUVCurve,
	SCALAR* _distances
)
{
	std::vector<SCALAR> separateParams;
	SCALAR fitTolerance = 10e-3;

	// 初期パラメータ群（10分割）セット
	for (int i = 0; i < 11; i++)
	{
		SCALAR start = _nearCurve->MinParam();
		SCALAR step = (_nearCurve->MaxParam() - _nearCurve->MinParam()) / 10.0f;
		separateParams.push_back(step * i + start);
	}

	// 近接曲線の最近点群から面上線を生成する
	{
		MATRIX passXYZPoints = vm->New(3, separateParams.size());
		MATRIX passUVPoints = vm->New(3, separateParams.size());
		SCALAR distance;

		VECTOR outXYZ = vm->New(3);
		VECTOR nearUV = vm->New(2);
		VECTOR nearXYZ = vm->New(3);

		// 各々のパラメータに対して
		for (int i = 0; i < separateParams.size(); i++)
		{
			// 位置ベクトルを求める
			if (_nearCurve->Position(separateParams[i], outXYZ) != 0)
			{
				return 1;
			}

			// 最近点算出
			this->NearestPoint(outXYZ, nearUV, nearXYZ, &distance);

			// 面上線(XYZ)の通過点とする
			passXYZPoints[0][i] = nearXYZ[0];
			passXYZPoints[1][i] = nearXYZ[1];
			passXYZPoints[2][i] = nearXYZ[2];

			// 面上線(UV)の通過点とする
			passUVPoints[0][i] = nearUV[0];
			passUVPoints[1][i] = nearUV[1];
			passUVPoints[2][i] = nearUV[2];

			// フィットトレランス更新
			if (distance > fitTolerance)
			{
				fitTolerance = distance;
			}
		}

		// 面上線(XYZ) 生成
		*_onXYZCurve = new CBSplineCurve(passXYZPoints, separateParams.size());

		// 面上線(UV) 生成
		if (_onUVCurve != NULL)
		{
			*_onUVCurve = new CBSplineCurve(passUVPoints, separateParams.size());
		}

		vm->Delete(outXYZ);
		vm->Delete(nearUV);
		vm->Delete(nearXYZ);

		vm->Delete(passXYZPoints);
		vm->Delete(passUVPoints);
	}

	// 近接曲線と面上線の距離チェック
	SCALAR minDistance = DBL_MAX;
	{
		SCALAR start = (*_onXYZCurve)->MinParam();
		SCALAR step = ((*_onXYZCurve)->MaxParam() - (*_onXYZCurve)->MinParam()) / 100.0f;

		SCALAR t;
		SCALAR distance;

		VECTOR o = vm->New(3);
		VECTOR n = vm->New(3);

		for (int i = 0; i < 101; i++)
		{
			(*_onXYZCurve)->Position(step * i + start, o);
			_nearCurve->NearestPoint(o, &t, n, &distance);

			if (_distances != NULL)
			{
				_distances[i] = distance;
			}

			if (minDistance > distance)
			{
				minDistance = distance;
			}
		}

		vm->Delete(o);
		vm->Delete(n);
	}

	// 近似の可否
	if (minDistance > fitTolerance)
	{
		// 近似できません
		return 2;
	}

	return 0;
}

/*
// 網目点列の生成
short CBSplineSurface::CreateMeshPoints(short _nUPoints, short _nVPoints)
{
VECTOR uv = vm->New(2);
VECTOR pos = vm->New(3);

VECTOR interval = vm->New(2);

interval[0] = uKnot[(short)nKnots[0] - 1] / (_nUPoints - 1);
interval[1] = vKnot[(short)nKnots[1] - 1] / (_nVPoints - 1);

meshPoints = vm->New(3, _nUPoints * _nVPoints);

for(int i = 0; i < _nUPoints; i++)
{
for(int j = 0; j < _nVPoints; j++)
{
uv[0] = interval[0] * i;
uv[1] = interval[1] * j;

if( Position(uv, pos) != 0 )
{
return 1;
}

for(int k = 0; k < 3; k++)
{
meshPoints[k][_nVPoints * i + j] = pos[k];
}
}
}

vm->Delete(interval);

vm->Delete(uv);
vm->Delete(pos);

return 0;
}
*/

// モデルの生成
short CBSplineSurface::CreateModel()
{
	UINT nVertices = MODEL_POLYGONS * MODEL_POLYGONS;
	UINT nIndices = (MODEL_POLYGONS - 1) * (MODEL_POLYGONS - 1) * 3 * 2;

	D3DVertex* vertices = new D3DVertex[nVertices];
	WORD* indices = new WORD[nIndices];

	VECTOR uv = vm->New(2);
	VECTOR pos = vm->New(3);

	//SCALAR zmin = 0.0;
	//SCALAR zmax = 0.0;
	//SCALAR ymin = 0.0;
	//SCALAR ymax = 0.0;
	SCALAR xmin = 0.0;
	SCALAR xmax = 0.0;

	// Vertices Position
	SCALAR uParamMin = uKnot[0];
	SCALAR vParamMin = vKnot[0];
	SCALAR uParamMax = uKnot[(short)nKnots[0] - 1];
	SCALAR vParamMax = vKnot[(short)nKnots[1] - 1];

	for (int i = 0; i < MODEL_POLYGONS; i++)
	{
		for (int j = 0; j < MODEL_POLYGONS; j++)
		{
			uv[0] = (uParamMax - uParamMin) * i / (SCALAR)(MODEL_POLYGONS - 1) + uParamMin;
			uv[1] = (vParamMax - vParamMin) * j / (SCALAR)(MODEL_POLYGONS - 1) + vParamMin;

			if (uv[0] < uParamMin) uv[0] = uParamMin;
			else if (uv[0] > uParamMax) uv[0] = uParamMax;
			if (uv[1] < vParamMin) uv[1] = vParamMin;
			else if (uv[1] > vParamMax) uv[1] = vParamMax;

			if (Position(uv, pos) != 0)
			{
				return 1;
			}

			vertices[i * MODEL_POLYGONS + j].Pos = XMFLOAT3(pos[0], pos[1], pos[2]);

			//if (zmin > pos[2]) zmin = pos[2];
			//if (zmax < pos[2]) zmax = pos[2];
			//if (ymin > pos[1]) ymin = pos[1];
			//if (ymax < pos[1]) ymax = pos[1];
			if (xmin > pos[0]) xmin = pos[0];
			if (xmax < pos[0]) xmax = pos[0];
		}
	}

	vm->Delete(uv);
	vm->Delete(pos);

	// Indices
	int k, l;
	for (int i = 0; i < MODEL_POLYGONS - 1; i++)
	{
		for (int j = 0; j < MODEL_POLYGONS - 1; j++)
		{
			k = i * MODEL_POLYGONS + j;
			l = (i * (MODEL_POLYGONS - 1) + j) * 6;

			indices[l + 0] = k;
			indices[l + 1] = k + 1;
			indices[l + 2] = k + MODEL_POLYGONS;

			indices[l + 3] = k + 1;
			indices[l + 4] = k + MODEL_POLYGONS + 1;
			indices[l + 5] = k + MODEL_POLYGONS;
		}
	}

	// Vertices Color
	short hsv[3] = { 0, 255, 255 };
	short rgb[3];

	for (UINT i = 0; i < nVertices; i++)
	{
		if (modelColor.w >= 2.0f)
		{
			//hsv[0] = (1.0f - (vertices[i].Pos.z - zmin) / (zmax - zmin)) * 240;
			//hsv[0] = (1.0f - (vertices[i].Pos.y - ymin) / (ymax - ymin)) * 240;
			hsv[0] = (1.0f - (vertices[i].Pos.x - xmin) / (xmax - xmin)) * 240;

			app->HSV2RGB(hsv, rgb);

			vertices[i].Color = XMFLOAT4(rgb[0] / 255.0f, rgb[1] / 255.0f, rgb[2] / 255.0f, 1.0f);
		}
		else {
			vertices[i].Color = modelColor;
		}
	}

	if (FAILED(surfaceModel.Init(vertices, nVertices, indices, nIndices, D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST, false)))
	{
		return 2;
	}

	for (UINT i = 0; i < nVertices; i++)
	{
		vertices[i].Color = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
	}

	if (FAILED(surfaceWireframe.Init(vertices, nVertices, indices, nIndices, D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST, true)))
	{
		return 3;
	}

	delete vertices;
	delete indices;

	return 0;
}

// 網目の生成
short CBSplineSurface::CreateMesh()
{
	// 網目点 全消去
	if (ClearPoints(&meshPoints) != 0)
	{
		return 1;
	}

	// 網目点
	VECTOR pos = vm->New(3);

	for (int i = 0; i < nCtrlPoints[0]; i++)
	{
		for (int j = 0; j < nCtrlPoints[1]; j++)
		{
			for (int k = 0; k < 3; k++)
			{
				pos[k] = ctrlPoints[k][(short)nCtrlPoints[1] * i + j];
			}

			meshPoints.push_back(new CPoint(pos, meshColor));
		}
	}

	vm->Delete(pos);


	// 網目エッジ 全消去
	if (ClearEdges(&meshEdges) != 0)
	{
		return 2;
	}

	VECTOR start = vm->New(3);
	VECTOR end = vm->New(3);

	short idx;

	// V 軸に沿った網
	for (int i = 0; i < nCtrlPoints[0]; i++)
	{
		for (int j = 0; j < nCtrlPoints[1] - 1; j++)
		{
			for (int k = 0; k < 3; k++)
			{
				idx = nCtrlPoints[1] * i + j;

				start[k] = ctrlPoints[k][idx];
				end[k] = ctrlPoints[k][idx + 1];
			}

			meshEdges.push_back(new CLine(start, end, meshColor));
		}
	}

	// U 軸に沿った網
	for (int i = 0; i < nCtrlPoints[1]; i++)
	{
		for (int j = 0; j < nCtrlPoints[0] - 1; j++)
		{
			for (int k = 0; k < 3; k++)
			{
				idx = nCtrlPoints[1] * j + i;

				start[k] = ctrlPoints[k][idx];
				end[k] = ctrlPoints[k][idx + (int)nCtrlPoints[1]];
			}

			meshEdges.push_back(new CLine(start, end, meshColor));
		}
	}

	vm->Delete(start);
	vm->Delete(end);

	return 0;
}

// 網目の参照
short CBSplineSurface::ReferMesh(short _idx, SCALAR _t, VECTOR _pos)
{
	if (_idx >= meshEdges.size() || _idx < 0)
	{
		return 1;
	}

	if (meshEdges[_idx]->Position(_t, _pos) != 0)
	{
		return 2;
	}

	return 0;
}

// 制御点の平行移動
short CBSplineSurface::Shift(VECTOR _amount)
{
	return __super::Shift(_amount, (short)nCtrlPoints[0] * nCtrlPoints[1]);
}

// 更新
void CBSplineSurface::Update()
{
	Release();
	Setup(order, updateCtrlPoints, nUpdateCtrlPoints, updateUKnot, updateVKnot);
}

// 描画
void CBSplineSurface::Draw()
{
	if (!createdModel)
	{
		SetupModel();
		createdModel = true;
	}

	if (drawWireframe)
	{
		surfaceWireframe.Draw();
	}

	if (drawMeshPoints)
	{
		std::vector<CPoint*>::iterator it = meshPoints.begin();
		while (it != meshPoints.end())
		{
			(*it)->Draw();
			++it;
		}
	}

	if (drawMeshEdges)
	{
		std::vector<CLine*>::iterator it = meshEdges.begin();
		while (it != meshEdges.end())
		{
			(*it)->Draw();
			++it;
		}
	}

	surfaceModel.Draw(BLEND_MULTIPLY);
}