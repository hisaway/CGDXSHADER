/*
Programeed : Takumi Adachi / C&G SYSTEMS 2017.8.30
*/

#include "Camera.h"

#define _USE_MATH_DEFINES
#include <math.h>

using namespace DirectX;

#define Rasian(angle) angle * M_PI / 180.0f
#define Cos(angle) cos(Rasian(angle))
#define Sin(angle) sin(Rasian(angle))

Camera::Camera()
	: reverse(false)
{
	SetPosition(10.0f);
	SetShift(0.0f, 0.0f);
	SetZoom(1.0f);
	SetTarget(0.0f, 0.0f, 0.0f);
	SetTurn(180.0f, 89.0f);
	Update();
}

Camera::~Camera()
{

}

void Camera::Shift(float _x, float _y)
{
	SetShift(shiftPos.x + _x, shiftPos.y + _y);
}

void Camera::Zoom(float _z)
{
	SetZoom(zoomRate + _z);
}

void Camera::Turn(float _u, float _v)
{	 
	float _longitude, _latitude;

	if (reverse)
		_longitude = longitude - _v;
	else
		_longitude = longitude + _v;

	_latitude = latitude + _u;

	if ( 0 < Cos(latitude) && 0 > Cos(_latitude)) {
		_longitude = longitude - _v;
		reverse = true;
	}
	else if ( 0 > Cos(latitude) && 0 < Cos(_latitude)){
		_longitude = longitude + _v;
		reverse = false;
	}
	else {
		//Do nothing
	}

	SetTurn(_longitude, _latitude);
}

void Camera::SetPosition(float _radius)
{
	if (_radius <= 0)
	{
		//あとからオブジェクトの大きさを加味して距離を決めたい
		distance = 10.0f;
	}
	else {
		distance = _radius;
	}
}

void Camera::SetShift(float _shiftX, float _shiftY)
{
	shiftPos.x = _shiftX;
	shiftPos.y = _shiftY;

	shift = XMMatrixTranslation(shiftPos.x, shiftPos.y, 0.0f);
}

void Camera::SetZoom(float _zoomRate)
{
	zoomRate = _zoomRate;

	zoom = XMMatrixTranslation(0.0f, 0.0f, zoomRate);
}

void Camera::SetTurn(float _longitude, float _latitude)
{
	longitude = _longitude;
	latitude = _latitude;

	float z = distance * Sin(_latitude) * Cos(_longitude);
	float x = distance * Sin(_latitude) * Sin(_longitude);
	float y = distance * Cos(_latitude);

	position = XMVectorSet(x, y, z, 0.0f) + target;
}

void Camera::SetTarget(float _x, float _y, float _z)
{
	float* v = target.vector4_f32;

	target = XMVectorSet(_x, _y, _z, 0.0f);

	/*if (v[0] == 0.0f || v[1] == 0.0f || v[2] == 0.0f)
	{
		target = XMVectorSet(_x, _y, _z, 0.0f);
	}
	else {
		target = XMVectorSet
		(
			(v[0] + _x) / 2, 
			(v[1] + _y) / 2, 
			(v[2] + _z) / 2, 
			0.0f
		);
	}*/
}



void Camera::Update()
{
	view = XMMatrixLookAtLH(position, target, XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f));
	view = XMMatrixMultiply(view, shift);
	view = XMMatrixMultiply(view, zoom);
}