/*
	Programmed : Yuki Hisae / C&G SYSTEMS 2018.4.30
*/
#include "VMUtil.h"

#pragma once

////////////////////////////////////////////////////////
// 3次元ベクトルクラス
////////////////////////////////////////////////////////
template<typename T = float>
class vector3
{
//private:
	// 要素
	T x;
	T y;
	T z;

public:
	// コンストラクタ
	vector3(VECTOR obj)
	{
		x = obj[0];
		y = obj[1];
		z = obj[2];
	}

	vector3() { Set(0, 0, 0); };
	vector3(T x_, T y_, T z_) { Set(x_, y_, z_); };
	vector3(const vector3 &obj) { 
		x = obj.x;
		y = obj.y;
		z = obj.z;
	}

	// 
	T GetX() { return x; };
	T GetY() { return y; };
	T GetZ() { return z; };

	void SetX(T _t) { x = _t; }
	void SetT(T _t) { y = _t; }
	void SetZ(T _t) { z = _t; }

	/*__declspec(property(get = GetX, put = SetX)) T X;
	__declspec(property(get = GetY, put = SetY)) T Y;
	__declspec(property(get = GetZ, put = SetZ)) T Z;*/

	// 演算子
	void operator =  (vector3 V) {
		Set(V.x, V.y, V.z);
	};
	bool operator == (vector3 V) {
		return (x == V.x && y == V.y && z == V.z);
	};
	vector3 operator +  (vector3 V) {
		return vector3(x + V.x, y + V.y, z + V.z);
	};	// 加算 +
	vector3 operator -  (vector3 V) {
		return vector3(x - V.x, y - V.y, z - V.z);
	};	// 減算 -
	vector3 operator *  (T k) {
		return vector3(k*x, k*y, k*z);
	};	// 係数倍 *

	T operator [] (short i) {
		switch (i)
		{
		case 0:
			return x;
		case 1:
			return y;
		case 2:
			return z;

		default:
			return NULL;
			break;
		}
	};

	// 単位ベクトル化
	vector3 Normalize() {
		vector3 v;
		T nrm = sqrt(x*x + y*y + z*z);
		v.x = x/nrm;
		v.y = y/nrm;
		v.z = z/nrm;

		return v;
	};

	static vector3 Normalize(vector3 a) { return a.Normalize(); };

	// 外積
	vector3 Cross(vector3 b) {
		vector3 v;
		v.x = (y * b.z) - (z * b.y);
		v.y = (z * b.x) - (x * b.z);
		v.z = (x * b.y) - (y * b.x);
		return v;
	};

	// 内積
	vector3 Dot(vector3 b) {
		vector3 v;
		v.x = (y * b.z) - (z * b.y);
		v.y = (z * b.x) - (x * b.z);
		v.z = (x * b.y) - (y * b.x);
		return v;
	};

	// スカラー積
	static T DotP(vector3 a, vector3 b){
		return (a.x * b.x) + (a.y * b.y) + (a.z * b.z);
	};

	// ベクトル積
	static vector3 CrossP(vector3 a, vector3 b) {
		vector3 v;
		v.x = (a.y * b.z) - (a.z * b.y);
		v.y = (a.z * b.x) - (a.x * b.z);
		v.z = (a.x * b.y) - (a.y * b.x);
		return v;
	};

	void Set(T x_, T y_, T z_) {
		x = x_;
		y = y_;
		z = z_;
	};
};