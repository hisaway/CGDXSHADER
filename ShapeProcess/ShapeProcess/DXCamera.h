/*
 Programeed	: Yuki Hisae	/ C&G SYSTEMS 2017.12.13
*/

#pragma once

#include "common.h"
#include "Quaternion.h"
#include "vector3.h"
#include "Camera.h"
#include "VMUtil.h"

class DXCamera : public Camera
{
public:
	DXCamera();
	~DXCamera();

	virtual void Update();
	void Rotate(float _dx, float _dy, bool clickedRight);
	void GetAxe(VECTOR& _x, VECTOR& _y, VECTOR& _z);
	void GetTarget(VECTOR& _target);
	//virtual void SetTarget(float _x, float _y, float _z);
private:
	vector3<float> up;
	vector3<float> posf;
	vector3<float> targetf;
	vector3<float> axe_u;
	vector3<float> axe_w;
	vector3<float> axe_z;

	XMMATRIX world;

	bool oldClickedRight = false;
	//void RotateWorld(float theta_u, float theta_v);
	void RotateByQuaternion(float theta_u, float theta_w, bool clickedRight);
	void CalcAxis();

	void InitRotate();
	XMMATRIX World() { Update(); return world; }
};