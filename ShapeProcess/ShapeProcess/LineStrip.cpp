/*
	Programmed : Yuki Hisae / C&G SYSTEMS 2018.4.30
*/
#include "LineStrip.h"
#include "Point.h"

CLineStrip::CLineStrip(
	const MATRIX _points, const int _nPnts,
	XMFLOAT4 _color)
	: drawLine(true), drawPnt(true)
{
	SetPoints(_points, _nPnts);
	CreModel();
}


CLineStrip::~CLineStrip()
{

}

short CLineStrip::CreModel()
{
	D3DVertex* vertices = new D3DVertex[nPnts];
	WORD* indices = new WORD[nPnts];

	if (drawPnt)
		cpoints.reserve(nPnts);

	for (int i = 0; i < nPnts; i++)
	{
		vertices[i] = { XMFLOAT3( points[0][i], points[1][i], points[2][i]) , color };
		indices[i] = i;

		cpoints.push_back(new CPoint(points[0][i], points[1][i], points[2][i], color));
	}

	if ( FAILED
	( polylineModel.Init(
		vertices, nPnts, 
		indices, nPnts, 
		D3D_PRIMITIVE_TOPOLOGY_LINESTRIP )
	) ) {
		return 2;
	}
		
	delete vertices;
	vertices = nullptr;

	delete indices;
	indices = nullptr;

	return 1;
}

void CLineStrip::Draw()
{
	polylineModel.Draw();

	if (drawPnt)
	{
		std::vector<CPoint*>::iterator it = cpoints.begin();
		while (it != cpoints.end())
		{
			(*it)->Draw();
			++it;
		}
	}
		
}

void CLineStrip::SetPoints(const MATRIX _points, const int _nPnts)
{
	if (_nPnts <= 0)
		return;

	nPnts = _nPnts;

	points = vm->New(3, nPnts);

	for (int i = 0; i < nPnts; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			points[j][i] = _points[i][j];
		}
	}
}