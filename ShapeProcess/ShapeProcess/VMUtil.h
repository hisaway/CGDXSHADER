/*
	Programmed	: Takumi Adachi / C&G SYSTEMS 2017.8.30
	Modified : Yuki Hisae / C&G SYSTEMS 2018.4.30
*/

#pragma once

#include <stdio.h>
#include <vector>

#define SCALAR double

typedef SCALAR *VECTOR, **MATRIX;
typedef VECTOR point3;

class VMUtil
{
protected:
	FILE* outfp;

public:
	VMUtil();	// コンストラクタ

	VECTOR New(int n);				// ベクトルの領域確保
	MATRIX New(int nrow, int ncol);	// 行列の領域確
	void Delete(VECTOR v);			// ベクトルの領域解放
	void Delete(MATRIX a);			// 行列の領域解放

	void Add(VECTOR a, VECTOR b, VECTOR c, int n);		// ベクトルの和
	void Sub(VECTOR a, VECTOR b, VECTOR c, int n);		// ベクトルの差
	SCALAR Dot(VECTOR u, VECTOR v, int dim);	// ベクトルの内積
	void Cross(VECTOR u, VECTOR v, VECTOR w);	// ベクトルの外積
	void Normalize(VECTOR x, VECTOR y, int n);	// ベクトルの単位化
	SCALAR Length(VECTOR x, int n);			// ベクトルの大きさ
	void Scale(VECTOR a, VECTOR b, int n, SCALAR nScale);

	void Add(MATRIX a, MATRIX b, MATRIX c, int nrow, int ncol);	// 行列の和
	void Mul(MATRIX a, MATRIX b, MATRIX c, int nrow, int ncol);	// 行列の積

	void SimultLinearEquations(MATRIX a, VECTOR b, VECTOR x, int n);	// 連立一次方程式の解
	void SimultQuadraticEquation(SCALAR a, SCALAR b, SCALAR c, VECTOR x1, VECTOR x2);	// 二次方程式の解

	bool Parse(
		const MATRIX a, 
		const int divUnit,
		const int nrow,
		const int ncol,
		std::vector<MATRIX> *vctMat
	);

	void Transpose(const MATRIX a, MATRIX b, const int nrow, const int ncol);

	void Print(VECTOR v, int n, char *format);		// ベクトルの表示
	void Print(MATRIX a, int ncol, char *format);	// 行列の表示
	void FilePrint(VECTOR v, int n, char *format);		// ベクトルのファイル書き込み
	void FilePrint(MATRIX a, int ncol, char *format);	// 行列のファイル書き込み
	
	void SetFilePointer(FILE* fp);	// ファイルポインタの設定
};
