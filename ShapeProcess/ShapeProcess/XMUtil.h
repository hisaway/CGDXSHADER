#pragma once
#include "common.h"

namespace DirectX
{
	short XMVectorGet(float* vector);
	short XMVectorGet(float& vector);
}