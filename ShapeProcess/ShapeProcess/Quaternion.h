#pragma once

#include "Vector3.h"

template <typename T = float>
class Quaternion
{
public:
	Quaternion(T _t, T _x, T _y, T _z)
	{
		create(_t, _x, _y, _z);
	};


	Quaternion(const Quaternion &obj)
	{
		create(obj.t, obj.x, obj.y, obj.z);
	}

	~Quaternion() {};



	static Quaternion<T> Rotation(T theta, vector3<T> axis, vector3<T> pos) {
		Quaternion<> P = Quaternion<>(0, pos[0], pos[1], pos[2]);
		Quaternion<> Q = Quaternion<>(
			cos(theta / 2),
			axis[0] * sin(theta / 2),
			axis[1] * sin(theta / 2),
			axis[2] * sin(theta / 2)
			);
		Quaternion<> R = Quaternion<>(
			cos(theta / 2),
			-axis[0] * sin(theta / 2),
			-axis[1] * sin(theta / 2),
			-axis[2] * sin(theta / 2)
			);
		Quaternion<> S = R*P*Q;
		
		return S;
	};
	vector3<T> vector() {
		return vector3<T>(x, y, z);
	};

	// ***** Zq
	void	operator =  (Quaternion _Q) {
		x = _Q.x;
		y = _Q.y;
		z = _Q.z;
		t = _Q.t;
	};	
	bool       operator == (Quaternion _Q) {
		return (t == Q.t && x == Q.x && y == Q.y && z == Q.z);
	};	
	Quaternion operator +  (Quaternion _Q) {
		return quaternion(t + Q.t, x + Q.x, y + Q.y, z + Q.z);
	};	
	Quaternion operator -  (Quaternion _Q) {
		return quaternion(t - Q.t, x - Q.x, y - Q.y, z - Q.z);
	};	
	Quaternion operator *  (Quaternion _Q) {
		// q1 = w1 + V1
		T    t1 = t;
		vector3<T> V1(x, y, z);

		// q2 = w2 + V2
		T    t2 = _Q.t;
		vector3<T> V2(_Q.x, _Q.y, _Q.z);

		// q1 * q2 = (t1*t2 - V1EV2) + (t1*V2 + t2*V1 + V1~V2)
		T    ans = t1*t2 - vector3<>::DotP(V1, V2);
		vector3<T> V = V2*t1 + V1*t2 + vector3<>::CrossP(V1, V2);
		return Quaternion(ans, V[0], V[1], V[2]);
	};	
	Quaternion operator *  (T k) {
		x *= k;
		y *= k;
		z += k;
	};		
	T operator [] (short i) {
		switch (i)
		{
		case 0:
			return x;
		case 1:
			return y;
		case 2:
			return z;

		default:
			break;
		}
	};

private:
	// 
	T x;
	T y;
	T z;
	// À
	T t;
	void create(T _t, T _x, T _y, T _z) {
		x = _x;
		y = _y;
		z = _z;
		t = _t;
	};
};
