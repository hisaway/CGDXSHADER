/*
Programeed : Takumi Adachi / C&G SYSTEMS 2017.8.30
*/

#pragma once

#include "common.h"

// モデルクラス
class Model
{
private:
	ID3D11Buffer* pVertexBuffer;
	ID3D11Buffer* pIndexBuffer;
	ID3D11Buffer* pConstantBuffer;

	D3DVertex* vertices;
	WORD* indices;

	UINT nVertices, nIndices;

	D3D_PRIMITIVE_TOPOLOGY topo;

	bool isWireframe;

public:
	Model();
	~Model();

	HRESULT Init(D3DVertex* _vertices, UINT _nVertices, WORD* _indices, UINT _nIndices,
		D3D_PRIMITIVE_TOPOLOGY _topo = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST,
		bool _isWireframe = false);
	void Cleanup();
	void Draw(BLEND_MODE blendMode = BLEND_ALPHA);

	ID3D11Buffer* GetVtxBuf() { return pVertexBuffer; }
	ID3D11Buffer* GetIdxBuf() { return pIndexBuffer; }
	ID3D11Buffer* GetCstBuf() { return pConstantBuffer; }

	UINT GetNumIndices() { return nIndices; }

	D3D_PRIMITIVE_TOPOLOGY GetTopo() { return topo; }

	bool IsWireframe() { return isWireframe; }
};