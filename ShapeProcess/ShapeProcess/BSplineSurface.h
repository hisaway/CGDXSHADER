/*
	Programeed : Takumi Adachi / C&G SYSTEMS 2017.8.30
*/
#pragma once

#include "BSplineBase.h"

class CBSplineCurve;

// Bスプライン曲面
class CBSplineSurface :public CBSplineBase
{
private:
	VECTOR order;			// 階数
	VECTOR nCtrlPoints;		// 制御点数
	VECTOR nKnots;			// ノット数
	VECTOR uKnot, vKnot;	// ノットベクトル

	VECTOR nUpdateCtrlPoints;
	VECTOR nUpdateKnots;
	VECTOR updateUKnot, updateVKnot;

	//MATRIX meshPoints;			// 網目点列
	std::vector<CPoint*> meshPoints;// 網目点
	std::vector<CLine*> meshEdges;	// 網目エッジ

	Model surfaceModel;			// 曲面モデル
	Model surfaceWireframe;		// 曲面ワイヤーフレーム

	XMFLOAT4 modelColor;	// モデルの色
	XMFLOAT4 meshColor;		// メッシュの色

	bool drawWireframe;		// ワイヤーフレーム
	bool drawMeshPoints;	// 網目点列		 を描画するか
	bool drawMeshEdges;		// 網目エッジ

	bool createdModel;		// 表示系が生成されたか

	const ORIGINATE_FROM origin;	// どういった手段で生成されたのか

									// パラメータ (u, v) -> ベクトル (x, y, z)
	short uv2xyz(VECTOR _uv, VECTOR _p, short _uDiffTimes = 0, short _vDiffTimes = 0);

	// 通過点から制御点列を得る
	short PassPoints2CtrlPoints(MATRIX _passPoints, short _nPassPoints, bool _isCoordU, MATRIX _ctrlPoints, short _nCtrlPoints);

	// 網目点列の生成
	//short CreateMeshPoints(short _nUPoints, short _nVPoints);

	// モデルの生成
	short CreateModel();

	// 網目の生成
	short CreateMesh();

	// セットアップ
	void Setup(VECTOR _order, MATRIX _ctrlPoints, VECTOR _nCtrlPoints, VECTOR _uKnot, VECTOR _vKnot);

	// リリース
	void Release();

	// セットアップ（表示系）
	void SetupModel();

	// リリース（表示系）
	void ReleaseModel();

public:
	// コンストラクタ : [in] 階数, 制御点, ノットベクトル
	CBSplineSurface(VECTOR _order, MATRIX _ctrlPoints, VECTOR _nCtrlPoints, VECTOR _uKnot, VECTOR _vKnot,
		XMFLOAT4 _modelColor = XMFLOAT4(0.0f, 0.0f, 0.0f, 2.0f),
		XMFLOAT4 _meshColor = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f));

	// コンストラクタ : [in] 通過点
	CBSplineSurface(MATRIX _passPoints, short _nUPassPoints, short _nVPassPoints,
		XMFLOAT4 _modelColor = XMFLOAT4(0.0f, 0.0f, 0.0f, 2.0f),
		XMFLOAT4 _meshColor = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f));

	// デストラクタ
	~CBSplineSurface();

	// 制御点列の設定
	short SetCtrlPoints(MATRIX _passPoints, short _nUPassPoints, short _nVPassPoints);

	// ノットベクトルの設定
	short SetKnot(VECTOR _uKnot, VECTOR _vKnot);
	short SetKnot(MATRIX _passPoints, short _nUPassPoints, short _nVPassPoints);

	// ノットの挿入
	short InsertKnot(SCALAR _t, bool _forU, short* _insertedIndex = NULL);

	// 基底関数
	short BasisFunc(
		SCALAR _t,
		bool _isCoordU,
		short _reqOrder,
		VECTOR _blendFunc,
		short _diffTimes = 0
	);

	// 位置ベクトル
	short Position(VECTOR _uv, VECTOR _pos);

	// 接線ベクトル
	short Tangent(VECTOR _uv, VECTOR _pu, VECTOR _pv);

	// 2回微分ベクトル
	short Secdiff(VECTOR _uv, VECTOR _puu, VECTOR _pvv, VECTOR _puv);

	// 離散点化
	short Discretize(
		short _nUPartitions,
		short _nVPartitions,
		std::vector<VECTOR>* _discretePoints,
		std::vector<std::pair<SCALAR, SCALAR>>* _parameters = NULL
	);
	short DiscretizeForKnot(
		short _nUPartitions,
		short _nVPartitions,
		std::vector<VECTOR>* _discretePoints,
		std::vector<std::pair<SCALAR, SCALAR>>* _parameters = NULL
	);

	// 最近点
	short NearestPoint(VECTOR _outPoint, VECTOR _uv, VECTOR _nearPoint = NULL, SCALAR* _distance = NULL);

	// 分割
	short Split(SCALAR _t, bool _forU, CBSplineSurface** surfaceA, CBSplineSurface** surfaceB);

	// MMボックス取得
	short GetMMBox(MMBox** mmbox);

	// 曲線との交点
	short IntersectionForCurve(CBSplineCurve* curve, VECTOR intersectPoint);

	// 自身に近い曲線から面上線を生成する
	short CreateCurveOnSelf(
		CBSplineCurve* _nearCurve,
		CBSplineCurve** _onXYZCurve,
		CBSplineCurve** _onUVCurve = NULL,
		SCALAR* _distances = NULL
	);

	// 網目の参照
	short ReferMesh(short _idx, SCALAR _t, VECTOR _pos);

	// 制御点の平行移動
	short Shift(VECTOR _amount);

	// 更新
	void Update();

	// 描画
	void Draw();

	// 描画状態設定
	void SetDrawState(bool _drawWireframe, bool _drawMeshPoints, bool _drawMeshEdges)
	{
		drawWireframe = _drawWireframe;
		drawMeshPoints = _drawMeshPoints;
		drawMeshEdges = _drawMeshEdges;
	}

	// 階数の取得
	short UOrder() { return order[0]; }
	short VOrder() { return order[1]; }

	// 制御点数の取得
	short UCtrlPointNum() { return nCtrlPoints[0]; }
	short VCtrlPointNum() { return nCtrlPoints[1]; }

	// 制御点列の取得
	SCALAR CtrlPoints(short row, short col)
	{
		if (row < 0 || row > 3 || col < 0 || col >= nCtrlPoints[0] * nCtrlPoints[1])
		{
			return 0.0;
		}

		return ctrlPoints[row][col];
	}

	// ノットの取得
	SCALAR UKnot(short index)
	{
		if (index < 0 || index >= nKnots[0])
		{
			return 0.0;
		}

		return uKnot[index];
	}
	SCALAR VKnot(short index)
	{
		if (index < 0 || index >= nKnots[1])
		{
			return 0.0;
		}

		return vKnot[index];
	}

	// 最小・最大パラメータの取得
	SCALAR MinUParam() { return uKnot[0]; }
	SCALAR MaxUParam() { return uKnot[(short)nKnots[0] - 1]; }
	SCALAR MinVParam() { return vKnot[0]; }
	SCALAR MaxVParam() { return vKnot[(short)nKnots[1] - 1]; }
};