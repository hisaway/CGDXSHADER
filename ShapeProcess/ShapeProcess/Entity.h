/*
	Programeed : Takumi Adachi / C&G SYSTEMS 2017.8.30
*/

#pragma once

#include "Application.h"
#include "VMUtil.h"
#include "Model.h"
#include "common.h"

#define TOLERANCE 1.0e-6

enum {
	ENT_POINT,
	ENT_EDGE,
	ENT_CURVE,
	ENT_SURFACE,
	ENT_BEZIERCURVE,
	ENT_BEZIERSURFACE,
	ENT_EXCEPTION
};

// エンティティ
class Entity
{
protected:
	VECTOR minPos;
	VECTOR maxPos;
public:
	Entity();
	~Entity();

	virtual void Draw() {}

	short GetMaxVector(VECTOR _maxPos) 
	{
		_maxPos = maxPos;
		return 0;
	};

	short GetMinVector(VECTOR _minPos) 
	{
		_minPos = minPos;
		return 0; 
	};

	short GetTarget(VECTOR _targetPos)
	{ 
		vm->Add(maxPos, minPos, _targetPos, 3);  

		for (int i = 0; i < 3; i++)
			_targetPos[i] /= 2.0;

		return 0;
	};
};

