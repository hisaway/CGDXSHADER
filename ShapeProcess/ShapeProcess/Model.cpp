/*
Programeed : Takumi Adachi / C&G SYSTEMS 2017.8.30
*/

#include "Model.h"
#include "Application.h"

Model::Model() :
	pVertexBuffer(nullptr), pIndexBuffer(nullptr), pConstantBuffer(nullptr),
	vertices(nullptr), indices(nullptr),
	nVertices(0), nIndices(0) {}

Model::~Model() {}

HRESULT Model::Init(D3DVertex* _vertices, UINT _nVertices, WORD* _indices, UINT _nIndices,
	D3D_PRIMITIVE_TOPOLOGY _topo, bool _isWireframe)
{
	HRESULT hr = S_OK;

	topo = _topo;
	isWireframe = _isWireframe;

	// Create vertex buffer
	nVertices = _nVertices;
	vertices = new D3DVertex[nVertices];

	for (UINT i = 0; i < nVertices; i++)
	{
		vertices[i].Pos = _vertices[i].Pos;
		vertices[i].Color = _vertices[i].Color;
	}

	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DYNAMIC;
	bd.ByteWidth = sizeof(D3DVertex) * nVertices;
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	D3D11_SUBRESOURCE_DATA InitData;
	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = vertices;

	hr = app->pd3dDevice->CreateBuffer(&bd, &InitData, &pVertexBuffer);
	if (FAILED(hr))
		return hr;


	// Create index buffer
	nIndices = _nIndices;
	indices = new WORD[nIndices];

	for (UINT i = 0; i < nIndices; i++)
	{
		indices[i] = _indices[i];
	}

	bd.Usage = D3D11_USAGE_DYNAMIC;
	bd.ByteWidth = sizeof(WORD) * nIndices;
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	InitData.pSysMem = indices;

	hr = app->pd3dDevice->CreateBuffer(&bd, &InitData, &pIndexBuffer);
	if (FAILED(hr))
		return hr;


	// Create the constant buffer
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(ConstantBuffer);
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;

	hr = app->pd3dDevice->CreateBuffer(&bd, nullptr, &pConstantBuffer);
	if (FAILED(hr))
		return hr;

	return hr;
}

void Model::Cleanup()
{
	delete vertices;
	delete indices;

	if (pConstantBuffer) pConstantBuffer->Release();
	if (pVertexBuffer)	 pVertexBuffer->Release();
	if (pIndexBuffer)	 pIndexBuffer->Release();
}

void Model::Draw(BLEND_MODE blendMode)
{
	app->RenderModel(this, blendMode);
}