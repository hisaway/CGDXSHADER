#include "DXCamera.h"
#include "Vector3.h"

#define _USE_MATH_DEFINES
#include <math.h>

DXCamera::DXCamera()
	//: Camera()
{
	world = XMMatrixIdentity();
	InitRotate();
	//RotateByQuaternion(0.0f, 0.0f, false);
	Update();
}

DXCamera::~DXCamera()
{
}

void DXCamera::InitRotate()
{
	posf = vector3<>(-25.0f, 25.0f, -25.0f);
	position = XMVectorSet(posf[0], posf[1], posf[2], 0.0f);
	//up = vector3<>(-0.866f, 0.0f, 0.5f);
	//up = up.Normalize();
	up = vector3<>(0.0f, 1.0f, 0.0f);

	targetf = vector3<>(0.0f, 0.0f, 0.0f);

	vector3<> R, a, u, w;
	u = up;
	w = vector3<>(-1.0f, 0.0f, 1.0f);

	Quaternion<> S = Quaternion<>::Rotation(0, u, posf);
	S = Quaternion<>::Rotation(0, w, S.vector());
	posf = S.vector();
	position = XMVectorSet(posf[0], posf[1], posf[2], 0.0f);

	S = Quaternion<>::Rotation(0, w, up);
	up = S.vector();

	axe_w = w;
	axe_u = u;
}

void DXCamera::GetAxe(VECTOR& _x, VECTOR& _y, VECTOR& _z)
{
	for (int i = 0; i < 3; i++)
	{
		_x[i] = axe_u[i];
		_y[i] = axe_w[i];
		_z[i] = axe_z[i];
	}
}

void DXCamera::GetTarget(VECTOR& _target)
{
	float* v = target.vector4_f32;
	for (int i = 0; i < 3; i++)
	{
		_target[i] = v[i];
	}
}

void DXCamera::RotateByQuaternion(float theta_u, float theta_w, bool clickedRight)
{
	//２つの回転軸を求める
	vector3<> R, a, u, w;

	/*if (clickedRight == true)
	{
		R = vector3<>(posf);
		a = vector3<>(up);
		u = vector3<>(0.0f, 1.0f, 0.0f);
		
		w = vector3<>::CrossP(R, u);
		w = w.Normalize(a);

		Quaternion<> S = Quaternion<>::Rotation(theta_u, u, posf);
		S = Quaternion<>::Rotation(theta_w, w, S.vector());
		posf = S.vector();
		position = XMVectorSet(posf[0], posf[1], posf[2], 0.0f);

		S = Quaternion<>::Rotation(theta_w, w, up);
		up = S.vector();
	}
	else
	{
		u = axe_u;
		w = axe_w;

		Quaternion<> S = Quaternion<>::Rotation(theta_u, u, posf);
		S = Quaternion<>::Rotation(theta_w, w, S.vector());

		posf = S.vector();
		position = XMVectorSet(posf[0], posf[1], posf[2], 0.0f);

		S = Quaternion<>::Rotation(theta_w, w, up);
		up = S.vector();

		axe_u = vector3<>::Normalize(up);
		axe_w = vector3<>::CrossP(posf, axe_u);
		axe_w = axe_w.Normalize();
	}*/

	

	if (oldClickedRight == false && clickedRight == true)
	{
		R = posf;
		a = vector3<>(up);
		u = a.Normalize();
		u = vector3<>(0.0f, 1.0f, 0.0f);
		w = vector3<>::CrossP(R, u);
		w = w.Normalize();



		Quaternion<> S = Quaternion<>::Rotation(theta_u, u, posf);
		S = Quaternion<>::Rotation(theta_w, w, S.vector());

		posf = S.vector();
		position = XMVectorSet(posf[0], posf[1], posf[2], 0.0f);

		S = Quaternion<>::Rotation(theta_w, w, up);
		up = S.vector();

		axe_w = w;
		axe_u = u;
		/*u = axe_u;
		w = axe_w;

		Quaternion<> S = Quaternion<>::Rotation(theta_u, u, posf);
		S = Quaternion<>::Rotation(theta_w, w, S.vector());
		posf = S.vector();
		position = XMVectorSet(posf[0], posf[1], posf[2], 0.0f);

		S = Quaternion<>::Rotation(theta_w, w, up);
		up = S.vector();*/

		oldClickedRight = true;
	}
	else if(oldClickedRight == true && clickedRight == true)
	{
		u = axe_u;
		w = axe_w;

		Quaternion<> S = Quaternion<>::Rotation(theta_u, u, posf);
		S = Quaternion<>::Rotation(theta_w, w, S.vector());
		posf = S.vector();
		position = XMVectorSet(posf[0], posf[1], posf[2], 0.0f);

		S = Quaternion<>::Rotation(theta_w, w, up);
		up = S.vector();

		oldClickedRight = true;
	}
	//右クリックされてない
	else if (oldClickedRight == false && clickedRight == false)
	{
		oldClickedRight = false;
	}//右クリック離れた
	else
	{
		/*u = axe_u;
		w = axe_w;*/

		//R = posf;
		//a = vector3<>(up);
		//u = a.Normalize();
		////u = vector3<>(0.0f, 1.0f, 0.0f);
		//w = vector3<>::CrossP(R, u);
		//w = w.Normalize();



		//Quaternion<> S = Quaternion<>::Rotation(theta_u, u, posf);
		//S = Quaternion<>::Rotation(theta_w, w, S.vector());

		//posf = S.vector();
		//position = XMVectorSet(posf[0], posf[1], posf[2], 0.0f);

		//S = Quaternion<>::Rotation(theta_w, w, up);
		//up = S.vector();

		//axe_w = w;
		//axe_u = u;

		oldClickedRight = false;
	}
}

void DXCamera::Rotate(float _dx, float _dy, bool clickedRight)
{
	RotateByQuaternion(_dx, _dy, clickedRight);

	//RotateWorld(_dx / 100.0f, _dy / 100.0f);
}

void DXCamera::Update()
{
	view = XMMatrixLookAtLH(position, target, XMVectorSet(up[0], up[1], up[2], 0.0f));
	//view = XMMatrixMultiply(view, shift);
	//view = XMMatrixMultiply(view, zoom);
}

void DXCamera::CalcAxis()
{
	/*axe_x = posf - targetf;
	axe_x.Normalize();

	axe_z = vector3<float>::CrossP(up, axe_x);
	axe_z.Normalize();

	axe_y = vector3<float>::CrossP(axe_x, axe_z);
	axe_y.Normalize();*/

	axe_z = vector3<float>::CrossP(axe_w, axe_u);
}