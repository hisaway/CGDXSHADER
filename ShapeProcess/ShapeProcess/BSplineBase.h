/*
Programeed : Takumi Adachi / C&G SYSTEMS 2017.8.30
*/
#pragma once

#include "Entity.h"

class CPoint;
class CLine;
class MMBox;

enum ORIGINATE_FROM
{
	ORIGIN_STANDARD,
	ORIGIN_PASSPOINTS
};

// Bスプラインベース
class CBSplineBase :public Entity
{
protected:
	MATRIX ctrlPoints;		// 制御点列
	MATRIX updateCtrlPoints;

	// 通過点からノットベクトルを得る
	short PassPoints2Knot(MATRIX _passPoints, short _nPassPoints, short _order, VECTOR _knot, short _nKnots);

	// 通過点から制御点列を得る
	short PassPoints2CtrlPoints(MATRIX _passPoints, short _nPassPoints, short _order, VECTOR _knot, short _nKnots, MATRIX _ctrlPoints, short _nCtrlPoints);

	// 基底関数
	short BasisFunc(
		SCALAR _t,
		short _order,
		short _reqOrder,
		short _nCtrlPoints,
		VECTOR _knot,
		short _nKnots,
		VECTOR _blendFunc,
		short _diffTimes = 0
	);

	// 点の全消去
	short ClearPoints(std::vector<CPoint*>* _pPoints);

	// エッジの全消去
	short ClearEdges(std::vector<CLine*>* _pEdges);

	// 離散点パラメータの取得
	short GetDisptParams(VECTOR _knot, short _nKnots, short _nPartitions, std::vector<SCALAR>* _disptParams);

	// ノットの挿入
	short CBSplineBase::InsertKnot(
		SCALAR _t,
		VECTOR _knot,
		short _nKnots,
		VECTOR _newKnot,
		short* _insertIdx
	);

	// 重複なしのノットの取得
	short GetUnDuplicatedKnots(VECTOR _knot, short _nKnots, std::vector<SCALAR>* _knotParams);

	// MMボックス取得
	short GetMMBox(MMBox** mmbox, short nCtrlPoints);

	// 制御点の平行移動
	short Shift(VECTOR _amount, short _nCtrlPoints);

public:
	// 制御点列の設定
	short SetCtrlPoints(MATRIX _ctrlPoints, short _nCtrlPoints);

	// 制御点列の取得
	MATRIX CtrlPoints() { return ctrlPoints; }
};