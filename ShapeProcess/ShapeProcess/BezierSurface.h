/*
	BezierSurface.h
	Programmed : Yuki Hisae / C&G SYSTEMS
*/

#pragma once

#include "BezierBase.h"


class BezierCurve;
class LOOP;
class CLineStrip;

class BezierSurface :
	public BezierBase
{
private:
	VECTOR nCtrlPoints;		// 制御点数

	std::vector<CPoint*> meshPoints;
	std::vector<CLine*>  meshEdges;

	Model surfaceModel;
	Model surfaceWireframe;

	XMFLOAT4 modelColor;
	XMFLOAT4 meshColor;

	bool drawWireframe;		// ワイヤーフレーム
	bool drawMeshPoints;	// 網目点列		 を描画するか
	bool drawMeshEdges;		// 網目エッジ

	bool createdModel;		// 表示系が生成されたか

	//const ORIGINATE_FROM origin;	// どういった手段で生成されたのか

	// パラメータ (u, v) -> ベクトル (x, y, z)
	short uv2xyz(
		VECTOR _uv, 
		VECTOR _p, 
		short _uDiffTimes = 0, 
		short _vDiffTimes = 0) const;

	// 通過点から制御点列を得る
	//short PassPoints2CtrlPoints(MATRIX _passPoints, short _nPassPoints, bool _isCoordU, MATRIX _ctrlPoints, short _nCtrlPoints);

	// 網目点列の生成
	//short CreateMeshPoints(short _nUPoints, short _nVPoints);

	// モデルの生成
	short CreateModel();

	// 網目の生成
	//short CreateMesh();

	// セットアップ
	void Setup(MATRIX _ctrlPoints);

	// リリース
	void Release();

	// セットアップ（表示系）
	void SetupModel();

	// リリース（表示系）
	void ReleaseModel();

public:
	// コンストラクタ : [in] 階数, 制御点, ノットベクトル
	BezierSurface(MATRIX _ctrlPoints,
		XMFLOAT4 _modelColor = XMFLOAT4(0.0f, 0.0f, 0.0f, 2.0f),
		XMFLOAT4 _meshColor = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f));

	// コンストラクタ : [in] 通過点
	BezierSurface(MATRIX _passPoints, VECTOR _uv, short _nUPassPoints, short _nVPassPoints,
		XMFLOAT4 _modelColor = XMFLOAT4(0.0f, 0.0f, 0.0f, 2.0f),
		XMFLOAT4 _meshColor = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f));

	// コピーコンストラクタ
	//BezierSurface(MATRIX)

	// デストラクタ
	~BezierSurface();

	// 制御点列の設定
	short SetCtrlPoints(MATRIX _passPoints, short _nUPassPoints, short _nVPassPoints);


	// 位置ベクトル
	short Position(VECTOR _uv, VECTOR _pos) const ;

	// 接線ベクトル
	short Tangent(VECTOR _uv, VECTOR _pu, VECTOR _pv);

	// 2回微分ベクトル
	short Secdiff(VECTOR _uv, VECTOR _puu, VECTOR _pvv, VECTOR _puv);

	// アイソ曲線
	short IsoCurve(
		const SCALAR _t, 
		const bool _forU, 
		BezierCurve** isoCurve) const ;

	// 離散点化
	/*short Discretize(
		short _nUPartitions,
		short _nVPartitions,
		std::vector<VECTOR>* _discretePoints,
		std::vector<std::pair<SCALAR, SCALAR>>* _parameters = NULL
	);*/
	/*short DiscretizeForKnot(
		short _nUPartitions,
		short _nVPartitions,
		std::vector<VECTOR>* _discretePoints,
		std::vector<std::pair<SCALAR, SCALAR>>* _parameters = NULL
	);*/

	// 最近点
	short NearestPoint(VECTOR _anyPoint, VECTOR _uv, VECTOR _nearestPoint = NULL, SCALAR* _distance = NULL);

	// 分割
	short Split(SCALAR _t, bool _forU, BezierSurface** _surfaceA, BezierSurface** _surfaceB);

	// トリミング(分割x2x2)
	short Trim(
		const SCALAR _u0,
		const SCALAR _u1,
		const SCALAR _v0,
		const SCALAR _v1,
		BezierSurface** _trmSurface);

	// MMボックス取得
	short GetMMBox(MMBox** mmbox);

	// 自身に近い曲線から面上線を生成する
	/*short CreateCurveOnSelf(
		CBSplineCurve* _nearCurve,
		CBSplineCurve** _onXYZCurve,
		CBSplineCurve** _onUVCurve = NULL,
		SCALAR* _distances = NULL
	);*/

	// 網目の参照
	//short ReferMesh(short _idx, SCALAR _t, VECTOR _pos);

	// 制御点の平行移動
	//short Shift(VECTOR _amount);

	// 更新
	//void Update();

	// 描画
	void Draw();

	// トリムループを面上線に変換
	//short Trim(LOOP _loop);

	// 描画状態設定
	void SetDrawState(bool _drawWireframe, bool _drawMeshPoints, bool _drawMeshEdges)
	{
		drawWireframe = _drawWireframe;
		drawMeshPoints = _drawMeshPoints;
		drawMeshEdges = _drawMeshEdges;
	}

	// 制御点数の取得
	short UCtrlPointNum() { return nCtrlPoints[0]; }
	short VCtrlPointNum() { return nCtrlPoints[1]; }

	// 制御点列の取得
	SCALAR CtrlPoints(short row, short col)
	{
		if (row < 0 || row > 3 || col < 0 || col >= nCtrlPoints[0] * nCtrlPoints[1])
		{
			return 0.0;
		}

		return ctrlPoints[row][col];
	}

	// 曲線との交点
	short IntersectionCurveByMMBox(
		const BezierCurve& curve, 
		VECTOR* intersect
	) const;

	// 曲線との交点

	// 曲面との交線
	// 出力：パラメトリック曲線
	short IntersectionCvForSurface(
		const UINT divNum,						//[IN]
		const BezierSurface& surface,			//[IN]
		vector<BezierCurve*> intersectcurve,		//[OUT]
		vector<BezierCurve*> intersect_uvcurveA,	//[OUT]
		vector<BezierCurve*> intersect_uvcurveB	//[OUT]
	) const ;

	// 曲面との交線(外部利用)
	static short Intersection2Surface(
		const UINT divNum,				//[IN]
		const BezierSurface& surfaceA,
		const BezierSurface& surfaceB,
		vector<BezierCurve*> intersectline,		//[OUT]
		vector<BezierCurve*> intersect_uvlineA,	//[OUT]
		vector<BezierCurve*> intersect_uvlineB	//[OUT]
	);

	// 曲面との交線
	// 出力：曲線
	short IntersectionPoints2Surface(
		const UINT divNum,				//[IN]
		const BezierSurface& surface,	//[IN]
		CLineStrip** intersectline,		//[OUT]
		CLineStrip** intersect_uvlineA,	//[OUT]
		CLineStrip** intersect_uvlineB	//[OUT]
	) const ;

	//(外部利用)
	static short IntersectionPoints2Surface(
		const UINT divNum,				//[IN]
		const BezierSurface& surfaceA,	//[IN]
		const BezierSurface& surfaceB,	//[IN]
		CLineStrip** intersectline,		//[OUT]
		CLineStrip** intersect_uvlineA,	//[OUT]
		CLineStrip** intersect_uvlineB	//[OUT]
	);
};

