/*
Programeed : Takumi Adachi / C&G SYSTEMS 2017.8.30
*/

#include "Axes.h"

#define DRAW_AXIS_RANGE  250.0f

Axes::Axes(bool _drawSubAxes, bool _drawField)
{
	drawSubAxes = _drawSubAxes;
	drawField = _drawField;

	CreateAxes();
	CreateField();
}

Axes::~Axes()
{
	xAxis.Cleanup();
	yAxis.Cleanup();
	zAxis.Cleanup();

	for (int i = 0; i < 51; i++)
	{
		xSubAxes[i].Cleanup();
		ySubAxes[i].Cleanup();
		ySubAxes[i].Cleanup();
	}

	field.Cleanup();
}

short Axes::CreateAxes()
{
	D3DVertex* vertices = new D3DVertex[2];
	WORD indices[2] = { 0, 1 };
	ColorData col;

	
	// X Axis RED
	vertices[0].Pos = XMFLOAT3(0.0f, 0.0f, 0.0f);
	vertices[0].Color = XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f);
	vertices[1].Pos = XMFLOAT3(DRAW_AXIS_RANGE, 0.0f, 0.0f);
	vertices[1].Color = XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f);

	if (FAILED(xAxis.Init(vertices, 2, indices, 2, D3D_PRIMITIVE_TOPOLOGY_LINELIST)))
	{
		delete vertices;
		return 1;
	}

	// Y Axis Green
	vertices[0].Pos = XMFLOAT3(0.0f, 0.0f, 0.0f);
	vertices[0].Color = XMFLOAT4(0.0f, 1.0f, 0.0f, 1.0f);
	vertices[1].Pos = XMFLOAT3(0.0f, DRAW_AXIS_RANGE, 0.0f);
	vertices[1].Color = XMFLOAT4(0.0f, 1.0f, 0.0f, 1.0f);

	if (FAILED(yAxis.Init(vertices, 2, indices, 2, D3D_PRIMITIVE_TOPOLOGY_LINELIST)))
	{
		delete vertices;
		return 2;
	}

	// Z Axis Blue
	vertices[0].Pos = XMFLOAT3(0.0f, 0.0f, 0.0f);
	vertices[0].Color = XMFLOAT4(0.0f, 0.0f, 1.0f, 1.0f);
	vertices[1].Pos = XMFLOAT3(0.0f, 0.0f, DRAW_AXIS_RANGE);
	vertices[1].Color = XMFLOAT4(0.0f, 0.0f, 1.0f, 1.0f);

	if (FAILED(zAxis.Init(vertices, 2, indices, 2, D3D_PRIMITIVE_TOPOLOGY_LINELIST)))
	{
		delete vertices;
		return 3;
	}

	// Sub Axes
	int j;
	SCALAR drawAxisMin, drawAxisMax;
	for (int i = 0; i < 51; i++)
	{
		j = i - 25;
		drawAxisMin = -DRAW_AXIS_RANGE;
		drawAxisMax = (j != 0) ? DRAW_AXIS_RANGE : 0.0f;

		// X
		/*vertices[0].Pos = XMFLOAT3(drawAxisMin, j * 10.0f, 0.0f);
		vertices[0].Color = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
		vertices[1].Pos = XMFLOAT3(drawAxisMax, j * 10.0f, 0.0f);
		vertices[1].Color = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);

		if (FAILED(xSubAxes[i].Init(vertices, 2, indices, 2, D3D_PRIMITIVE_TOPOLOGY_LINELIST)))
		{
			delete vertices;
			return 4;
		}*/

		// X
		vertices[0].Pos = XMFLOAT3(drawAxisMin, 0.0f, j * 10.0f);
		//vertices[0].Color = XMFLOAT4(0.1f, 0.3f, 0.6f, 1.0f);
		vertices[0].Color = col.GREEN;
		vertices[1].Pos = XMFLOAT3(drawAxisMax, 0.0f, j * 10.0f);
		vertices[1].Color = col.GREEN;
		vertices[0].Color = XMFLOAT4(0.1f, 0.3f, 0.6f, 1.0f);

		if (FAILED(xSubAxes[i].Init(vertices, 2, indices, 2, D3D_PRIMITIVE_TOPOLOGY_LINELIST)))
		{
			delete vertices;
			return 4;
		}

		// Y
		vertices[0].Pos = XMFLOAT3(j * 10.0f, drawAxisMin, 0.0f);
		vertices[0].Color = col.GREEN;
		vertices[1].Pos = XMFLOAT3(j * 10.0f, drawAxisMax, 0.0f);
		vertices[1].Color = col.GREEN;

		if (FAILED(ySubAxes[i].Init(vertices, 2, indices, 2, D3D_PRIMITIVE_TOPOLOGY_LINELIST)))
		{
			delete vertices;
			return 5;
		}

		// Z
		vertices[0].Pos = XMFLOAT3(j * 10.0f, 0.0f, drawAxisMin);
		vertices[0].Color = col.GREEN;
		vertices[1].Pos = XMFLOAT3(j * 10.0f, 0.0f, drawAxisMax);
		vertices[1].Color = col.GREEN;

		if (FAILED(zSubAxes[i].Init(vertices, 2, indices, 2, D3D_PRIMITIVE_TOPOLOGY_LINELIST)))
		{
			delete vertices;
			return 5;
		}
	}

	delete vertices;
	return 0;
}

short Axes::CreateField()
{
	if (!drawField)
		return -1;

	D3DVertex* vertices = new D3DVertex[4];
	WORD indices[6] = { 0, 1, 2, 1, 2, 3 };

	vertices[0].Pos = XMFLOAT3(-DRAW_AXIS_RANGE, 0.0f, -DRAW_AXIS_RANGE);
	vertices[0].Color = XMFLOAT4(0.6f, 0.6f, 0.6f, 1.0f);
	vertices[1].Pos = XMFLOAT3(+DRAW_AXIS_RANGE, 0.0f, -DRAW_AXIS_RANGE);
	vertices[1].Color = XMFLOAT4(1.0f, 0.6f, 0.6f, 1.0f);
	vertices[2].Pos = XMFLOAT3(-DRAW_AXIS_RANGE, 0.0f, +DRAW_AXIS_RANGE);
	vertices[2].Color = XMFLOAT4(0.6f, 1.0f, 0.6f, 1.0f);
	vertices[3].Pos = XMFLOAT3(+DRAW_AXIS_RANGE, 0.0f, +DRAW_AXIS_RANGE);
	vertices[3].Color = XMFLOAT4(0.6f, 0.6f, 1.0f, 1.0f);

	if (FAILED(field.Init(vertices, 4, indices, 6)))
	{
		delete vertices;
		return 1;
	}

	delete vertices;
	return 0;
}

void Axes::Draw()
{
	xAxis.Draw();
	yAxis.Draw();
	zAxis.Draw();

	if (drawSubAxes)
	{
		for (int i = 0; i < 51; i++)
		{
			xSubAxes[i].Draw();
			//ySubAxes[i].Draw();
			zSubAxes[i].Draw();
		}
	}

	//field.Draw(BLEND_MULTIPLY);
}