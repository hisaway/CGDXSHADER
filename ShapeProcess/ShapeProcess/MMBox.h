/*
Programeed : Takumi Adachi / C&G SYSTEMS 2017.8.30
*/

#pragma once

#include "Cube.h"

// MMボックス
class MMBox :public Entity
{
private:
	VECTOR minPos, maxPos;

public:
	// コンストラクタ
	MMBox(VECTOR _minPos, VECTOR _maxPos);
	
	// デストラクタ
	~MMBox();

	Model boxModel;
	short CreModel(XMFLOAT4 _modelColor = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f),
		XMFLOAT4 _polyColor = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f));
	void Draw();


	// 最小・最大の座標
	VECTOR Min(){ return minPos; }
	VECTOR Max(){ return maxPos; }
};

// 干渉ボックスの組
struct IntersectMMBoxPair
{
private:
	short id1, id2;					// 衝突ボックスのid
	SCALAR minPos[3], maxPos[3];
	SCALAR center[3];				// 2つのボックスを囲む中点座標
	SCALAR length;					// 対角線の長さ

public:
	// コンストラクタ
	IntersectMMBoxPair(short _id1, MMBox* _box1, short _id2, MMBox* _box2)
	{
		id1 = _id1, id2 = _id2;

		if( _box1 == NULL || _box2 == NULL ) return;

		minPos[0] = (_box1->Min()[0] <  _box2->Min()[0]) ? _box1->Min()[0] : _box2->Min()[0];
		maxPos[0] = (_box1->Max()[0] >= _box2->Max()[0]) ? _box1->Max()[0] : _box2->Max()[0];
		minPos[1] = (_box1->Min()[1] <  _box2->Min()[1]) ? _box1->Min()[1] : _box2->Min()[1];
		maxPos[1] = (_box1->Max()[1] >= _box2->Max()[1]) ? _box1->Max()[1] : _box2->Max()[1];
		minPos[2] = (_box1->Min()[2] <  _box2->Min()[2]) ? _box1->Min()[2] : _box2->Min()[2];
		maxPos[2] = (_box1->Max()[2] >= _box2->Max()[2]) ? _box1->Max()[2] : _box2->Max()[2];

		center[0] = (minPos[0] + maxPos[0]) / 2.0;
		center[1] = (minPos[1] + maxPos[1]) / 2.0;
		center[2] = (minPos[2] + maxPos[2]) / 2.0;

		length = sqrt(pow(maxPos[0] - minPos[0], 2) + pow(maxPos[1] - minPos[1], 2) + pow(maxPos[2] - minPos[2], 2));
	}

	// デストラクタ
	~IntersectMMBoxPair()
	{
	}

	short Id1(){ return id1; }
	short Id2(){ return id2; }

	VECTOR Center(){ return center; }	// 中点の座標
	SCALAR Length(){ return length; }	// 対角線の長さ
};