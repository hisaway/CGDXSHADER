/*
Programeed	: Takumi Adachi / C&G SYSTEMS 2017.8.30
Modified	: Yuki Hisae	/ C&G SYSTEMS 2017.12.13
*/

#pragma once

#include "common.h"

class Camera
{
protected:
	XMMATRIX view;

	XMMATRIX shift;
	XMMATRIX zoom;
	XMVECTOR position;
	XMVECTOR target;	// ターゲット
	XMFLOAT2 shiftPos;	// シフト量
	
	float zoomRate;		// 拡大率
	float longitude;	// 経度 [   0, +360) (Z軸+方向から見ている場合: == 0, Y軸+方向から見て時計回り)
	float latitude;		// 緯度 [- 90, + 90] (上から見下ろしている場合: > 0)
	float distance;		//カメラ位置とターゲットとの距離
	

	bool reverse;

	void SetShift(float _shiftX, float _shiftY);
	void SetZoom(float _zoomRate);
	void SetTurn(float _longitude, float _latitude);
	
	//void SetTarget(Entity* _entity)
public:
	Camera();
	~Camera();

	void SetPosition(float _radius);
	void Shift(float _x, float _y);
	void Zoom(float _z);
	void Turn(float _u, float _v);
	virtual void SetTarget(float _x, float _y, float _z);

	void Update();

	float GetZoomRate() { return zoomRate; }

	XMMATRIX View() { Update(); return view; }
};