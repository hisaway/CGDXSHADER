/*
	FileName	: LineStrip.h
	Programed	: Yuki Hisae / C&G SYSTEMS 2018.4.30
*/
#pragma once
#include "Entity.h"
#include "Vector3.h"

class CPoint;

class CLineStrip :
	public Entity
{
private:
	MATRIX points;
	int nPnts;

	Model polylineModel;
	vector<CPoint*> cpoints;
	XMFLOAT4 color;

	// Config : Draw Model 
	bool drawLine;
	bool drawPnt;

	short CreModel();
	void SetPoints(const MATRIX _points, int);
public:
	CLineStrip(
		const MATRIX _points, const int _nPnts,
		XMFLOAT4 _color = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.8f));
	~CLineStrip();
	
	void Draw();
	MATRIX GetPnts() { return points; };
	int SIZE() { return nPnts; };
};

