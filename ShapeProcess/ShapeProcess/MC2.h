/*
	FileName	: MC2.h
	Programed	: Yuki Hisae / C&G SYSTEMS 2018.4.30
*/
#pragma once
#include <vector>

#include "SPFile.h"

using namespace std;

class Entity;
class BezierBase;
class BezierCurve;
class BezierSurface;

typedef struct _mc2
{
	vector<Entity*> *resource;
	SPFile file;

	void GetResource(vector<Entity*>& _resource , const int);

	// 第1回 パラメトリック曲線
	// 提出プログラムはありません


	/**************************************
		第2回 パラメトリック曲線の性質を知る
	**************************************/
	// BEZIER曲線の基底関数(0.0 <= t <= 1.0)
	BezierBase* DisplayBernstain();

	// 曲線の表示 + 曲線の全長 
	// + 接線ベクトルの表示とノルム計算
	// + 法線(2階微分)ベクトルの表示とノルム計算
	void DisplayCurve();
	
	// 通過点から逆変換
	BezierCurve* MkBezierCurveFromThroughPoint();

	/**************************************
		第3回課題 曲面への表示
	**************************************/
	// 曲面の図示(メッシュ表示) + 制御点メッシュ
	// + UVアイソラインの表示
	// + 接線ベクトル
	// + 接平面の法線ベクトル(単位ベクトル)
	void DisplaySurface();

	// UV曲線の表示
	// 面上線
	void UVCurve();


	/**************************************
		第4回課題 曲線・曲面への最近点計算
	**************************************/
	void CalcNearpointToBS();
	void CalcNearpointToBC();

	/**************************************
		第5回課題 
	**************************************/
	//プログラム提出なし

	/**************************************
		第6回課題 曲線・曲面のオフセット
	**************************************/
	//曲線のオフセット 
	//BezierCurve* MCP_2015_06_0();
	void BCOffset();
	//曲線のオフセット
	void BSOffset();

	/**************************************
		第8回課題 曲線・曲面の曲率、曲率半径を算出
	**************************************/
	void BC_Kapper();
	void BS_Kapper();



	/**************************************
		第9回課題 BEZIER曲面、曲面の操作
	**************************************/
	// 指定パラメータ位置でBEZIER曲線・曲面の分割
	void SplitBC();
	void SplitBS();

	// 部分曲線・部分曲面の取得
	void TrmBS();

	// BEZIER曲線・曲面のミニマクスボックスの算出
	// ２つのミニマクスボックスの干渉判定
	void InterferenceDetector();

	/**************************************
		第10回課題 BEZIER曲面、曲面の操作2
	**************************************/
	// 曲線・曲面の各種微分ベクトルの算出
	// 外部の点からBEZIER曲線への最近点
	// 外部の点からBEZIER曲面への最近点
	//該当ソースコードを整理せよという課題
	//機能自体はすでに実装済み

	/**************************************
		第11回課題 BEZIER曲線とBEZIER曲面の交点
	**************************************/
	void CalcIntersectionPointByMMbox();

	/**************************************
		第12回課題 2曲面間の交線
	**************************************/
	void CalcCrossLine2BS();

	/**************************************
	第*回課題 
	**************************************/

	BezierCurve* MCP_();

	void DisplayPoints();

	void Clear();

}MC2;