/*
Programeed : Takumi Adachi / C&G SYSTEMS 2017.8.30
*/

#pragma once

#include "Entity.h"

class CLine;
class MMBox;

// 立方体
class CCube :public Entity
{
private:
	VECTOR pos0, pos1;

	Model cubeModel;
	std::vector<CLine*> cubeEdges;

	XMFLOAT4 color;

	// モデルの生成
	short CreateModel();

public:
	// コンストラクタ
	CCube(VECTOR _pos0, VECTOR _pos1, XMFLOAT4 _color = XMFLOAT4(0.4f, 0.7f, 1.0f, 1.0f));
	CCube(MMBox* _box, XMFLOAT4 _color = XMFLOAT4(0.4f, 0.7f, 1.0f, 1.0f));

	// デストラクタ
	~CCube();

	// 描画
	void Draw();
};