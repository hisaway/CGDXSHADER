/*
Programeed : Takumi Adachi / C&G SYSTEMS 2017.8.30
*/

#pragma once

#include "CollisionDetector.h"
#include "VMUtil.h"

class CBSplineCurve;
class CBSplineSurface;
class CollisionDetector;
class BezierCurve;
class BezierSurface;


// 形状処理
class ShapeProcess
{
private:
	ShapeProcess();

	static CollisionDetector collisDetector;

public:
	// 曲線 - 曲面 の交点の算出（ボックス干渉法）
	// Bスプライン
	static short IntersectionPointByMMBox(
		CBSplineCurve* curve, 
		CBSplineSurface* surface, 
		VECTOR intersectPoint);
	
	// BEZIER
	static short IntersectionPointByMMBox(
		const BezierCurve& curve,
		const BezierSurface& surface, 
		VECTOR intersectPoint);

	static short IntersectionPointByMMBox(
		const BezierCurve& curve,
		const BezierSurface& surface,
		VECTOR* intersectPoint);
};