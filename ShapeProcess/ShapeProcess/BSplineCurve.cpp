/*
Programeed : Takumi Adachi / C&G SYSTEMS 2017.8.30
*/

#include "BSplineCurve.h"
#include "ShapeProcess.h"
#include "Point.h"
#include "Line.h"

#define _USE_MATH_DEFINES
#include <math.h>

#define MODEL_POLYGONS 200

// コンストラクタ : [in] 階数, 制御点, ノットベクトル
CBSplineCurve::CBSplineCurve(short _order, MATRIX _ctrlPoints, short _nCtrlPoints, VECTOR _knot,
	XMFLOAT4 _modelColor, XMFLOAT4 _polyColor)
	: modelColor(_modelColor), polyColor(_polyColor),
	  drawPolyPoints(true), drawPolyEdges(true)
{
	order = _order;
	nCtrlPoints = _nCtrlPoints;

	if( order < 1 )
	{
		order = 1;
	}

	if( nCtrlPoints < order )
	{
		nCtrlPoints = order;
	}

	nKnots = order + nCtrlPoints;

	updateCtrlPoints = vm->New(4, nCtrlPoints);
	updateKnot = vm->New(nKnots);

	Setup(order, _ctrlPoints, nCtrlPoints, _knot);
}

// コンストラクタ : [in] 通過点
CBSplineCurve::CBSplineCurve(MATRIX _passPoints, short _nPassPoints,
	XMFLOAT4 _modelColor, XMFLOAT4 _polyColor)
	: modelColor(_modelColor), polyColor(_polyColor),
	  drawPolyPoints(true), drawPolyEdges(true)
{
	// (階数) = 4
	order = 4;

	// (制御点数) = (通過点数) + 2
	nCtrlPoints = _nPassPoints + 2;

	// ノット数
	nKnots = order + nCtrlPoints;

	// 制御点列
	ctrlPoints = vm->New(4, nCtrlPoints);

	// ノットベクトル
	knot = vm->New(nKnots);

	SetKnot(_passPoints, _nPassPoints);
	SetCtrlPoints(_passPoints, _nPassPoints);

	CreateModel();
	CreatePoly();
}

// デストラクタ
CBSplineCurve::~CBSplineCurve()
{
	Release();

	vm->Delete(updateCtrlPoints);
	vm->Delete(updateKnot);
}

// セットアップ
void CBSplineCurve::Setup(short _order, MATRIX _ctrlPoints, short _nCtrlPoints, VECTOR _knot)
{
	order = _order;
	nCtrlPoints = _nCtrlPoints;

	if( order < 1 )
	{
		order = 1;
	}

	if( nCtrlPoints < order )
	{
		nCtrlPoints = order;
	}

	// 制御点列の初期化
	ctrlPoints = vm->New(4, nCtrlPoints);
	__super::SetCtrlPoints(_ctrlPoints, nCtrlPoints);

	// ノット数
	nKnots = order + nCtrlPoints;

	// ノットベクトルの初期化
	knot = vm->New(nKnots);
	SetKnot(_knot);

	nUpdateCtrlPoints = nCtrlPoints;
	nUpdateKnots = nKnots;

	CreateModel();
	CreatePoly();
}

// リリース
void CBSplineCurve::Release()
{
	vm->Delete(ctrlPoints);
	vm->Delete(knot);

	ClearPoints(&polyPoints);
	ClearEdges(&polyEdges);

	curveModel.Cleanup();
}

// 制御点列の設定
short CBSplineCurve::SetCtrlPoints(MATRIX _passPoints, short _nPassPoints)
{
	return __super::PassPoints2CtrlPoints(_passPoints, _nPassPoints, order, knot, nKnots, ctrlPoints, nCtrlPoints);
}

// ノットベクトルの設定
short CBSplineCurve::SetKnot(VECTOR _knot)
{
	for(int i = 0; i < nKnots; i++)
	{
		knot[i] = _knot[i];

		updateKnot[i] = knot[i];
	}

	return 0;
}

short CBSplineCurve::SetKnot(MATRIX _passPoints, short _nPassPoints)
{
	return __super::PassPoints2Knot(_passPoints, _nPassPoints, order, knot, nKnots);
}

// ノットの挿入
short CBSplineCurve::InsertKnot(SCALAR _t, short* _insertedIndex)
{
	VECTOR newKnot = vm->New(nUpdateKnots + 1);
	MATRIX newCtrlPoints = vm->New(4, nUpdateCtrlPoints + 1);
	short insertIndex;

	__super::InsertKnot(_t, updateKnot, nUpdateKnots, newKnot, &insertIndex);

	// 新しい制御点列を求める
	for(int i = 0; i < nUpdateCtrlPoints + 1; i++)
	{
		SCALAR alpha;

		if( i <= insertIndex - order )
		{
			alpha = 1.0;
		}
		else if( insertIndex - order + 1 <= i && i <= insertIndex - 1 )
		{
			alpha = (_t - newKnot[i]) / (newKnot[i + order] - newKnot[i]);
		}
		else if( insertIndex <= i )
		{
			alpha = 0.0;
		}

		for(int j = 0; j < 3; j++)
		{
			newCtrlPoints[j][i] = alpha * updateCtrlPoints[j][i] + (1 - alpha) * updateCtrlPoints[j][i - 1];
		}
	}

	nUpdateKnots++;
	nUpdateCtrlPoints++;

	vm->Delete(updateKnot);
	vm->Delete(updateCtrlPoints);

	updateKnot = vm->New(nUpdateKnots);
	updateCtrlPoints = vm->New(4, nUpdateCtrlPoints);

	for(int i = 0; i < nUpdateKnots; i++)
	{
		updateKnot[i] = newKnot[i];
	}

	for(int i = 0; i < nUpdateCtrlPoints; i++)
	{
		for(int j = 0; j < 4; j++)
		{
			updateCtrlPoints[j][i] = newCtrlPoints[j][i];
		}
	}

	vm->Delete(newKnot);
	vm->Delete(newCtrlPoints);

	if( _insertedIndex != NULL )
	{
		*_insertedIndex = insertIndex;
	}

	return 0;
}

// 基底関数
short CBSplineCurve::BasisFunc(
	SCALAR _t,
	short _reqOrder,
	VECTOR _blendFunc,
	short _diffTimes
) {
	return __super::BasisFunc(_t, order, _reqOrder, nCtrlPoints, knot, nKnots, _blendFunc, _diffTimes);
}

// パラメータ t -> ベクトル (x, y, z)
short CBSplineCurve::t2xyz(SCALAR _t, VECTOR _p, short _diffTimes)
{
	VECTOR blendFunc = vm->New(nCtrlPoints);

	if( BasisFunc(_t, order, blendFunc, _diffTimes) != 0 ) return 1;

	for(int i = 0; i < 3; i++)
	{
		_p[i] = 0.0;

		for(int j = 0; j < nCtrlPoints; j++)
		{
			_p[i] += blendFunc[j] * ctrlPoints[i][j];
		}
	}

	vm->Delete(blendFunc);

	return 0;
}

// 位置ベクトル
short CBSplineCurve::Position(SCALAR _t, VECTOR _pos)
{
	return t2xyz(_t, _pos);
}

// 接線ベクトル
short CBSplineCurve::Tangent(SCALAR _t, VECTOR _tan)
{
	return t2xyz(_t, _tan, 1);
}

// 2回微分ベクトル
short CBSplineCurve::Secdiff(SCALAR _t, VECTOR _sec)
{
	return t2xyz(_t, _sec, 2);
}

// 離散点化
short CBSplineCurve::Discretize(short _nPartitions, std::vector<VECTOR>* _discretePoints)
{
	// 離散点の全消去
	_discretePoints->clear();

	// 離散点のパラメータを取得する
	std::vector<SCALAR> disptParams;

	if( GetDisptParams(knot, nKnots, _nPartitions, &disptParams) != 0 )
	{
		return 1;
	}

	// パラメータから位置ベクトルに変換する
	for(int i = 0; i < disptParams.size(); i++)
	{
		VECTOR discretePoint = vm->New(3);

		if( Position(disptParams[i], discretePoint) != 0 )
		{
			return 2;
		}

		_discretePoints->push_back(discretePoint);
	}

	return 0;
}

// 最近点
short CBSplineCurve::NearestPoint(VECTOR _outPoint, SCALAR* _t, VECTOR _nearPoint, SCALAR* _distance)
{
	VECTOR toOut0 = vm->New(3);
	VECTOR toOut1 = vm->New(3);
	VECTOR tan0 = vm->New(3);
	VECTOR tan1 = vm->New(3);
	VECTOR nearToOut = vm->New(3);

	SCALAR leftPrm, rightPrm, midPrm;
	SCALAR appliedLeftPrm, appliedRightPrm;
	SCALAR maxPrm, minPrm;
	SCALAR angle0, angle1, angle2;
	SCALAR appliedAngle0, appliedAngle1;

	int candidCount = 0;	// 候補パラメータの数
	SCALAR candidDist;		// 候補距離

	// 最大・最小パラメータの取得
	std::vector<SCALAR> knotParams;
	GetUnDuplicatedKnots(knot, nKnots, &knotParams);
	maxPrm = knotParams.back();
	minPrm = knotParams.front();

	// 最初に8分割行い、どの区間に該当するか
	for(int i = 0; i < 7; i++)
	{
		leftPrm = minPrm + i / 7.0 * maxPrm;
		rightPrm = minPrm + (i + 1) / 7.0 * maxPrm;

		Position(leftPrm, toOut0);
		vm->Sub(_outPoint, toOut0, toOut0, 3);
		Tangent(leftPrm, tan0);

		Position(rightPrm, toOut1);
		vm->Sub(_outPoint, toOut1, toOut1, 3);
		Tangent(rightPrm, tan1);

		angle0 = acos(vm->Dot(toOut0, tan0, 3) / (vm->Length(toOut0, 3) * vm->Length(tan0, 3)));
		angle1 = acos(vm->Dot(toOut1, tan1, 3) / (vm->Length(toOut1, 3) * vm->Length(tan1, 3)));

		// 該当区間であるか
		if( (angle0 <= M_PI / 2.0 && angle1 > M_PI / 2.0) ||
			(angle0 > M_PI / 2.0 && angle1 <= M_PI / 2.0) )
		{
			midPrm = (leftPrm + rightPrm) / 2.0;
			Position(midPrm, toOut0);
			vm->Sub(_outPoint, toOut0, toOut0, 3);

			// 2個以上の候補 -> 最短距離のパラメータを適用
			if( (candidCount == 0) ||
				(candidCount >= 1 && vm->Length(toOut0, 3) < candidDist) )
			{
				candidDist = vm->Length(toOut0, 3);
				appliedLeftPrm = leftPrm;
				appliedRightPrm = rightPrm;
				appliedAngle0 = angle0;
				appliedAngle1 = angle1;
			}

			candidCount++;
		}
	}

	// 該当区間があった場合
	// -> 接線ベクトルと直交する位置の特定
	if( candidCount > 0 )
	{
		leftPrm = appliedLeftPrm;
		rightPrm = appliedRightPrm;
		angle0 = appliedAngle0;
		angle1 = appliedAngle1;

		// パラメータがほぼ等しくなるまで計算
		while(rightPrm - leftPrm >= 0.0001)
		{
			midPrm = (leftPrm + rightPrm) / 2.0;

			Position(midPrm, toOut0);
			vm->Sub(_outPoint, toOut0, toOut0, 3);
			Tangent(midPrm, tan0);

			angle2 = acos(vm->Dot(toOut0, tan0, 3) / (vm->Length(toOut0, 3) * vm->Length(tan0, 3)));

			if( angle0 <= angle1 )
			{
				if( angle2 <= M_PI / 2.0 )
				{
					leftPrm = midPrm;
					rightPrm = rightPrm;
					continue;
				}
				else {
					leftPrm = leftPrm;
					rightPrm = midPrm;
					continue;
				}
			}
			else {
				if( angle2 <= M_PI / 2.0 )
				{
					leftPrm = leftPrm;
					rightPrm = midPrm;
					continue;
				}
				else {
					leftPrm = midPrm;
					rightPrm = rightPrm;
					continue;
				}
			}
		}

		*_t = (leftPrm + rightPrm) / 2.0;

		if( *_t < minPrm ) *_t = minPrm;
		else if( *_t > maxPrm ) *_t = maxPrm;
	}

	// 該当区間がなかった場合
	// -> 最近点は両端のどちらか
	else {
		SCALAR dist1, dist2;

		Position(minPrm, toOut0);
		vm->Sub(_outPoint, toOut0, toOut0, 3);
		dist1 = vm->Length(toOut0, 3);

		Position(maxPrm, toOut0);
		vm->Sub(_outPoint, toOut0, toOut0, 3);
		dist2 = vm->Length(toOut0, 3);

		*_t = (dist1 <= dist2) ? minPrm : maxPrm;
	}

	if( _nearPoint != NULL ) Position(*_t, _nearPoint);

	vm->Sub(_nearPoint, _outPoint, nearToOut, 3);

	if( _distance != NULL ) *_distance = vm->Length(nearToOut, 3);

	vm->Delete(toOut0);
	vm->Delete(toOut1);
	vm->Delete(tan0);
	vm->Delete(tan1);
	vm->Delete(nearToOut);

	return 0;
}

// 分割
short CBSplineCurve::Split(SCALAR _t, CBSplineCurve** curveA, CBSplineCurve** curveB)
{
	// 挿入インデックスの開始と終了の取得
	short insertBeginIndex, insertEndIndex;
	{
		short idx;
		for(int i = 0; i < order - 1; i++)
		{
			InsertKnot(_t, &idx);

			if( i == 0 ) insertBeginIndex = idx;
			if( i == order - 2 ) insertEndIndex = idx;
		}
		Update();
	}

	short nSplitCtrlPoints;
	short nSplitKnots;

	MATRIX splitCtrlPoints;
	VECTOR splitKnot;

	// 分割曲線A
	{
		nSplitKnots = insertEndIndex + 2;
		nSplitCtrlPoints = nSplitKnots - order;

		splitCtrlPoints = vm->New(4, nSplitCtrlPoints);
		splitKnot	   = vm->New(nSplitKnots);

		// ノット
		for(int i = 0; i < nSplitKnots - 1; i++)
		{
			splitKnot[i] = knot[i];
		}
		splitKnot[nSplitKnots - 1] = _t;

		// 制御点
		for(int i = 0; i < 4; i++)
		{
			for(int j = 0; j < nSplitCtrlPoints; j++)
			{
				splitCtrlPoints[i][j] = (i < 3) ? ctrlPoints[i][j] : 1.0;
			}
		}

		*curveA = new CBSplineCurve(this->order, splitCtrlPoints, nSplitCtrlPoints, splitKnot,
			this->modelColor, this->polyColor);

		vm->Delete(splitCtrlPoints);
		vm->Delete(splitKnot);
	}

	// 分割曲線B
	{
		nSplitKnots = nKnots - (insertBeginIndex - 1);
		nSplitCtrlPoints = nSplitKnots - order;

		splitCtrlPoints = vm->New(4, nSplitCtrlPoints);
		splitKnot	   = vm->New(nSplitKnots);

		// ノット
		splitKnot[0] = _t;
		for(int i = 1; i < nSplitKnots; i++)
		{
			splitKnot[i] = knot[i - 1 + insertBeginIndex];
		}

		// 制御点
		for(int i = 0; i < 4; i++)
		{
			for(int j = 0; j < nSplitCtrlPoints; j++)
			{
				splitCtrlPoints[i][nSplitCtrlPoints - 1 - j] =
					(i < 3) ? ctrlPoints[i][nCtrlPoints - 1 - j] : 1.0;
			}
		}

		*curveB = new CBSplineCurve(this->order, splitCtrlPoints, nSplitCtrlPoints, splitKnot,
			this->modelColor, this->polyColor);

		vm->Delete(splitCtrlPoints);
		vm->Delete(splitKnot);
	}

	return 0;
}

// MMボックス取得
short CBSplineCurve::GetMMBox(MMBox** mmbox)
{
	return __super::GetMMBox(mmbox, nCtrlPoints);
}

// 曲面との交点
short CBSplineCurve::IntersectionForSurface(CBSplineSurface* surface, VECTOR intersectPoint)
{
	return ShapeProcess::IntersectionPointByMMBox(this, surface, intersectPoint);
}

/*
// 折れ線点列の生成
short CBSplineCurve::CreatePolyPoints(short _nPoints)
{
	VECTOR pos = vm->New(3);

	SCALAR interval = knot[nKnots - 1] / (_nPoints - 1);
	polyPoints = vm->New(3, _nPoints);

	for(int j = 0; j < _nPoints; j++)
	{
		if( Position(interval * j, pos) != 0 )
		{
			return 1;
		}

		for(int i = 0; i < 3; i++)
		{
			polyPoints[i][j] = pos[i];
		}
	}

	vm->Delete(pos);

	return 0;
}
*/

// モデルの生成
short CBSplineCurve::CreateModel()
{
	UINT nVertices = MODEL_POLYGONS;
	UINT nIndices = MODEL_POLYGONS;

	D3DVertex* vertices = new D3DVertex[nVertices];
	WORD* indices = new WORD[nIndices];

	SCALAR t;
	VECTOR pos = vm->New(3);

	SCALAR ymin = 0.0;
	SCALAR ymax = 0.0;

	// Vertices Position
	SCALAR paramMin = knot[0];
	SCALAR paramMax = knot[(short)nKnots - 1];

	for (int i = 0; i < MODEL_POLYGONS; i++)
	{
		t = (paramMax - paramMin) * i / (SCALAR)(MODEL_POLYGONS - 1) + paramMin;

		if (Position(t, pos) != 0)
		{
			return 1;
		}

		vertices[i].Pos = XMFLOAT3(pos[0], pos[1], pos[2]);

		if (ymin > pos[1]) ymin = pos[1];
		if (ymax < pos[1]) ymax = pos[1];
	}

	vm->Delete(pos);

	// Indices
	for (int i = 0; i < MODEL_POLYGONS; i++)
	{
		indices[i] = i;
	}

	// Vertices Color
	for (int i = 0; i < nVertices; i++)
	{
		vertices[i].Color = modelColor;
	}

	if (FAILED(curveModel.Init(vertices, nVertices, indices, nIndices, D3D_PRIMITIVE_TOPOLOGY_LINESTRIP)))
	{
		return 2;
	}

	delete vertices;
	delete indices;

	return 0;
}

// 折れ線の生成
short CBSplineCurve::CreatePoly()
{
	// 折れ線点 全消去
	if (ClearPoints(&polyPoints) != 0)
	{
		return 1;
	}

	// 折れ線点
	VECTOR pos = vm->New(3);

	for(int i = 0; i < nCtrlPoints; i++)
	{
		for(int k = 0; k < 3; k++)
		{
			pos[k] = ctrlPoints[k][i];
		}

		polyPoints.push_back(new CPoint(pos, polyColor));
	}

	vm->Delete(pos);


	// 折れ線エッジ 全消去
	if (ClearEdges(&polyEdges) != 0)
	{
		return 2;
	}

	// 折れ線エッジ 生成
	VECTOR start = vm->New(3);
	VECTOR end = vm->New(3);

	for (int i = 0; i < nCtrlPoints - 1; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			start[j] = ctrlPoints[j][i];
			end[j] = ctrlPoints[j][i + 1];
		}

		polyEdges.push_back(new CLine(start, end, polyColor));
	}

	vm->Delete(start);
	vm->Delete(end);

	return 0;
}

// 折れ線の参照
short CBSplineCurve::ReferPoly(short _idx, SCALAR _t, VECTOR _pos)
{
	if (_idx >= polyEdges.size() || _idx < 0)
	{
		return 1;
	}

	if (polyEdges[_idx]->Position(_t, _pos) != 0)
	{
		return 2;
	}

	return 0;
}

// 更新
void CBSplineCurve::Update()
{
	Release();
	Setup(order, updateCtrlPoints, nUpdateCtrlPoints, updateKnot);
}

// 描画
void CBSplineCurve::Draw()
{
	curveModel.Draw();
	
	if( drawPolyPoints )
	{
		std::vector<CPoint*>::iterator it = polyPoints.begin();
		while (it != polyPoints.end())
		{
			(*it)->Draw();
			++it;
		}
	}

	if( drawPolyEdges )
	{
		std::vector<CLine*>::iterator it = polyEdges.begin();
		while (it != polyEdges.end())
		{
			(*it)->Draw();
			++it;
		}
	}
}