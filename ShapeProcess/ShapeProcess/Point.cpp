/*
	Programeed : Takumi Adachi / C&G SYSTEMS 2017.8.30
*/

#include "Point.h"
#include "Application.h"

#define _USE_MATH_DEFINES
#include <math.h>

#define SPHERE_RADIUS 0.05f

// コンストラクタ
CPoint::CPoint(VECTOR _pos, XMFLOAT4 _color)
{
	pos = vm->New(3);

	for(int i = 0; i < 3; i++)
	{
		pos[i] = _pos[i];
	}

	color = _color;

	CreateModel();
}

CPoint::CPoint(SCALAR _x, SCALAR _y, SCALAR _z, XMFLOAT4 _color)
{
	pos = vm->New(3);
	pos[0] = _x;
	pos[1] = _y;
	pos[2] = _z;

	color = _color;

	CreateModel();
}


// デストラクタ
CPoint::~CPoint()
{
	sphereModel.Cleanup();
}

// 位置ベクトル
short CPoint::Position(VECTOR _pos)
{
	for(int i = 0; i < 3; i++)
	{
		_pos[i] = pos[i];
	}

	return 0;
}

// 描画
void CPoint::Draw()
{
	sphereModel.Draw();
}

// モデルの生成
short CPoint::CreateModel()
{
	int sliceCount = 5;
	int stackCount = 5;

	std::vector<XMFLOAT3> dynVertices;
	std::vector<WORD> dynIndices;

	// Vertices Position
	dynVertices.push_back(XMFLOAT3(pos[0] + 0.0f, pos[1] + SPHERE_RADIUS, pos[2] + 0.0f));

	float phiStep = M_PI / stackCount;
	float thetaStep = 2.0f * M_PI / sliceCount;
	for (int i = 1; i <= stackCount - 1; i++)
	{
		float phi = i * phiStep;
		for (int j = 0; j <= sliceCount; j++)
		{
			float theta = j * thetaStep;

			dynVertices.push_back(
				XMFLOAT3(
					pos[0] + SPHERE_RADIUS * sin(phi) * cos(theta),
					pos[1] + SPHERE_RADIUS * cos(phi),
					pos[2] + SPHERE_RADIUS * sin(phi) * sin(theta)
				)
			);
		}
	}

	dynVertices.push_back(XMFLOAT3(pos[0] + 0.0f, pos[1] - SPHERE_RADIUS, pos[2] + 0.0f));

	// Indices
    for (int i = 1; i <= sliceCount; i++)
	{
		dynIndices.push_back(0);
		dynIndices.push_back(i + 1);
		dynIndices.push_back(i);
    }

    int baseIndex = 1;
    int ringVertexCount = sliceCount + 1;

    for (int i = 0; i < stackCount - 2; i++)
	{
        for (int j = 0; j < sliceCount; j++)
		{
            dynIndices.push_back(baseIndex + i * ringVertexCount + j);
            dynIndices.push_back(baseIndex + i * ringVertexCount + j + 1);
            dynIndices.push_back(baseIndex + (i + 1) * ringVertexCount + j);

            dynIndices.push_back(baseIndex + (i + 1) * ringVertexCount + j);
            dynIndices.push_back(baseIndex + i * ringVertexCount + j + 1);
            dynIndices.push_back(baseIndex + (i + 1) * ringVertexCount + j + 1);
        }
    }

    int southPoleIndex = dynVertices.size() - 1;
    baseIndex = southPoleIndex - ringVertexCount;

    for (int i = 0; i < sliceCount; i++)
	{
        dynIndices.push_back(southPoleIndex);
        dynIndices.push_back(baseIndex + i);
        dynIndices.push_back(baseIndex + i + 1);
    }

	// Set Vertices and Indices
	UINT nVertices = dynVertices.size();
	UINT nIndices = dynIndices.size();

	D3DVertex* vertices = new D3DVertex[nVertices];
	WORD* indices = new WORD[nIndices];

	for(UINT i = 0; i < nVertices; i++)
	{
		vertices[i].Pos = dynVertices[i];
		vertices[i].Color = color;
	}

	for(UINT i = 0; i < nIndices; i++)
	{
		indices[i] = dynIndices[i];
	}

	if (FAILED(sphereModel.Init(vertices, nVertices, indices, nIndices, D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST)))
	{
		return 1;
	}

	delete vertices;
	delete indices;

	return 0;
}