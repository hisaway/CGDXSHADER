/*
Programeed : Takumi Adachi / C&G SYSTEMS 2017.8.30
*/

#pragma once

#include "Entity.h"

// 点
class CPoint :public Entity
{
private:
	VECTOR pos;
	Model sphereModel;
	XMFLOAT4 color;

	// モデルの生成
	short CreateModel();

public:
	// コンストラクタ
	CPoint(VECTOR _pos, XMFLOAT4 _color = XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	CPoint(SCALAR _x, SCALAR _y, SCALAR _z, XMFLOAT4 _color = XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));

	// デストラクタ
	~CPoint();

	// 位置ベクトル
	short Position(VECTOR _pos);

	// 描画
	void Draw();
};